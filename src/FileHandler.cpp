/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2013 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
******************************************************************************/

#include "FileHandler.h"

//bool FileHandler::ReadParsedOrTaggedCorpus(string file_name, vector< vector<string> > & tags, vector<Tree> & trees, vector< vector<string> > & words, Model & model)
//{
//    ifstream infile;
//    infile.open(file_name.c_str());
//    if (!infile.good()) {
//        cerr << "Error: could not open file '" << file_name << "' for reading" << endl;
//        return false;
//    }
//    string token;
//    infile >> token;
//    infile.close();
//    if (token[0] == '(') {
//    	cerr << ">Reading parsed corpus." << endl;
//    	bool ret = ReadParsedCorpus(file_name, tags, trees, model, words);
//    	//model.t
//    	return ret;
//    } else {
//    	vector<string> tokens = StringTokenize(token, "/");
//    	cerr << "File is tagged: tokens.size(): " << tokens.size() << endl;
//    	cerr << ">Reading tagged corpus." << endl;
//    	bool ret = ReadTaggedText(file_name, "/", words, tags);
//    	cerr << "words: " << words.size() <<"; tags: "<< tags.size() << endl;
//    	model.addSentencesTags(tags);
//    	return ret;
//    }
//
//    cerr << "Error: unknown format reading corpus file '" << file_name << "'." << endl;
//    return false;
//}

bool FileHandler::ReadTaggedText(string file_name, vector<vector<string> >& words, vector<vector<string> >& tags,
        map<unsigned int, string>& separators, const set<string>& final_punctuation_symbols) {

    ifstream infile;
    infile.open(file_name.c_str());
    if (!infile.good()) {
        cerr << "Error: could not open file '" << file_name << "' for reading" << endl;
        return false;
    }

    string line;
    vector<string> line_tokens;
    int line_number = 0, sentence_number = 0;

    //"/" and " "; // Tycho Brahe format.
    //"_" and " "; // MAC-MORPHO format.
    //" " and "\n"; // Conll format.
    string intra_sep = separators[0];
    string inter_sep = separators[1];

    vector<string> sentence_words, sentence_tags;

    while (getline(infile, line)) {
//    while (true) {
        line_number++;
//        infile >> line;
//        cerr << line << endl;
//        if (infile.eof()) break;
        Trim(line);

        line_tokens = StringTokenize(line, inter_sep);

        vector<string> word_tag;
        for (unsigned int i = 0; i < line_tokens.size(); i++) {
            word_tag = StringTokenize(line_tokens[i], intra_sep, 1, true);
            if (word_tag.size() != 2) {
                cerr << "Warning: reading file '" << file_name << "': in line[" << line_number << "] '" << line_tokens[i] << "': missing a word or tag. Proceeding." << endl;
                continue;
            }
            if (word_tag[0].length() == 0) {
                cerr << "Warning: reading file '" << file_name
                     << "': in line[" << line_number << "] '" << line_tokens[i]
                     << "'" << endl
                     << "         Word has length zero. Proceeding." << endl;
                cerr << "         (The line is: '" << line << "')" << endl;
                continue;
            }

            sentence_words.push_back(word_tag[0]);
            sentence_tags.push_back(word_tag[1]);

            if (final_punctuation_symbols.count(word_tag[1]) > 0) {
                // Is an end of sentence tag
                words.push_back(sentence_words);
                tags.push_back(sentence_tags);
                sentence_number++;

                sentence_words.clear();
                sentence_tags.clear();
            }

        }
        if (sentence_words.size() > 0) {
            words.push_back(sentence_words);
            tags.push_back(sentence_tags);
            sentence_number++;

            sentence_words.clear();
            sentence_tags.clear();
        }
    }

    infile.close();
    return true;
}

bool FileHandler::ReadTaggedText(string file_name, string delimiter, vector< vector<string> >& sentence_words, vector< vector<string> >& sentence_tags)
{
    ifstream infile;
    infile.open(file_name.c_str());

    if (!infile.good()) {
        cerr << "Error: 'could not open file '" << file_name << "' for reading" << endl;
        return false;
    }

    string line, delim;
    vector<string> line_tokens;
    int line_number = 0, sentence_number = 0;

    vector<string> words, tags;
    //int max_line_length = 500000;
    //char line_char[max_line_length];

    //delim = "/"; // Tycho Brahe format.
    //delim = "_"; // MAC-MORPHO format.
    delim = delimiter;

    while (getline(infile, line)) {
        line_number++;
        //infile >> line;
        //infile.getline(line_char, max_line_length);
        //line = line_char;
        //if (infile.eof()) break;

        line_tokens = StringTokenize(line, " ");

        vector<string> word_tag;
        for (int i = 0; i < (int)line_tokens.size(); i++) {
            word_tag = StringTokenize(line_tokens[i], delim);
            if (word_tag.size() != 2) {
                if (delim == "_") {
                    if (word_tag.size() == 3) {
                        if (word_tag[0].length() == 0) {
                            word_tag[0] = delim;
                        } else {
                            word_tag[0] += word_tag[1];
                        }
                        if (word_tag[2].length() != 0) {
                            word_tag[1] = word_tag[2];
                        }
                        word_tag.pop_back();
                    } else {
                        cerr << "Warning: reading file '" << file_name << "': in line '" << line_tokens[i] << "': missing a word or tag. Proceeding." << endl;
                        continue;
                    }
                } else {
                    cerr << "Warning: reading file '" << file_name << "': in line '" << line_tokens[i] << "': missing a word or tag. Proceeding." << endl;
                    continue;
                }
            }
            if (word_tag[0].length() == 0) {
                cerr << "Warning: reading file '" << file_name
                 << "': in line [" << line_number << "], sentence [" << sentence_number+1 << "] '" << line_tokens[i]
                 << "': word has length zero. Proceeding." << endl;
                cerr << "\tLine is: '" << line << "'" << endl;
                continue;
            }
            words.push_back(word_tag[0]);
            tags.push_back(word_tag[1]);

            /*
            if (word_tag[1] == ".") { // End of sentence.
            	sentence_words.push_back(words);
            	sentence_tags.push_back(tags);
            	words.clear();
            	tags.clear();
            	sentence_number++;
            }*/
        }
    	sentence_words.push_back(words);
    	sentence_tags.push_back(tags);
    	words.clear();
    	tags.clear();
    	sentence_number++;
    }
    //ChangeConflictingTags(tags);

    infile.close();
    return true;
}


/**
 * Read tagged text in the format: WORD1sep[0]TAG1sep[1]WORD2sep[0]TAG2.
 */
bool FileHandler::ReadTaggedText(string file_name, map<unsigned int, string>& separators, vector<string>& words, vector<string>& tags)
{
//    //std::locale sv_SE("sv_SE.utf8");  // Any UTF-8 locale would do I guess
//    typedef std::codecvt<wchar_t, char, std::mbstate_t> utf8_codecvt_t;
//    const utf8_codecvt_t &utf8_codecvt = std::use_facet<utf8_codecvt_t>(std::locale(""));
//    std::locale utf8_locale(std::locale(""), &utf8_codecvt);
//
//    //const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
//    //const std::locale utf8_locale = newlocale() ;//std::locale(std::locale(), new std::codecvt_byname("UTF8"));
//    //std::codecvt_byname();
//    std::wcout << "User-preferred locale setting is " << std::locale("").name().c_str() << endl;

    ifstream infile;
//    infile.imbue(utf8_locale);
    infile.open(file_name.c_str());
    if (!infile.good()) {
        cerr << "Error: could not open file '" << file_name << "' for reading" << endl;
        return false;
    }

    string line;
    vector<string> line_tokens;
    int line_number = 0;

    //"/" and " "; // Tycho Brahe format.
    //"_" and " "; // MAC-MORPHO format.
    //" " and "\n"; // Conll format.
    string intra_sep = separators[0];
    string inter_sep = separators[1];

    while (true) {
        line_number++;
        infile >> line;
        if (infile.eof()) break;

        line_tokens = StringTokenize(line, inter_sep);

        vector<string> word_tag;
        for (int i = 0; i < (int)line_tokens.size(); i++) {
            word_tag = StringTokenize(line_tokens[i], intra_sep, 1, true);
            if (word_tag.size() != 2) {
                cerr << "Warning: reading file '" << file_name << "': in line '" << line_tokens[i] << "': missing a word or tag. Proceeding." << endl;
                continue;
            }
    	    if (word_tag[0].length() == 0) {
    		    cerr << "Warning: reading file '" << file_name
        			 << "': in line[" << line_number << "] '" << line_tokens[i]
                     << "'" << endl
        			 << "         Word has length zero. Proceeding." << endl;
    		    cerr << "         (The line is: '" << line << "')" << endl;
    		    continue;
    	    }
            words.push_back(word_tag[0]);
            tags.push_back(word_tag[1]);
        }
    }
    //ChangeConflictingTags(tags);

    infile.close();
    return true;
}

/*
 * @brief Write tagged text to file using @a separators as delimiters.
 *
 * If separators[2] and/or [3] are defined, they are used instead of [0] and [1].
 * The output format is then: WORD1sep[2|0]TAG1sep[3|1]WORD2sep[2|0]TAG2...
 */
bool FileHandler::WriteTaggedText(string file_name, map<unsigned int, string>& separators, const vector< vector<string> >& words, const vector< vector<string> >& tags)
{
    if (words.size() != tags.size()) {
        cerr << "Warning: writing tagged text: number of words not equal to number of tags. Trying to continue." << endl;
        //return false;
    }

    ofstream outfile;
    outfile.open(file_name.c_str());

    if (!outfile.good()) {
        cerr << "Error: could not open file '" << file_name << "' for writing" << endl;
        return false;
    }

    string intra_sep = separators.count(2) ? separators[2] : separators[0];
    string inter_sep = separators.count(3) ? separators[3] : separators[1];

    for (int i = 0; i < (int)words.size(); i++) {
        if (words[i].size() != tags[i].size()) {
            cerr << "Warning: writing tagged text: sentence " << i+1
				 << ": number of words is " << (words[i].size() < tags[i].size() ? "less" : "greater")
				 << " than number of tags. Continuing." << endl;
            unsigned int size = min(words[i].size(), tags[i].size());
            for (unsigned int j = 0; j < size; j++) {
                outfile << words[i][j] << intra_sep << tags[i][j] << inter_sep;
            }
            if (words[i].size() < tags[i].size()) {
                for (unsigned int j = size; j < tags[i].size(); j++) {
                    outfile << "--MISSING--" << intra_sep << tags[i][j] << inter_sep;
                }
            } else {
                for (unsigned int j = size; j < words[i].size(); j++) {
                    outfile << words[i][j] << intra_sep << "--MISSING--" << inter_sep;
                }
            }
        }
        for (int j = 0; j < (int)words[i].size(); j++) {
            outfile << words[i][j] << intra_sep << tags[i][j] << inter_sep;
        }
	    outfile << endl; // FIXME: use a separator given as argument.
    }

    outfile.close();
    return true;
}

/*
 * @brief Write tagged text to file using @a separators as delimiters.
 *
 * If separators[2] and/or [3] are defined, they are used instead of [0] and [1].
 * The output format is then: WORD1sep[2|0]TAG1sep[3|1]WORD2sep[2|0]TAG2...
 *
 * Additionally, a newline is output after a tag '.'.
 */
bool FileHandler::WriteTaggedText(string file_name, map<unsigned int, string>& separators, const vector<string>& words, const vector<string>& tags)
{
    if (words.size() != tags.size()) {
        cerr << "Warning: writing tagged text: number of words not equal to number of tags. Trying to continue." << endl;
        //return false;
    }

    ofstream outfile;
    outfile.open(file_name.c_str());

    if (!outfile.good()) {
        cerr << "Error: could not open file '" << file_name << "' for writing." << endl;
        outfile.close();
        return false;
    }

    string intra_sep = separators.count(2) ? separators[2] : separators[0];
    string inter_sep = separators.count(3) ? separators[3] : separators[1];

    for (unsigned int i = 0; i < words.size() && i < tags.size(); i++) {
        outfile << words[i] << intra_sep << tags[i] << inter_sep;
	    if (tags[i] == ".") outfile << endl;
    }

    outfile.close();
    return true;
}

/**
 * Read untagged text.
 */
bool FileHandler::ReadUntaggedText(string file_name, vector<string>& words)
{
    ifstream infile;
    infile.open(file_name.c_str());
    if (!infile.good()) {
        cerr << "Error: could not open file '" << file_name << "' for reading" << endl;
        return false;
    }

    string line;
    string delim;
    vector<string> line_tokens;
    int line_number = 0;

    while (true) {
        line_number++;
        infile >> line;
        if (infile.eof()) break;

        delim = " ";
        line_tokens = StringTokenize(line, delim);

        words.insert(words.end(), line_tokens.begin(), line_tokens.end());
    }

    infile.close();
    return true;
}

bool FileHandler::WriteText(string file_name, vector< vector<string> > sentences_words, string line_separator, string word_separator)
{
    ofstream outfile;
    outfile.open(file_name.c_str());

    if (!outfile.good()) {
        cerr << "Error: could not open file '" << file_name << "' for writing" << endl;
        outfile.close();
        return false;
    }

    for (unsigned int i = 0; i < sentences_words.size(); i++) {
        for (unsigned int j = 0; j < sentences_words[i].size()-1; j++) {
            outfile << sentences_words[i][j] << word_separator;
        }
        outfile << sentences_words[i][sentences_words[i].size()-1] << line_separator;
    }

    outfile.close();
    return true;
}

bool FileHandler::WriteText(string file_name, string headline, const vector<string>& text_lines, string line_separator = "\n")
{
    ofstream outfile;
    outfile.open(file_name.c_str());

    if (!outfile.good()) {
        cerr << "Error: could not open file '" << file_name << "' for writing" << endl;
        outfile.close();
        return false;
    }

    outfile << headline;
    for (int i = 0; i < (int)text_lines.size(); i++) {
        outfile << text_lines[i] << line_separator;
    }

    outfile.close();
    return true;
}


bool FileHandler::WriteText(string file_name, const string& text)
{
    ofstream outfile;
    outfile.open(file_name.c_str());

    if (!outfile.good()) {
        cerr << "Error: could not open file '" << file_name << "' for writing" << endl;
        return false;
    }

	outfile << text;

    outfile.close();
    return true;
}

bool FileHandler::StripTag(string tag_to_strip, vector<vector<string> > & words, vector<vector<string> > & tags)
{
	if (words.size() != tags.size()) {
		cerr << "Error: cannot strip tag '" << tag_to_strip << "': number of words and tags differ." << endl;
		return false;
	}
	for (unsigned int i = 0; i < tags.size(); i++) {
		if (words[i].size() != tags[i].size()) {
			cerr << "Error: cannot strip tag '" << tag_to_strip << "': number of words and tags differ." << endl;
			return false;
		}
		for (unsigned int j = 0; j < tags[i].size(); j++) {
			if (tags[i][j] == tag_to_strip) {
				words[i].erase(words[i].begin() + j);
				tags[i].erase(tags[i].begin() + j);
				j--;
			}
		}
	}
	return true;
}

bool FileHandler::StripLongSentences(unsigned int max_length, vector< vector<string> > & words, vector< vector<string> > & tags)
{
	if (words.size() != tags.size()) {
		cerr << "Error: cannot strip sentences longer than '" << max_length << "': number of words and tags differ." << endl;
		return false;
	}
	vector< vector<string> > new_words, new_tags;
	for (unsigned int i = 0; i < tags.size(); i++) {
		if (words[i].size() != tags[i].size()) {
			cerr << "Error: cannot strip sentences longer than '" << max_length << "': number of words and tags differ." << endl;
			return false;
		}
		if (words[i].size() <= max_length) {
			new_words.push_back(words[i]);
			new_tags.push_back(tags[i]);
		}
	}
	words = new_words;
	tags = new_tags;
	return true;
}

bool FileHandler::StripShortSentences(unsigned int min_length, vector< vector<string> > & words, vector< vector<string> > & tags)
{
	if (words.size() != tags.size()) {
		cerr << "Error: cannot strip sentences longer than '" << min_length << "': number of words and tags differ." << endl;
		return false;
	}
	vector< vector<string> > new_words, new_tags;
	for (unsigned int i = 0; i < tags.size(); i++) {
		if (words[i].size() != tags[i].size()) {
			cerr << "Error: cannot strip sentences longer than '" << min_length << "': number of words and tags differ." << endl;
			return false;
		}
		if (words[i].size() >= min_length) {
			new_words.push_back(words[i]);
			new_tags.push_back(tags[i]);
		}
	}
	words = new_words;
	tags = new_tags;
	return true;
}

