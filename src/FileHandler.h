/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2013 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
******************************************************************************/

#ifndef FILEHANDLER_H
#define FILEHANDLER_H

// include headers that implement a archive in simple text format
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>

#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <cassert>
#include <iterator>
#include "functions.h"
//#include "Tree.h"


using namespace std;

class FileHandler {
    // Associations
    // Attributes
public:
//    static unsigned int max_testing_sentence_size;
//    static double proportion_for_testing;

    // Operations
public:
    FileHandler();
    virtual ~FileHandler();

    /**
     * Save a serializable object to a file.
     * Object must have its serialization method registered with s11n library.
     * @param compress If true file is compressed using zlib.
     * @param format If not specified, uses the default (funtxt), but can be 'funxml',
     *        'simplexml', 'parens', etc (see s11n documentation at http://s11n.net).
     * @return <code>true</code> if saving was succesful, false otherwise.
     */
    //*
    template <typename T>
    static bool SaveSerializableObject(const T & object, string file_name, bool compress = false, string format = "")
    {
        try {
			// create and open a character archive for output
			std::ofstream ofs(file_name.c_str());
//        	string temp_file = tmpnam(0);
//        	cerr << "Temp FILE: " << temp_file << endl;
//        	std::ofstream ofs("temp");
//        	std::stringstream ofs;
			// save data to archive
			{
				boost::archive::text_oarchive oa(ofs);
				oa.register_type(static_cast<T *>(NULL));
				// write class instance to archive
				oa << object;
				// archive and stream closed when destructors are called
			}
			//ofs.close();

			/*
			std::ifstream ifs("temp", std::ios::binary);

			gzofstream gzofs;
        	gzofs.open(file_name.c_str());

        	gzofs << ifs.rdbuf();

        	gzofs.close();
			ifs.close();
			*/

			/*
            s11nlite::micro_api< T > mic;
            if (format != "") {
                mic.serializer_class(format);
            }
            if (compress) {
                gzofstream outf;
                outf.open(file_name.c_str());
                if (!mic.buffer(object)) return false;
                outf << mic.buffer();
                mic.clear_buffer();
                outf.close();
            } else {
                mic.save(object, file_name);
            }
            // */
            return true;
        } catch (const std::exception & ex) {
            std::cerr << "Error (saving '" << file_name << "'): " << ex.what() << "." << endl;
            return false;
        }
    };
    // */

    /**
     * Load a serializable object from a file.
     * Object must have its deserialization method registered with s11n library.
     * @return <code>true</code> if loading was succesful, false otherwise.
     */
    //*
    template <typename T>
    static bool LoadSerializableObject(T & object, string file_name)
    {
        try {
        	{
        		/*
                gzifstream inf;
                //inf.rdbuf()->pubsetbuf(0,0);
                inf.open(file_name.c_str());

                std::ofstream ofs("tempasdljfk");
                ofs << inf.rdbuf();
                ofs.close();
                inf.close();

                */

                // create and open an archive for input
				std::ifstream ifs(file_name.c_str());//, std::ios::binary);
//                std::ifstream ifs("tempasdljfk", std::ios::binary);
				//gzifstream ifs(file_name.c_str(), std::ios::binary);
				boost::archive::text_iarchive ia(ifs);
				// read class state from archive
				ia >> object;
				// archive and stream closed when destructors are called
//	        	ifs.close();
			}
        	/*
            gzifstream inf;
            inf.rdbuf()->pubsetbuf(0,0);
            inf.open(file_name.c_str());
            s11nlite::micro_api< T > mic;
            T * obj = mic.load(inf);
            if (!obj) {
                cerr << "Error (loading file '" << file_name << "'): file not found or deserialization failed." << endl;
                return false;
            }
            object = *obj;
            delete obj;
            inf.close();
            // */
            return true;
        } catch (const std::exception & ex) {
            std::cerr << "Error (loading file '" << file_name << "'): " << ex.what() << "." << endl;
            return false;
        }
    };
    // */

    static bool ReadTaggedText(string file_name, string delimiter, vector< vector<string> >& words, vector< vector<string> >& tags);
    static bool ReadTaggedText(string file_name, map<unsigned int, string>& separators, vector<string>& words, vector<string>& tags);
    static bool ReadTaggedText(string file_name, vector< vector<string> >& words, vector< vector<string> >& tags, map<unsigned int, string>& separators, const set<string>& final_punctuation_symbols);
    static bool ReadUntaggedText(string file_name, vector<string>& words);
    static bool WriteTaggedText(string file_name, map<unsigned int, string>& separators, const vector< vector<string> >& words, const vector< vector<string> >& tags);
    static bool WriteTaggedText(string file_name, map<unsigned int, string>& separators, const vector<string>& words, const vector<string>& tags);
    static bool WriteText(string file_name, vector< vector<string> > sentences_words, string line_separator = "\n", string word_separator = " ");
    static bool WriteText(string file_name, string headline, const vector<string>& text_lines, string line_separator);
    static bool WriteText(string file_name, const string& text);

    static bool StripTag(string tag_to_strip, vector< vector<string> > & words, vector< vector<string> > & tags);
    static bool StripLongSentences(unsigned int max_length, vector< vector<string> > & words, vector< vector<string> > & tags);
    static bool StripShortSentences(unsigned int min_length, vector< vector<string> > & words, vector< vector<string> > & tags);

    /**
     * Write each element of the given container to one line in the file.
     * Container must allow iterators.
     * Elements of the given container must implement operator<<(ostream&).
     * @param container of elements.
     * @param name of the file to be written.
     * @return true if successful, false otherwise.
     */
    template <class C>
    static bool WriteContainer(string file_name, C& container)
    {
        try {
            ofstream outfile;
            outfile.open(file_name.c_str());

            if (!outfile.good()) {
                cerr << "Error: could not open file '" << file_name << "' for writing" << endl;
                outfile.close();
                return false;
            }

            for (auto it = container.begin(); it != container.end(); it++) {
                outfile << (*it).toString() << endl;
            }
            /*
            unsigned int n = trees.size();
            for (unsigned int i = 0; i < n; i++) {
                //trees[i].print();
                outfile << trees[i].toString() << endl;
            }
            */
            //copy(trees.begin(), trees.end(), ostream_iterator<Tree>(outfile, "\n"));

            outfile.close();
            return true;
        } catch (exception & e) {
            cerr << "Error: " << e.what() << endl;
            return false;
        }
    };

};

#endif
