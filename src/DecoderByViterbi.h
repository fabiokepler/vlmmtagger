/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2013 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#ifndef _ALGO_VITERBI_HPP
#define _ALGO_VITERBI_HPP

#include <queue>

#include "main.h"
#include "Model.h"

class DecoderByViterbi {
private:
    Model& model_;
//    vector<string> m_output;
//    vector<string> m_best_path;
//    long double m_prob_best_path;
//    typedef struct {
//        vector<string> state_sequence;
//        long double probability;
//    } subpath_t;
    typedef struct {
        deque<string>* tag_sequence;
        long double log_probability;
    } path_pointer_t;
    typedef struct {
        deque<string> tag_sequence;
        long double log_probability;
    } path_t;

public:
    DecoderByViterbi(Model& model) :
            model_(model) {
    };
    ~DecoderByViterbi(){};

    vector<vector<string> > tag(const vector<vector<string> >& words);
    vector<string> getMostLikelyTags(const vector<string>& words);
};

#endif
