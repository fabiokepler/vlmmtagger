/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2014 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
******************************************************************************/

#ifndef _FUNCTIONS_HPP
#define _FUNCTIONS_HPP

using namespace std;

#include <string>
#include <vector>
#include <deque>
#include <map>
#include <fstream>
#include <iostream>
#include <ostream>
#include <sstream>
#include <set>
#include <stdexcept>
#include <typeinfo>
//#include "Tree.h"

class BadConversion : public std::runtime_error {
public:
	BadConversion(const std::string& s) : std::runtime_error(s)	{ }
};

//vector<string> StringTokenize(string, string);
vector<string> StringTokenize(string source, string delimiter, unsigned int max_splits = 0, bool backwards = false);
vector< vector<string> > VectorStringTokenize(vector<string> source, string delimiter);
vector< vector<string> > VectorStringTokenize(vector<string> source, set<string> delimiters);
vector< vector<string> > ImitateVectorStringTokenize(vector<string> source, vector< vector<string> > model);

string TrimString(const string &, const string &);
void Trim(std::string& str, const std::string & ChrsToTrim = " \t\n\r", int TrimDir = 0);
inline void TrimRight(std::string& str, const std::string & ChrsToTrim);
inline void TrimLeft(std::string& str, const std::string & ChrsToTrim);

inline bool HasUpperChar(const string& str)
{
	for (auto i = str.begin(); i != str.end(); i++) {
		if (isupper(*i)) return true;
	}
	return false;
};

inline string ToLower(const string& str)
{
	string lower = str;
	for (unsigned int i = 0; i < lower.length(); i++)
		lower[i] = tolower(lower[i]);
	return lower;
}

map<string, long double> CountTags(const vector< vector<string> >& tags);
multimap<long double, string> ReverseCountTags(const vector< vector<string> >& tags);

long double CalculateAccuracy(vector< vector<string> >, vector< vector<string> >, vector< vector<string> >, set<string>, set<string>, string errors_file_name = "errors.info");


map<string, map<string, int> > GetComposedTagsWithWords(vector< vector<string> > & stags, vector< vector<string> > & swords);
//map<deque<string>, map<deque<string>, int> > GetSplitWordsWithTags(vector<Tree> & trees);
map<string, map< deque<string>, int> > GetMapOfComposedWords(map<string, map<string, int> > & composed_tags, map<deque<string>, map<deque<string>, int> > & split_words);

// Implementations of templated functions

template < typename T >
void ChangeConflictingTags(T& tags)
{
    string old_tag, new_tag;
    old_tag = ",";
    new_tag = "COMMA";
    replace(tags.begin(), tags.end(), old_tag, new_tag);
    old_tag = ".";
    new_tag = "FPUNC";
    replace(tags.begin(), tags.end(), old_tag, new_tag);
    old_tag = "(";
    new_tag = "PARENTHESES";
    replace(tags.begin(), tags.end(), old_tag, new_tag);
}

template < >
void ChangeConflictingTags(set<string>& tags);

template < typename T >
void UpdateTags(T& tags)
{
    string old_tag, new_tag;
    old_tag = "COMMA";
    new_tag = ",";
    replace(tags.begin(), tags.end(), old_tag, new_tag);
    old_tag = "FPUNC";
    new_tag = ".";
    replace(tags.begin(), tags.end(), old_tag, new_tag);
    old_tag = "PARENTHESES";
    new_tag = "(";
    replace(tags.begin(), tags.end(), old_tag, new_tag);
}

////////////////////////////////////////////////////////

//*
//template <Tree::Node*>
//template<>
//string ToString(const Tree::Node* & datum);
/*{
	ostringstream ost;
	ost << "TESTE" << (datum);
	return string(ost.str());
}
// */

template <typename T>
string ToString(T * pdatum)
{
	ostringstream ost;
	//string str = datum->toString();
	T datum = (*pdatum);
	ost << datum;
	return string(ost.str());
}
// */


template <class T>
string ToString(const T & datum)
{

	ostringstream ost;
	ost << datum;
	return string(ost.str());
}

template <typename T>
string ToString(const set<T> & vdatum, string separator = " ")
{
	if (vdatum.empty())
		return "";

    ostringstream ost;
    auto it = vdatum.begin();
    ost << ToString((*it));
    it++;
    for (; it != vdatum.end(); it++) {
        ost << separator << ToString((*it));
    }

    return string(ost.str());
}

template <typename T>
string ToString(const vector<T> & vdatum, string separator = " ")
{
    ostringstream ost;
    unsigned int size = vdatum.size();
    for (unsigned int i = 0; i < size; i++) {
        ost << ToString(vdatum[i]) << ((i == size-1) ? "" : separator);
    }

    return string(ost.str());
}

template <typename T>
string ToString(const deque<T> & vdatum, string separator = " ")
{
    ostringstream ost;
    unsigned int size = vdatum.size();
    for (unsigned int i = 0; i < size; i++) {
        ost << ToString(vdatum[i]) << ((i == size-1) ? "" : separator);
    }

    return string(ost.str());
}

template <typename T, typename U>
string ToString(const pair<T,U> & vdatum, string separator = ",")
{
    ostringstream ost;
    ost << ToString(vdatum.first) << separator << ToString(vdatum.second);

    return string(ost.str());
}

template <typename T, typename U>
string ToString(const multimap<T,U> & vdatum, string intra_separator = ",", string inter_separator = "\n")
{
    ostringstream ost;
    for (auto it = vdatum.begin(); it != vdatum.end(); it++) {
    	ost << ToString((*it).first) << intra_separator << ToString((*it).second) << inter_separator;
    }

    return string(ost.str());
}

template <typename T, typename U>
string ToString(const map<T,U> & vdatum, string intra_separator = ",", string inter_separator = "\n")
{
    ostringstream ost;
    for (auto it = vdatum.begin(); it != vdatum.end(); it++) {
    	ost << ToString((*it).first) << intra_separator << ToString((*it).second) << inter_separator;
    }

    return string(ost.str());
}

template <typename T, typename U>
string ToString(const deque< pair<T,U> > & vdatum, string intra_separator = ",", string inter_separator = "\n")
{
    ostringstream ost;
    for (auto it = vdatum.begin(); it != vdatum.end(); it++) {
    	ost << ToString((*it), intra_separator) << inter_separator;
    }

    return string(ost.str());
}


template <typename T>
inline T FromString(const string s)
{
	std::istringstream ist(s);
	T x;
	if (!(ist >> x))
		throw BadConversion(string(typeid(T).name()) + "FromString(\"" + s + "\")");
	return x;
};

////////////////////////////////////////////////////////

#endif
