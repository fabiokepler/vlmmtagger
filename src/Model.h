/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2014 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#ifndef _MODEL_HPP
#define _MODEL_HPP

#include <string>
#include "main.h"
#include "functions.h"
#include "Corpus.h"
#include "AffixTree.h"
#include "ContextTree.h"
#include "ProgramOptions.h"

using namespace std;

class Model {
public:
    class ModelOptions {
    public:
        ModelOptions();
        virtual ~ModelOptions();
        void setOptions(ProgramOptions& options);
        void print() const;
        bool isIgnoringPunctuation() const;

        bool do_training;
        unsigned int iterations;

        bool skip_leftContextTree_building;
        bool skip_wordSuffixTree_building;
        bool skip_tag_word_probabilities_building;
        bool ignore_punctuation;

        unsigned int opened_tag_minimum_assigns;
        unsigned int unknown_word_threshold;

        bool word_suffix_size_is_set;
        long double word_suffix_size;

        long double cut_value; // ContextTree pruning constant.
        int maximum_markov_order;
        int minimum_times_to_be_in_tree; // Only sequences of tags occurring at least this number of times are put on the tree.
    };


    Model();
    bool initialize();
    ~Model();

    void setOptions(ProgramOptions& options);

    inline const ModelOptions& getOptions() const {
        return options_;
    }
    
    void setTrained(bool trained) {
        is_trained = trained;
    };
    bool needsTraining() {
        return !is_trained;
    };
    bool hasData();
    bool train(Corpus& corpus);

    bool firstPassing(vector<vector<string> >& words, vector<vector<string> >& tags);
    bool computeWordsAndTagsCounts(vector<vector<string> >& words, vector<vector<string> >& tags);
    bool computeOpenAndClosedTags();
    bool computeUnknownWordFeaturesCounts(vector<vector<string> >& words, vector<vector<string> >& tags);
    bool buildAffixTrees(vector<vector<string> >& words, vector<vector<string> >& tags);
    bool computeContext(vector<vector<string> >& words, vector<vector<string> >& tags);
    bool normalizeWordsAndTags();
    bool normalizeUnknownWordFeatures();
    bool initializeWordsAndTagsProbability(long double initial_probability = 0.0);
    bool initializeUnknownWordFeaturesProbability(long double initial_probability = 0.0);
    bool addToUnknownWordFeaturesProbability(const string& word, const string& tag, long double probability);

    bool secondPassing(vector<vector<string> > & words, vector<vector<string> > & tags);
    bool buildContextTree(vector<vector<string> > & words, vector<vector<string> > & tags);
    bool computeDependencies(const vector<vector<string> > & words, const vector<vector<string> > & tags);
    bool computeRightContext(const vector<vector<string> > & words, const vector<vector<string> > & tags);
    bool computeConstituentContext(const vector<vector<string> > & words, const vector<vector<string> > & tags);
    bool computeWordsContext(const vector<vector<string> > & words, const vector<vector<string> > & tags);

    bool isPunctuation(string word);
    bool isFinalPunctuation(string word);
    bool isWordKnown(string word) const;
    bool isWordRare(string word);
    inline bool isWordAmbiguous(const string& word) const {
        return ambiguous_words_.find(word) != ambiguous_words_.end() || !isWordKnown(word);
    }
    inline bool isClosedTag(string tag) {
        return closed_tags_.find(tag) != closed_tags_.end();
    }

    set<string> getKnownWords();
    set<string> getAmbiguousWords();
    set<string> getTagsForWord(string word);
    vector<string> getCandidateTagsForWord(string word);

    long double getLogProbabilityAndContextUsed(string candidate_tag, string word, const deque<string>& left_context,
            deque<string> &used_branch);
    long double getKnownWordProbability(const string& tag, const string& word, long double probability_if_not_found = 0.0);
    long double getKnownWordLogProbability(const string& tag, const string& word, long double probability_if_not_found = -numeric_limits<long double>::infinity());
    long double getUnknownWordLogProbability(const string& candidate_tag, const string& word);

    inline long double getNumberOfTagOccurences(const string& tag) const {
        auto it = tag_counter_.find(tag);
        if (it != tag_counter_.end())
            return (*it).second;
        else
            return 0.0;
    }
    inline long double getNumberOfWordOccurences(const string& word) const {
        auto it = word_counter_.find(word);
        if (it != word_counter_.end())
            return (*it).second;
        else
            return 0.0;
    }
    deque<pair<string, long double> > getWordsByFrequency(int max = 25) const;

    string getWordSuffix(string word);
    string getWordPrefix(string word);
    set<string> getTagsForWord(string word, string tag_from_last_word);

    bool filterContext(deque<string>& context);
    deque<string> getFilteredContext(const deque<string>& context);
    bool simplifyOneTag(string& tag);
    bool simplifyTags(deque<string>& context);

    template<typename T1, typename T2, typename T3>
    static bool initializeMap(map<T1, T2>& container, T3 value) {
        for (auto it1 = container.begin(); it1 != container.end(); it1++) {
            (*it1).second = value;
        }
        return true;
    }
    template<typename T1, typename T2, typename T3, typename T4>
    static bool initializeMatrix(map<T1, map<T2, T3> >& matrix, T4 value) {
        for (auto it1 = matrix.begin(); it1 != matrix.end(); it1++) {
            for (auto it2 = (*it1).second.begin(); it2 != (*it1).second.end(); it2++) {
                (*it2).second = value;
            }
        }
        return true;
    }
    template<typename T1, typename T2, typename T3>
    static bool initializeMatrix1WithMatrix2TimesFactor(map<T1, map<T2, T3> >& matrix1, map<T1, map<T2, T3> >& matrix2,
            T3 factor) {
        for (auto it1 = matrix2.begin(); it1 != matrix2.end(); it1++) {
            for (auto it2 = (*it1).second.begin(); it2 != (*it1).second.end(); it2++) {
                matrix1[(*it1).first][(*it2).first] = (*it2).second * factor;
            }
        }
        return true;
    }
    template<typename T1, typename T2>
    static bool applyLogToMapValues(map<T1, T2>& container) {
        for (auto it1 = container.begin(); it1 != container.end(); it1++) {
            (*it1).second = logl((*it1).second);
        }
        return true;
    }
    template<typename T1, typename T2, typename T3>
    static bool applyLogToMatrixValues(map<T1, map<T2, T3> >& matrix) {
        for (auto it1 = matrix.begin(); it1 != matrix.end(); it1++) {
            for (auto it2 = (*it1).second.begin(); it2 != (*it1).second.end(); it2++) {
                (*it2).second = logl((*it2).second);
            }
        }
        return true;
    }

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version) {
        ar & input_words_size_;
        ar & input_tags_size_;
        ar & input_sentences_size_;

        ar & tag_counter_;
        ar & tag_word_counter_;
        ar & word_counter_;
        ar & unknown_tag_counter_;
        ar & word_tag_counter_;

        ar & leftContextTree_;
        ar & wordSuffixTree_;

        ar & average_open_word_length_;

        ar & closed_tags_;
        ar & opened_tags_;

        ar & ambiguous_words_;
        ar & words_ambiguity_;

        ar & uppercase_letter_tags_counter_;
        ar & uppercase_word_tags_counter_;
        ar & numerical_tags_counter_;
        ar & total_numerical_tags_;
        ar & total_uppercase_letter_tags_;
        ar & total_uppercase_word_tags_;
        ar & hyphen_word_tags_counter_;
        ar & total_hyphen_word_tags_;
    }


private:
    ModelOptions options_;

    bool is_trained;

    ContextTree leftContextTree_;
    AffixTree wordSuffixTree_;

    unsigned int input_words_size_;
    unsigned int input_tags_size_;
    unsigned int input_sentences_size_;
    long double average_open_word_length_;

    map<string, long double> tag_counter_;
    map<string, long double> word_counter_;
    map<string, map<string, long double> > word_tag_counter_;
    map<string, map<string, long double> > tag_word_counter_;

    set<string> closed_tags_;
    set<string> opened_tags_;

    map<string, long double> unknown_tag_counter_;

    set<string> ambiguous_words_;
    map<unsigned int, set<string> > words_ambiguity_;

    map<string, long double> uppercase_letter_tags_counter_; // Tags occurring with words where the first letter is in uppercase.
    map<string, long double> uppercase_word_tags_counter_; // Tags occurring with words that have all letters in uppercase.
    map<string, long double> numerical_tags_counter_; // Tags occurring with words that start with a digit.
    map<string, long double> hyphen_word_tags_counter_;// Tags occurring with words that are hyphenated.
    long double total_numerical_tags_;
    long double total_uppercase_letter_tags_;
    long double total_uppercase_word_tags_;
    long double total_hyphen_word_tags_;

};

BOOST_CLASS_VERSION(Model, 2)

#endif
