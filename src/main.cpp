/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2013 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include <iostream>
#include <string>

#include "main.h"
#include "functions.h"
#include "Corpus.h"
#include "DecoderByViterbi.h"
#include "FileHandler.h"
#include "Model.h"
#include "ProgramOptions.h"

#define PROGRAM_NAME "VLMMTagger"
#define CONTACT "<fabio@kepler.pro.br>"
#define VERSION "2.1"

ui::UIHandler::VerboseOutput vout, voutHigh, voutLow;

bool saveModel(Model& model, string filename) {
    vout << "[Saving Trained Markov Model to '" << filename << "'...]" << endl;
    try {
//   std::ofstream ofs(fname, std::ios::out|std::ios::binary|std::ios::trunc);
        ofstream ofs(filename.c_str(), ios::binary);
        if (!ofs.good()) {
            cerr << "Error: could not open file '" << filename << "' for writing" << endl;
            return false;
        }
        //boost::archive::binary_oarchive oa(ofs);
        boost::iostreams::filtering_streambuf<boost::iostreams::output> out;
        out.push(boost::iostreams::gzip_compressor());
        out.push(ofs);
        boost::archive::binary_oarchive oa(out);
        oa << model;
    } catch (exception& e) {
        cerr << "Error: " << e.what() << endl;
        return false;
    }
    return true;
}

bool loadModel(Model& model, string filename) {
    vout << "[Loading Trained Markov Model from file '" << filename << "'..." << endl;
    try {
        std::ifstream ifs(filename.c_str(), ios::binary);
        if (!ifs.good()) {
            cerr << "Error: could not open file '" << filename << "' for writing" << endl;
            return false;
        }
        //boost::archive::binary_iarchive ia(ifs);
        boost::iostreams::filtering_streambuf<boost::iostreams::input> in;
        in.push(boost::iostreams::gzip_decompressor());
        in.push(ifs);
        boost::archive::binary_iarchive ia(in);
        ia >> model;
    } catch (exception & e) {
        cerr << "Error: " << e.what() << endl;
        return false;
    }
    return true;
}

bool buildModel(Model& model, ProgramOptions& options) {
    if (options.isSet("load-model")) {
        if (!loadModel(model, options.get<string>("load-model"))) {
            return false;
        }
        model.setTrained(true);
    }
    model.setOptions(options);
    if (vout.level != ui::UIHandler::VerboseOutput::QUIET)
        model.getOptions().print();
    return true;
}

bool buildCorpus(Corpus& corpus, Corpus& test_corpus, ProgramOptions& options) {
    if (options.isSet("corpus") or options.isSet("train")) {
        if (options.isSet("corpus")) {
            vout << "[Reading corpus from '" << options.get<string>("corpus") << "']" << endl;
            if (!corpus.loadTaggedText(options.get<string>("corpus"), options.getSeparators(),
                    options.getFinalPunctuationSymbols(), options.get<unsigned int>("max-length"))) {
                cerr << "Could not open file; exiting." << endl;
                return false;
            }
            if (options.isSet("part-corpus")) {
                vout << "[Randomly selecting " << options.get<double>("part-corpus") * 100
                        << "\% of the corpus for training, and the remaining for testing]" << endl;
                corpus.splitRandomly(test_corpus, options.get<double>("part-corpus"));
                vout << "\tNumber of words/tags for training: " << corpus.getNumberOfWords() << endl;
                vout << "\tNumber of words/tags for testing: " << test_corpus.getNumberOfWords() << endl;
            }
        } else if (options.isSet("train")) {
            vector<string> files = options.get<vector<string> >("train");
            for (unsigned int i = 0; i < files.size(); i++) {
                vout << "[Reading training corpus from '" << files[i] << "']" << endl;
                if (!corpus.loadTaggedText(files[i], options.getSeparators(), options.getFinalPunctuationSymbols(),
                        options.get<unsigned int>("max-length"))) {
                    cerr << "Could not open file '" << files[i] << "'; exiting." << endl;
                    return false;
                }
            }
        }
        if (options.isSet("part-train")) {
            vout << "[Randomly selecting " << options.get<double>("part-train") * 100 << "\% of the training corpus]"
                    << endl;
            Corpus dummy;
            corpus.splitRandomly(dummy, options.get<double>("part-train"));
            vout << "\tNumber of words/tags for training: " << corpus.getNumberOfWords() << endl;
        }
        voutLow << "\tNumber of words: " << corpus.getNumberOfWords() << endl;
        voutLow << "\tNumber of tags: " << corpus.getNumberOfTags() << endl;
        voutLow << "\tDiscarded sentences longer than " << options.get<unsigned int>("max-length")
                << " words (and punctuation)." << endl;
        voutLow << "\tNumber of sentences: " << corpus.getNumberOfSentences() << endl;

        if (options.isSet("save-train")) {
            string filename = options.get<string>("save-train");
            if (options.isSet("save-id")) {
                filename = options.get<string>("save-id") + "." + filename;
            }
            corpus.saveTaggedText(filename, options.getSeparators());
        }
    }

    // Pre-loading testing file
    if (options.isSet("test")) {
        vector<string> files = options.get<vector<string> >("test");
        for (unsigned int i = 0; i < files.size(); i++) {
            vout << "[Reading testing corpus from '" << files[i] << "']" << endl;
            if (!test_corpus.loadTaggedText(files[i], options.getSeparators(), options.getFinalPunctuationSymbols(),
                    options.get<unsigned int>("max-length"))) {
                cerr << "Could not open file '" << files[i] << "'; exiting." << endl;
                return false;
            }
        }
        if (options.isSet("save-test")) {
            string filename = options.get<string>("save-test");
            if (options.isSet("save-id")) {
                filename = options.get<string>("save-id") + "." + filename;
            }
            test_corpus.saveTaggedText(filename, options.getSeparators());
        }
    } else if (options.isSet("tag")) {
        vout << "[Reading untagged text from '" << options.get<string>("tag") << "']" << endl;
        vector<string> files = options.get<vector<string> >("tag");
        for (unsigned int i = 0; i < files.size(); i++) {
            if (!test_corpus.loadUntaggedText(files[i], options.getFinalPunctuationSymbols(),
                    options.get<unsigned int>("max-length"))) {
                return false;
            }
        }
        if (options.isSet("save-test")) {
            string filename = options.get<string>("save-test");
            if (options.isSet("save-id")) {
                filename = options.get<string>("save-id") + "." + filename;
            }
            test_corpus.saveUntaggedText(filename);
        }
    }
    if (test_corpus.getNumberOfSentences() <= 0) {
        voutHigh
                << "[Warning: Testing corpus not specified or empty. Not going to parse anything, but training will continue now.]"
                << endl;
    }

    return true;
}

int main(int argc, char **argv) {
    ProgramOptions options(PROGRAM_NAME, VERSION, CONTACT);

    if (options.parseArguments(argc, argv) <= 0) {
        return -1;
    }
    voutLow.setOutputPriority(ui::UIHandler::VerboseOutput::LOW);
    voutHigh.setOutputPriority(ui::UIHandler::VerboseOutput::HIGH);
    if (options.isSet("quiet")) {
        vout.setLevel(ui::UIHandler::VerboseOutput::QUIET);
        voutLow.setLevel(ui::UIHandler::VerboseOutput::QUIET);
        voutHigh.setLevel(ui::UIHandler::VerboseOutput::QUIET);
    } else if (options.isSet("verbose")) {
        vout.setLevel(ui::UIHandler::VerboseOutput::VERBOSE);
        voutLow.setLevel(ui::UIHandler::VerboseOutput::VERBOSE);
        voutHigh.setLevel(ui::UIHandler::VerboseOutput::VERBOSE);
    }

    vout << "[Starting VLMM Tagger]" << endl;

    time_t start_time, stop_time;
    start_time = time(NULL);

    Corpus train_corpus, test_corpus;
    if (!buildCorpus(train_corpus, test_corpus, options)) {
        return -1;
    }

    Model model;
    if (!buildModel(model, options)) {
        return -1;
    }

    if (model.needsTraining()) {
        if (!model.hasData()) {
            if (train_corpus.hasData()) {
                bool trained = model.train(train_corpus);
                if (!trained)
                    return -1;
            } else {
                cerr << "Error: no data available for training a model." << endl;
                return -1;
            }
        }
    }

    if (options.isSet("save-model")) {
        string filename = options.get<string>("save-model");
        if (options.isSet("save-id")) {
            filename = options.get<string>("save-id") + "." + filename;
        }
        if (!saveModel(model, filename)) {
            return -1;
        }
    }

    /*
     if (args.count_tags) {
     string file_name = args.corpus_file ? args.corpus_file_name : args.train_file_name;
     string file_name_tags = file_name + ".tag_count.info";
     WriteTextToFile(file_name_tags, ToString(ReverseCountTags(sentences_tags)));
     string file_name_words = file_name + ".word_count.info";
     WriteTextToFile(file_name_words, ToString(model.getWordsByFrequency(50), ":", "\n"));
     }*/

    // Tagging
    if (test_corpus.hasData()) {
        vout << "[Tagging text]" << endl;
        voutLow << "Number of words: " << test_corpus.getNumberOfWords() << endl;

        DecoderByViterbi viterbi(model);
        vector<vector<string> > tags_out;
        vout << "[Viterbi Algorithm: calculating...]" << endl;
        time_t vit_start_time = time(NULL);
        tags_out = viterbi.tag(test_corpus.getSentencesWords());
        time_t vit_stop_time = time(NULL);
        vout << "[Algorithm of Viterbi: [OK] in " << difftime(vit_stop_time, vit_start_time) << " seconds.]" << endl;

        Corpus tagged_corpus(test_corpus.getSentencesWords(), tags_out);
        vout << "Number of words/tags: " << test_corpus.getNumberOfWords() << "/" << tagged_corpus.getNumberOfTags()
                << endl;
        string taggedfilename;
        if (options.isSet("save-id")) {
            taggedfilename = options.get<string>("save-id") + ".";
        }
        if (options.isSet("save")) {
            taggedfilename += options.get<string>("save");
        } else if (options.isSet("corpus")) {
            taggedfilename += options.get<string>("corpus");
            taggedfilename += ".tagged";
        } else if (options.isSet("test") and options.get<vector<string> >("test").size() == 1) {
            taggedfilename += options.get<vector<string> >("test")[0];
            taggedfilename += ".tagged";
        } else {
            taggedfilename += "test";
            taggedfilename += ".tagged";
        }
        vout << "[Saving tagged text to '" << taggedfilename << "'.]" << endl;
        tagged_corpus.saveTaggedText(taggedfilename, options.getSeparators());

        //UpdateTags(tags_in);
        set<string> known_words = model.getKnownWords();
        set<string> ambiguous_words = model.getAmbiguousWords();
        voutLow << "Number of known words: " << known_words.size() << endl;

        cout << setfill('-') << setw(80) << "-" << endl;
        cout << setfill(' ');
        if (test_corpus.isTagged()) {
            string errors_file_name =
                    options.isSet("save-id") ? options.get<string>("save-id") + ".errors.info" : "errors.info";
            long double accuracy = CalculateAccuracy(test_corpus.getSentencesTags(), tags_out,
                    test_corpus.getSentencesWords(), known_words, ambiguous_words, errors_file_name);
            voutHigh << "[Accuracy: " << accuracy * 100 << "% correct tagging]" << endl;
        }
    }
    cout << setfill('-') << setw(80) << "-" << endl;
    cout << setfill(' ');
    stop_time = time(NULL);
    voutHigh << "Total time of execution: " << difftime(stop_time, start_time) << " seconds" << endl;
    for (int i = 0; i < argc; i++)
        vout << argv[i] << " ";
    vout << endl;
    cout << setfill('-') << setw(80) << "-" << endl;
    cout << setfill(' ');
    //cin.get();

    return 1;
}

