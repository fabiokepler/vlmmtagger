/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2014 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

/*
 * DecodeViterbi.cpp
 *
 *  Created on: Aug 21, 2008
 *      Author: kepler
 */

#include <cassert>
#include "DecoderByViterbi.h"

extern ui::UIHandler::VerboseOutput vout, voutHigh, voutLow;

vector<vector<string> > DecoderByViterbi::tag(const vector<vector<string> >& sents_words) {
    unsigned int S = sents_words.size();
    vector<vector<string> > tags_out(S);
    int prog = S > 20 ? S / 20 : 1;

    voutLow << "Number of sentences: " << S << endl;
    for (unsigned int s = 0; s < S; s++) {
        if (s % prog == 0) {
            vout << "\r" << int((s + 1) * 100 / S) << "%";
            cout.flush();
        }
        tags_out[s] = getMostLikelyTags(sents_words[s]);
    }

    return tags_out;
}

/**
 * @brief Return the sequence of tags with maximum probability given @a words.
 * Uses the Viterbi algorithm.
 */
vector<string> DecoderByViterbi::getMostLikelyTags(const vector<string>& words) {
    if (words.empty())
        return vector<string>();

    map<string, map<deque<string>, path_pointer_t> > current_best_paths; // [tag] = state (sequence of tags) leading to such tag
    deque<path_t> previous_paths;

    path_t p0;
    p0.log_probability = 0.0;
    p0.tag_sequence = deque<string>();
    previous_paths.push_back(p0);

    unsigned int n = words.size();
    for (unsigned int t = 0; t < n; t++) {
        vector<string> candidate_tags = model_.getCandidateTagsForWord(words[t]);
        for (vector<string>::iterator it_pt = candidate_tags.begin(); it_pt != candidate_tags.end(); it_pt++) {
            string candidate_tag = (*it_pt);
            for (auto it_prev = previous_paths.begin(); it_prev != previous_paths.end(); it_prev++) {
                deque<string>* path = &(*it_prev).tag_sequence;
                long double path_prob = (*it_prev).log_probability;
                deque<string> used_context;
                long double log_prob;
                if (model_.getOptions().isIgnoringPunctuation()) {
                    log_prob = model_.getLogProbabilityAndContextUsed(candidate_tag, words[t], model_.getFilteredContext(*path), used_context);
                } else {
                    log_prob = model_.getLogProbabilityAndContextUsed(candidate_tag, words[t], *path, used_context);
                }
                log_prob += path_prob;

                map<deque<string>, path_pointer_t>::iterator itc = current_best_paths[candidate_tag].find(used_context);
                if (itc == current_best_paths[candidate_tag].end()) { // No previous path leading to #candidate_tag
                    current_best_paths[candidate_tag][used_context].log_probability = log_prob;
                    current_best_paths[candidate_tag][used_context].tag_sequence = path;
                } else if (log_prob >= (*itc).second.log_probability) { // Found a better path to #candidate_tag
                    (*itc).second.log_probability = log_prob;
                    (*itc).second.tag_sequence = path;
                }
            }
        }
        deque<path_t> next_paths;
        for (auto it1 = current_best_paths.begin(); it1 != current_best_paths.end(); it1++) {
            string tag = (*it1).first;
            for (auto it2 = (*it1).second.begin(); it2 != (*it1).second.end(); it2++) {
                path_t path;
                path.log_probability = (*it2).second.log_probability;
                path.tag_sequence = *(*it2).second.tag_sequence;
                path.tag_sequence.push_front(tag);
                next_paths.push_back(path);
            }
        }
        previous_paths.clear();
        previous_paths = next_paths;
        current_best_paths.clear();
    }

    long double max_prob = -numeric_limits<long double>::infinity();
    deque<string>* max_sequence = NULL;
    for (auto it_prev = previous_paths.begin(); it_prev != previous_paths.end(); it_prev++) {
        long double path_prob = (*it_prev).log_probability;
        if (path_prob >= max_prob) {
            max_prob = path_prob;
            max_sequence = &(*it_prev).tag_sequence;
        }
    }
    if (max_sequence != NULL)
        return vector<string>(max_sequence->rbegin(), max_sequence->rend());
    return vector<string>();
}
