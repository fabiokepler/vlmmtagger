/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2013 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
******************************************************************************/

/* 
 * File:   Corpus.h
 * Author: kepler
 *
 * Created on November 23, 2011, 11:55 AM
 */

#ifndef CORPUS_H
#define	CORPUS_H

#include <vector>
#include <string>
#include <set>
#include <map>


using namespace std;

class Corpus {
public:
    Corpus();
    Corpus(const vector< vector<string> >& sentences_words, const vector< vector<string> >& sentences_tags);
    virtual ~Corpus();

    void setFinalPunctuationSymbols(const set<string> _final_punctuation_symbols) {
        this->final_punctuation_symbols_ = _final_punctuation_symbols;
    }

    const set<string> getFinalPunctuationSymbols() const {
        return final_punctuation_symbols_;
    }

    void setSeparators(const map<unsigned int, string> _separators) {
        this->separators_ = _separators;
    }

    const map<unsigned int, string> getSeparators() const {
        return separators_;
    }
    
    /**
     * Set the sentences of words and tags of the current tagged corpus.
     * @param sentences_words
     * @param sentences_tags
     */
    bool setSentences(vector<vector<string> >& sentences_words, vector<vector<string> >& sentences_tags);

    /**
     * Set the sentences of words of the current untagged corpus.
     * @param sentences_words
     */
    bool setSentences(vector<vector<string> >& sentences_words);

    /**
     * Append sentences of words and tags to current tagged corpus.
     * @param sentences_words
     * @param sentences_tags
     */
    bool addSentences(vector<vector<string> >& sentences_words, vector<vector<string> >& sentences_tags);
    
    /**
     * Append sentences of words to current untagged corpus.
     * @param sentences_words
     */
    bool addSentences(vector<vector<string> >& sentences_words);

    /**
     * Select @percentage_to_keep sentences randomly, and @return remaining sentences as another corpus.
     */
    bool splitRandomly(Corpus& second_corpus, double percentage_to_keep);

    bool loadTaggedText(string filename, map<unsigned int, string> separators, set<string> final_punctuation_symbols, unsigned int max_sentence_size);

    bool loadUntaggedText(string filename, set<string> final_punctuation_symbols, unsigned int max_sentence_size);

    bool saveTaggedText(string filename, map<unsigned int, string> separators);

    bool saveUntaggedText(string filename);

    bool hasData() const {
        return getNumberOfSentences() > 0;
    }

    bool isTagged() const {
        return is_tagged;
    }

    unsigned int getNumberOfSentences() const {
        return number_of_sentences;
    }

    unsigned int getNumberOfTags() const {
        return number_of_tags;
    }

    unsigned int getNumberOfWords() const {
        return number_of_words;
    }

    vector<vector<string> > getSentencesTags() const {
        return sentences_tags_;
    }

    vector<vector<string> > getSentencesWords() const {
        return sentences_words_;
    }

private:
    void init();
    void count() {
        number_of_sentences = sentences_words_.size();
        number_of_words = number_of_tags = 0;
        for (unsigned int s = 0; s < sentences_words_.size(); s++) {
            number_of_words += sentences_words_[s].size();
        }
        for (unsigned int s = 0; s < sentences_tags_.size(); s++) {
            number_of_tags += sentences_tags_[s].size();
        }
    }

    vector< vector<string> > sentences_tags_, sentences_words_;
    unsigned int number_of_words, number_of_tags, number_of_sentences;
    bool is_tagged;

    map<unsigned int, string> separators_;
    set<string> final_punctuation_symbols_;
};

#endif	/* CORPUS_H */

