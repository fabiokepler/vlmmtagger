/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2014 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include "ContextTree.h"

extern ui::UIHandler::VerboseOutput vout, voutHigh, voutLow;

ContextTree::ContextTree() {
    // Setting parameters.
    minimum_times_to_be_in_tree_ = 0;
    // Initializing
    init();
}
ContextTree::ContextTree(int min_times) {
    // Setting parameters.
    minimum_times_to_be_in_tree_ = min_times;
    // Initialization
    init();
}
void ContextTree::init() {
    // Initialization
    root_.probability = 0.0;
    root_.count = 0.0;
    root_.depth = 0;
    root_.is_final_leaf = false;
    root_.parent = NULL;

    deepest_level_number_ = 0;
    star_node_ = "*";

    cut_value_ = -1.0;
}

ContextTree::~ContextTree() {
}

// Private //

ContextTree::sprout ContextTree::newSprout() {
    sprout new_sprout;
    new_sprout.count = 0.0;
    return new_sprout;
}

ContextTree::sprout ContextTree::newSprout(long double initial_prob) {
    sprout new_sprout;
    new_sprout.count = initial_prob;
    return new_sprout;
}

ContextTree::node ContextTree::newNode() {
    node new_node;
    new_node.count = 0.0;
    new_node.depth = -1;
    new_node.is_final_leaf = false;
    new_node.parent = NULL;

    return new_node;
}

// Public //

void ContextTree::setMinimumTimesForBranchesToBeInTree(int min_times) {
    minimum_times_to_be_in_tree_ = min_times;
}

void ContextTree::setCutValue(long double K) {
    cut_value_ = K;
}

/**
 * 'nodes' contains a reversed sequence of tags occurring in the training corpus.
 * The first node in 'nodes' is the destination, and the remaining, the past
 * (from most recent to oldest).
 */
//void ContextTree::addBranch(deque<string> nodes) {
//	if (nodes.empty())
//		return;
//	string cur_sprout = nodes[0];
//	nodes.pop_front();
//	addBranch(cur_sprout, nodes);
//}
/**
 * 'branch' is a sequence of tags that is the context of 'dest_sprout';
 * tags in 'branch' are added, from position 0 to n-1, from the nearest
 * to the farthest.
 */
void ContextTree::addBranch(const string& dest_sprout, const deque<string>& branch) {
    // FIXME: check why adding every sprout to root does not work (results get worse).
    // FIXME: maybe there is a need in distinguishing between all sprouts in the root and sprouts that begin sentences (must create a special symbol).
    // FIXME: but I recall that the root sprouts were being used as a tagsCounter. Must check this.
    // FIXME: 2014/01/31: results got better by adding every sprout to the root: from 96.3191 to 96.3195 (97.1014 to 97.1018 in known words); one ambiguous word gets correctly tagged.

    if (root_.sprouts.count(dest_sprout) == 0) {
        root_.sprouts[dest_sprout] = newSprout();
    }
    root_.sprouts[dest_sprout].count++;
    root_.count++;

    if (branch.empty()) {
        return;
    }

    node* parent = &root_;
    node* curNode = NULL;
    for (unsigned int i = 0; i < branch.size(); i++) {
        if (parent->branches.count(branch[i]) == 0) {
            parent->branches[branch[i]] = newNode();
            curNode = &parent->branches[branch[i]];
            curNode->name = branch[i];
            curNode->parent = parent;
            curNode->depth = parent->depth + 1;
        } else {
            curNode = &parent->branches[branch[i]];
        }
        curNode->count++;

        // Add sprout and increment probability (counter).
        if (curNode->sprouts.count(dest_sprout) == 0) {
            curNode->sprouts[dest_sprout] = newSprout();
        }
        curNode->sprouts[dest_sprout].count++;

        parent = curNode;
    }
}

/*
 void ContextTree::addBranch(const string& dest_sprout,
 const deque<string>& branch, const deque<bool>& ambiguity) {
 if (branch.empty()) {
 (*(root_.sprouts.insert(pair<string, sprout>(dest_sprout, newSprout()))).first).second.count++;
 root_.count++;
 root_.probability++;
 return;
 }
 node* father = &root_;
 node* new_node;
 unsigned int n = branch.size();
 for (unsigned int i = 0; i < n; i++) {
 new_node = &((*(father->branches.insert(
 pair<string, node>(branch[i], newNode()))).first).second);
 new_node->count++;
 new_node->depth = i + 1;
 new_node->father = father;
 new_node->name = branch[i];
 // Add sprout and increment probability (counter).
 (*(new_node->sprouts.insert(
 pair<string, sprout>(dest_sprout, newSprout()))).first).second.count++;

 father = new_node;
 }

 if (branch.size() != ambiguity.size()) {
 return;
 }

 // Insert star nodes
 bool seen_first_star = false;
 father = &root_;
 for (unsigned int i = 0; i < n; i++) {
 if (ambiguity[i]) {
 new_node = &((*(father->branches.insert(
 pair<string, node>(star_node_, newNode()))).first).second);
 new_node->count++;
 new_node->depth = i + 1;
 new_node->father = father;
 new_node->name = star_node_;
 // Add sprout and increment probability (counter).
 (*(new_node->sprouts.insert(
 pair<string, sprout>(dest_sprout, newSprout()))).first).second.count++;

 father = new_node;
 seen_first_star = true;
 } else {
 if (!seen_first_star) {
 new_node = &((*father->branches.find(branch[i])).second);
 father = new_node;
 } else {
 new_node =
 &((*(father->branches.insert(
 pair<string, node>(branch[i], newNode()))).first).second);
 new_node->count++;
 new_node->depth = i + 1;
 new_node->father = father;
 new_node->name = branch[i];
 // Add sprout and increment probability (counter).
 (*(new_node->sprouts.insert(
 pair<string, sprout>(dest_sprout, newSprout()))).first).second.count++;

 father = new_node;
 }
 }
 }
 }
 */

void ContextTree::createBranch(const string& dest_sprout, const deque<string>& branch,
        long double initial_sprout_prob) {
    if (branch.empty()) {
//        (*(_root.sprouts.insert(pair<string, sprout>(dest_sprout, newSprout()))).first).second.count++;
        root_.sprouts.insert(pair<string, sprout>(dest_sprout, newSprout(initial_sprout_prob)));
        root_.count += initial_sprout_prob;
        root_.probability += initial_sprout_prob;
        return;
    }

    map<string, node>::iterator it_node;
    it_node = (root_.branches.insert(pair<string, node>(branch[0], newNode()))).first;
    root_.count += initial_sprout_prob;
    node* father = &root_;
    for (unsigned int i = 0; i < branch.size(); i++) {
        (*it_node).second.count += initial_sprout_prob;
        (*it_node).second.depth = i + 1;
        (*it_node).second.parent = father;
        (*it_node).second.name = branch[i];
        // Add sprout and increment probability (counter).
        (*it_node).second.sprouts.insert(pair<string, sprout>(dest_sprout, newSprout(initial_sprout_prob)));
        if (i < branch.size() - 1) {
            father = &(*it_node).second;
            // Insert a descendant node.
            it_node = ((*it_node).second.branches.insert(pair<string, node>(branch[i + 1], newNode()))).first;
        } else { //is leaf
            //(*it_node).second.probability++;
        }
    }
}

// Same method as above, but here we have the word w_i, its tag t_i, and the context t_{i-1, -\infty}.
// The tags (from t_i to t_-\infty) are in this order in 'nodes' (from position 0 to ...).
/*
 void ContextTree::addBranch(deque<string> nodes, string word)
 {
 map<string, node>::iterator it_node;
 it_node = (_root.branches.insert(pair<string, node>(word, newNode()))).first;
 _root.probability++;
 if (nodes.empty()) return;
 string cur_sprout = nodes[0];
 nodes.pop_front();
 node * father = &_root;
 for (unsigned int i = 0; i < nodes.size(); i++) {
 (*it_node).second.probability++;
 (*it_node).second.depth = i + 1;
 (*it_node).second.father = father;
 // Add sprout and increment probability (counter).
 (*((*it_node).second.sprouts.insert(pair<string, sprout>(cur_sprout, newSprout()))).first).second.probability++;
 if (i < nodes.size() - 1) {
 father = &(*it_node).second;
 // Insert a descendant node.
 it_node = ((*it_node).second.branches.insert(pair<string, node>(nodes[i], newNode()))).first;
 } else { //is leaf
 //(*it_node).second.probability++;
 }
 }
 } // */

void ContextTree::normalizeNode(node* ni) {
    if (ni == NULL)
        return;
    node* cur_node = ni;
    for (auto its = cur_node->sprouts.begin(); its != cur_node->sprouts.end(); its++) {
        (*its).second.probability = (*its).second.count / cur_node->count;
    }
    if (cur_node->parent != NULL) {
        cur_node->probability = cur_node->count / cur_node->parent->count;
    } else {
        cur_node->probability = 1.0;
    }
}

/**
 * @brief Remove nodes occurring less times than the specified density (#_minimum_times_to_be_in_tree).
 */
void ContextTree::shake(bool apply_to_sprouts) {
    node* cur_node;
    deque<node*> node_queue;
    node_queue.push_back(&root_);
    long double smallest_frequency = 1000000.0, biggest_frequency = 0.0;
    // Going Breadth First down the tree.
    // Cut off rare nodes.
    while (!node_queue.empty()) {
        cur_node = node_queue.front();
        node_queue.pop_front();
        smallest_frequency = min(smallest_frequency, cur_node->count);
        biggest_frequency = max(biggest_frequency, cur_node->count);
        if (cur_node->count < minimum_times_to_be_in_tree_) {
            if (cur_node->parent == NULL) { // so cur_node is the root
                //delete cur_node;
                cur_node->branches.clear();
                return;
            }
            cur_node->parent->branches.erase(cur_node->name);
        } else {
            if (apply_to_sprouts) {
                vector<decltype(cur_node->sprouts.begin())> erasables;
                for (auto its = cur_node->sprouts.begin(); its != cur_node->sprouts.end(); its++) {
                    if ((*its).second.count < minimum_times_to_be_in_tree_) {
                        cur_node->count -= (*its).second.count;
                        erasables.push_back(its);
                    }
                }
                for (unsigned int i = 0; i < erasables.size(); i++) {
                    cur_node->sprouts.erase(erasables[i]);
                }
            }
            if (cur_node->sprouts.empty() && cur_node->parent != NULL) {
                cur_node->parent->branches.erase(cur_node->name);
            } else {
                for (auto itb = cur_node->branches.begin(); itb != cur_node->branches.end(); itb++) {
                    node_queue.push_back(&(*itb).second);
                }
            }
        }
    }
    voutLow << name_ << "'s smallest frequency: " << smallest_frequency << endl;
    voutLow << name_ << "'s biggest frequency: " << biggest_frequency << endl;
}

void ContextTree::normalizeProbabilities() {
    node* cur_node;
    deque<node*> node_queue;
    node_queue.push_back(&root_);
    // Normalize.
    while (!node_queue.empty()) {
        cur_node = node_queue.front();
        node_queue.pop_front();
        for (auto its = cur_node->sprouts.begin(); its != cur_node->sprouts.end(); its++) {
            (*its).second.probability = (*its).second.count / cur_node->count;
        }
        if (cur_node->parent != NULL) {
            cur_node->probability = cur_node->count / cur_node->parent->count;
        } else {
            cur_node->probability = 1.0;
        }
        for (auto itb = cur_node->branches.begin(); itb != cur_node->branches.end(); itb++) {
            node_queue.push_back(&(*itb).second);
        }
    }

    /*
     map<string, sprout>::iterator it_sprout;
     for (it_sprout = _root.sprouts.begin(); it_sprout != _root.sprouts.end(); it_sprout++) {
     (*it_sprout).second.probability = (*it_sprout).second.count / _root.count;
     }

     map<string, node>::iterator it_roots;
     for (it_roots = _root.branches.begin(); it_roots != _root.branches.end(); it_roots++) {
     //deque<string> new_root;
     deque< map<string, node>::iterator > node_queue;
     node_queue.push_back(map<string, node>::iterator(it_roots));

     // Going Breadth First down the tree.
     while (!node_queue.empty()) {
     map<string, node>::iterator cur_element = node_queue.front();
     node_queue.pop_front();

     (*cur_element).second.probability = (*cur_element).second.count / (*cur_element).second.father->count;

     // Normalize: P(X|Y) = N(Y,X) / N(Y).
     map<string, sprout>::iterator it_sprout;
     for (it_sprout = (*cur_element).second.sprouts.begin(); it_sprout != (*cur_element).second.sprouts.end(); it_sprout++) {
     //?if ((*it_sprout).second.probability >= _minimum_times_to_be_in_tree) {
     (*it_sprout).second.probability = (*it_sprout).second.count / (*cur_element).second.count;
     //?}
     }

     // Update queue.
     map<string, node>::iterator it_node;
     for (it_node = (*cur_element).second.branches.begin(); it_node != (*cur_element).second.branches.end();) {
     if ((*it_node).second.count < _minimum_times_to_be_in_tree) {
     map<string, node>::iterator it_next = it_node;
     it_next++;
     (*cur_element).second.branches.erase(it_node);
     it_node = it_next;
     } else {
     node_queue.push_back(map<string, node>::iterator(it_node));
     it_node++;
     }
     }
     }
     }
     */
}

// FIXME: there is a bug here: when K is too big (1000 or 2500) prune stops with a segfault.
/**
 * @brief Remove nodes if the KL Divergence with respect to their parents is less than the cut value (#getCutValue()).
 *
 * This implementation traverses the tree in a bottom-up manner.
 * Gives a slightly different result than the top-down approach.
 */
/*
 bool ContextTree::prune() {
 vout << "[Prunning Nodes of " << getName() << " Tree]" << endl;

 long double cut_value = getCutValue();
 bool is_being_pruned = true;
 int times_pruned = 0, times_not_pruned = 0;
 while (is_being_pruned) {
 is_being_pruned = false;
 deque<node*> leaves = getLeafNodes();
 voutLow << "Leaves: " << leaves.size() << endl;
 for (unsigned int i = 0; i < leaves.size(); i++) {
 if (!leaves[i]->is_final_leaf) {
 long double delta = 0.0;
 map<string, sprout>::iterator it_sprout;
 for (it_sprout = leaves[i]->sprouts.begin(); it_sprout != leaves[i]->sprouts.end(); it_sprout++) {
 long double cut_branch_prob = logl(leaves[i]->father->sprouts[(*it_sprout).first].count)
 - logl(leaves[i]->father->count);
 long double sprout_prob = logl((*it_sprout).second.count) - logl(leaves[i]->count);
 long double product = getKLDivergence(sprout_prob, cut_branch_prob);
 product *= leaves[i]->count; // Weighted divergence
 delta += product;
 }
 // Decide pruning or not.
 if (delta < cut_value) { // prune
 leaves[i]->father->branches.erase(leaves[i]->name);
 // Warning: now leaves[i] is invalid for the rest of this iteration.
 times_pruned++;
 is_being_pruned = true;
 } else {
 // Leave this branch on the tree.
 // #is_final_leaf prevents the loop from analyzing this branch again.
 leaves[i]->is_final_leaf = true;
 times_not_pruned++;
 }
 }
 }
 }
 voutLow << "\tTimes pruned: " << times_pruned << " Times not pruned: " << times_not_pruned << endl;

 return true;
 }
 */

/**
 * @brief Remove nodes if the KL Divergence with respect to their parents is less than the cut value (#getCutValue()).
 *
 * This implementation traverses the tree in a top-down manner.
 * Gives a slightly different result than the bottom-up approach.
 */
bool ContextTree::prune() {
    vout << "[Prunning " << getName() << " Tree]" << endl;

    int times_pruned = 0, times_not_pruned = 0;

    stack<node*> nodes_stack;
    nodes_stack.push(&root_);
    node* cur_node;
    while (!nodes_stack.empty()) {
        cur_node = nodes_stack.top();
        nodes_stack.pop();

        // Copy subnodes references in order to be able to erase them from cur_node.branches (i.e., we can't iterate over cur_node.branches and erase a branch).
        deque<node*> subnodes;
        for (auto it_subnodes = (*cur_node).branches.begin(); it_subnodes != (*cur_node).branches.end();
                it_subnodes++) {
            subnodes.push_back(&(*it_subnodes).second);
        }
        for (unsigned int i = 0; i < subnodes.size(); i++) {
            node* subnode = subnodes[i];
            long double delta = 0.0;
            for (map<string, sprout>::iterator it_sprout = (*subnode).sprouts.begin();
                    it_sprout != (*subnode).sprouts.end(); it_sprout++) {
                long double parent_sprout_prob = logl((*cur_node).sprouts[(*it_sprout).first].count)
                        - logl((*cur_node).count);
                long double child_sprout_prob = logl((*it_sprout).second.count) - logl((*subnode).count);
                long double product = getKLDivergence(child_sprout_prob, parent_sprout_prob);
                product *= (*subnode).count; // Weighted divergence
                delta += product;
            }
            // Decide pruning or not.
            if (delta < getCutValue()) { // prune
                (*cur_node).branches.erase((*subnode).name);
                times_pruned++;
            } else {
                // Leave this node on the tree.
                nodes_stack.push(subnode);
                times_not_pruned++;
            }
        }
        if ((*cur_node).branches.empty()) {
            (*cur_node).is_final_leaf = true;
        }
    }
    voutLow << "\tTimes pruned: " << times_pruned << " Times not pruned: " << times_not_pruned << endl;
    return true;
}

long double ContextTree::getCutValue() {
// C > 2|X|+4 (? appearing to be not correct.)
// K = Kn ~ C * log(n)
// So considering some other constant.

    long double Kn = 0.0;
    long double C = getAverageSentenceSize();
    if (cut_value_ >= 0.0) {
        Kn = cut_value_;
    } else {
        Kn = ((long double) log((long double) getInputSize()) / (long double) log((long double) getNumberOfTags()) * C);
    }
//Kn = Kn * 20.0;
//    voutLow << "Kn = " << Kn << " (C: " << C << " |words|: " << getInputSize() << " |tags|: " << getNumberOfTags()
//            << " log(|words|)/log(|tags|): "
//            << (long double) (log((long double) getInputSize()) / log((long double) getNumberOfTags())) << endl;

    return Kn;
}

/**
 * if cut_value < 0, use getCutValue().
 */
bool ContextTree::pruneSprouts(long double cut_value) {
    vout << "[Prunning Sprouts of " << name_ << " Tree]" << endl;

    if (cut_value < 0.0)
        cut_value = getCutValue();
    bool is_being_pruned = true;
    while (is_being_pruned) {
        is_being_pruned = false;
        int times_pruned = 0, times_not_pruned = 0;
        long double max_count = 0;
        node* max_node = &root_;

        deque<node*> leaves = getLeafNodes();
        for (unsigned int i = 0; i < leaves.size(); i++) {
            if (!leaves[i]->is_final_leaf && leaves[i] != &root_) {
                long double delta = 0.0;
                vector<decltype(leaves[i]->sprouts.begin())> erasables;
                for (auto it_sprout = leaves[i]->sprouts.begin(); it_sprout != leaves[i]->sprouts.end(); it_sprout++) {
                    auto it_parents_sprout = leaves[i]->parent->sprouts.find((*it_sprout).first);
                    if (it_parents_sprout != leaves[i]->parent->sprouts.end()) {
                        /*
                         long double parents_sprout_prob = (*it_parents_sprout).second.probability;
                         //						if (parents_sprout_prob == 0.0) {
                         //							vout.setOutputPriority(VerboseOutput::MEDIUM);
                         //							cerr << "Sprout: " << (*it_sprout).first
                         //								 << ", prob = " << (*it_sprout).second.probability
                         //								 << "; Cut branch prob: " << parents_sprout_prob << "; count: " << leaves[i]->father->sprouts[(*it_sprout).first].count << "; Depth: " << leaves[i]->father->depth << endl;
                         //						}
                         long double log_value = 0.0;
                         long double product = 0.0;
                         if (algoParam.use_perceptron) { // Perceptron already uses log in all probabilities.
                         log_value = (*it_sprout).second.probability - parents_sprout_prob;
                         product = expl((*it_sprout).second.probability) * log_value;// * leaves[i]->count;
                         } else {
                         log_value = logl((*it_sprout).second.probability / parents_sprout_prob);
                         product = (*it_sprout).second.probability * log_value;// * leaves[i]->count;
                         }
                         */
                        // THIS IS A TEST! NOT FORMALY SPECIFIED.
                        //product *= (1.0 + ((*cur_node).second.depth / algoParam.contextTree_max_order));
                        long double product = getKLDivergence((*it_sprout).second.probability,
                                (*it_parents_sprout).second.probability);

                        if (leaves[i]->count > max_count) {
                            max_count = leaves[i]->count;
                            max_node = leaves[i];
                        }
                        delta = product;
                        //cerr << "Delta: " << delta << endl;

                        // Decide pruning or not.
                        if (delta < cut_value) { // prune
                            erasables.push_back(it_sprout);
                            times_pruned++;
                            is_being_pruned = true;

                            // FIXME: TESTING
                            node* l1 = leaves[i]->parent;
                            node* l2 = l1->parent;
                            while (l1 != NULL && l2 != NULL && l1 != &root_ && l2 != &root_) {
                                string s = (*it_sprout).first;
                                if (l1->sprouts.find(s) == l1->sprouts.end()) {
                                    cerr << "***UE, NULL?" << endl;
                                }
                                long double kl = getKLDivergence(l1->sprouts[s].probability,
                                        l2->sprouts[s].probability);
                                if (kl < cut_value) {
                                    l1->sprouts.erase(s);
                                    l1 = l2;
                                    l2 = l1->parent;
                                } else {
                                    l1 = NULL;
                                }
                            }
                        } else {
                            // Leave this branch on the tree.
                            // is_final_leaf prevents the loop from analysing this branch again.
                            //leaves[i]->is_final_leaf = true;
                            times_not_pruned++;
                        }
                    } else {
//                		cerr << "Parent doesn't have same sprout." << endl;
                    }
                }
                for (unsigned int e = 0; e < erasables.size(); e++) {
                    leaves[i]->count -= (*erasables[e]).second.count;
                    leaves[i]->sprouts.erase(erasables[e]);
                }
//*
//                node* parent = NULL;
                if (leaves[i]->sprouts.empty()) {
                    //cerr << "Erasing node" << endl;
//                	parent = leaves[i]->father;
                    leaves[i]->parent->branches.erase(leaves[i]->name);
                }
//                if (parent != NULL) {
//                	parent->father->branches.erase(parent->name);
//                }
// */
            }
        }
        voutLow << "\tTimes pruned: " << times_pruned << " Times not pruned: " << times_not_pruned << endl;
        if (is_being_pruned) {
            voutLow << "\tMax branch count: " << max_count << " with size: " << max_node->depth << endl;
        }
    }

    return true;
}

void ContextTree::cutShortBranches(unsigned int min_size) {
    min_size = max(min_size, getMinimumBranchSize());
    if (min_size == 0)
        return;

    vout << "[Cutting Short Branches of " << name_ << " Tree]" << endl;

    map<node*, bool> being_cut;

    bool is_being_cut = true;
    while (is_being_cut) {
        is_being_cut = false;
        int times_cut = 0;
        stack<node*> nodes_stack;
        nodes_stack.push(&root_);
        node* cur_node;
        while (!nodes_stack.empty()) {
            cur_node = nodes_stack.top();
            nodes_stack.pop();
            if (cur_node->depth < (int) min_size) {
                if ((*cur_node).branches.empty()) { // No children.
                    cur_node->parent->branches.erase(cur_node->name);
                    is_being_cut = true;
                    times_cut++;
                } else {
                    for (auto it_node = (*cur_node).branches.rbegin(); it_node != (*cur_node).branches.rend();
                            it_node++) {
                        nodes_stack.push(&(*it_node).second);
                    }
                }
            }
        }
        voutLow << "\tTimes cut (size < " << min_size << "): " << times_cut << endl;
    }
    return;
}

/**
 * @brief Get Kullback-Leibler divergence of two log probabilities.
 *
 * This is a pseudo-KL Divergence, since it does not include the sum over 't'.
 */
long double ContextTree::getKLDivergence(long double logprob_uv, long double logprob_v) {
    return expl(logprob_uv) * (logprob_uv - logprob_v); // * leaves[i]->count;
//    return prob_uv * logl(prob_uv / prob_v); // * leaves[i]->count;
}

///////////////////////////////////////////////////////////////////////

/**
 * Reset context tree.
 * Counts are set to <initial_count> and probabilities to 0.
 */
long double ContextTree::reset(long double initial_count) {
//	vout.setOutputPriority(VerboseOutput::LOW);
//	vout << "[Resetting " << _name << " Context Tree]" << endl;
    map<string, node>::iterator it_node;
    stack<node*> nodes_stack;

    /*
     for (it_node = _root.branches.begin(); it_node != _root.branches.end(); it_node++) {
     if (!(*it_node).second.branches.empty()) {
     nodes_stack.push(it_node);
     }
     }*/
    nodes_stack.push(&root_);
    node* cur_node;
    while (!nodes_stack.empty()) {
        cur_node = nodes_stack.top();
        nodes_stack.pop();

        (*cur_node).count = initial_count * (*cur_node).sprouts.size();
        (*cur_node).probability = 0.0;

        map<string, sprout>::iterator it_sprout;
        for (it_sprout = (*cur_node).sprouts.begin(); it_sprout != (*cur_node).sprouts.end(); it_sprout++) {
            (*it_sprout).second.count = initial_count;
            (*it_sprout).second.probability = 0.0;
        }
        for (it_node = (*cur_node).branches.begin(); it_node != (*cur_node).branches.end(); it_node++) {
            nodes_stack.push(&(*it_node).second);
        }
    }

    return initial_count;
}

/**
 * Reset probabilities to <initial_probability>.
 * Leave counts unaltered.
 */
long double ContextTree::resetProbabilities(long double initial_probability) {
    map<string, node>::iterator it_node;
    stack<node*> nodes_stack;

    nodes_stack.push(&root_);
    node* cur_node;
    while (!nodes_stack.empty()) {
        cur_node = nodes_stack.top();
        nodes_stack.pop();

        //(*cur_node).count = initial_probability * (*cur_node).sprouts.size();
        (*cur_node).probability = initial_probability;

        map<string, sprout>::iterator it_sprout;
        for (it_sprout = (*cur_node).sprouts.begin(); it_sprout != (*cur_node).sprouts.end(); it_sprout++) {
            //(*it_sprout).second.count = initial_count;
            (*it_sprout).second.probability = initial_probability;
        }
        for (it_node = (*cur_node).branches.begin(); it_node != (*cur_node).branches.end(); it_node++) {
            nodes_stack.push(&(*it_node).second);
        }
    }

    return initial_probability;
}

/**
 * Sets leaves->is_final_leaf to either true or false.
 * This is useful when a second prunning is needed.
 * @returns Number of leaves.
 */
unsigned int ContextTree::resetLeavesStatus(bool is_final_leaf) {
    deque<node*> leaves = getLeafNodes();
    voutLow << "Leaves: " << leaves.size() << endl;
    for (unsigned int i = 0; i < leaves.size(); i++) {
        leaves[i]->is_final_leaf = is_final_leaf;
    }
    return leaves.size();
}

/**
 * Set all sprouts probability to weight value times factor.
 * nodes->sprouts->probability = sprouts->count * factor
 * Nodes counts and probabilities are not altered.
 */
long double ContextTree::setSproutsProbabilityToWeightTimesFactor(long double factor) {
    map<string, node>::iterator it_node;
    stack<node*> nodes_stack;

    nodes_stack.push(&root_);
    node* cur_node;
    while (!nodes_stack.empty()) {
        cur_node = nodes_stack.top();
        nodes_stack.pop();

        //(*cur_node).count = initial_probability * (*cur_node).sprouts.size();
        //(*cur_node).probability = initial_probability;

        map<string, sprout>::iterator it_sprout;
        for (it_sprout = (*cur_node).sprouts.begin(); it_sprout != (*cur_node).sprouts.end(); it_sprout++) {
            (*it_sprout).second.probability = (*it_sprout).second.count * factor;
        }
        for (it_node = (*cur_node).branches.begin(); it_node != (*cur_node).branches.end(); it_node++) {
            nodes_stack.push(&(*it_node).second);
        }
    }

    return factor;
}

/**
 * nodes->sprouts.probability = logl(nodes->sprouts.probability)
 */
long double ContextTree::applyLogToSproutsProbability() {
    map<string, node>::iterator it_node;
    stack<node*> nodes_stack;

    nodes_stack.push(&root_);
    node* cur_node;
    while (!nodes_stack.empty()) {
        cur_node = nodes_stack.top();
        nodes_stack.pop();

        //(*cur_node).count = initial_probability * (*cur_node).sprouts.size();
        //(*cur_node).probability = initial_probability;
        if ((*cur_node).probability == 0.0)
            cerr << " SDFSDFDSF" << endl;
        (*cur_node).probability = logl((*cur_node).probability);

        map<string, sprout>::iterator it_sprout;
        for (it_sprout = (*cur_node).sprouts.begin(); it_sprout != (*cur_node).sprouts.end(); it_sprout++) {
            //if ((*it_sprout).second.probability) cerr << "JJJJJJJJJJ" << endl;
            //(*it_sprout).second.probability = logl((*it_sprout).second.probability);
            (*it_sprout).second.probability =
                    (*it_sprout).second.probability > 0.0 ? logl((*it_sprout).second.probability) : 0.0;
        }
        for (it_node = (*cur_node).branches.begin(); it_node != (*cur_node).branches.end(); it_node++) {
            nodes_stack.push(&(*it_node).second);
        }
    }

    return 0.0;
}

///////////////////////////////////////////////////////////////////////
bool ContextTree::setSproutProbability(const string& dest_sprout, const deque<string>& branch,
        long double probability) {
    node* n = getLastNodeOfBranch(branch);
    if (n->depth == (int) branch.size()) {
        auto it = n->sprouts.find(dest_sprout);
        if (it != n->sprouts.end()) {
            (*it).second.probability = probability;
        } else {
            return false;
        }
    } else {
        return false;
    }
    return true;
}

bool ContextTree::addToSproutProbability(const string& dest_sprout, const deque<string>& branch,
        long double probability) {
    node* n = getLastNodeOfBranch(branch);
    if (n->depth == (int) branch.size()) {
        auto it = n->sprouts.find(dest_sprout);
        if (it != n->sprouts.end()) {
            (*it).second.probability += probability;
        } else {
            return false;
        }
    } else {
        return false;
    }
    return true;
}

/**
 * Sets branch->sprout->count = count and returns true.
 * Returns false if tree does not contain the entire branch.
 */
bool ContextTree::setSproutWeight(const string& dest_sprout, const deque<string>& branch, long double count) {
    node* n = getLastNodeOfBranch(branch);
    if (n->depth == (int) branch.size()) {
        auto it = n->sprouts.find(dest_sprout);
        if (it != n->sprouts.end()) {
            (*it).second.count = count;
        } else {
            return false;
        }
    } else {
        return false;
    }
    return true;
}

/**
 * Sets branch->sprout->count += count and return true.
 * Returns false if tree does not contain the entire branch.
 */
bool ContextTree::addToSproutWeight(const string& dest_sprout, const deque<string>& branch, long double count) {
    node* n = getLastNodeOfBranch(branch);
    if (n->depth == (int) branch.size()) {
        auto it = n->sprouts.find(dest_sprout);
        if (it != n->sprouts.end()) {
            (*it).second.count += count;
        } else {
            return false;
        }
    } else {
        return false;
    }
    return true;
}

/**
 * Returns true if tree contains the entire branch.
 * False otherwise.
 */
bool ContextTree::addToSproutsProbabilityAlongBranch(const string& dest_sprout, const deque<string>& branch,
        long double probability) {
    node* n = getLastNodeOfBranch(branch);
    node* n1 = n;
    while (n != NULL) {
        auto it = n->sprouts.find(dest_sprout);
        if (it != n->sprouts.end()) {
            (*it).second.probability += probability;
            /*
             if ((*it).second.probability < 0.0) {
             cerr << "Sprout: " << dest_sprout
             << "; prob: " << (*it).second.probability
             << "; Branch: " << ToString(getBranchOfNode(n))
             << endl;
             }
             */
            // FIXME: TESTING
            // Let's do some pruning here.
            /*
             if (n1 == n && (*it).second.probability < -5.0) {
             n->sprouts.erase(dest_sprout);
             }
             */
        } else {
            //return false;
        }
        //FIXME: also adding to node probability, but then this should be on a different method (or
        //this method should have a different name.
        n->probability += probability;
        n = n->parent;
    }
    if (n1->depth == (int) branch.size()) {
        return true;
    } else {
        return false;
    }
}

///////////////////////////////////////////////////////////////////////

long double ContextTree::getSproutProbability(string sprout_name, const deque<string>& branch) {
    deque<string> not_interested;
    return getSproutProbability(sprout_name, branch, not_interested);
}

long double ContextTree::getSproutProbability(string sprout_name, const deque<string>& branch,
        deque<string>& used_branch) {
    /*
     if (branch.empty()) {
     used_branch = deque<string>();
     map<string, sprout>::iterator it_sprout = _root.branches["|"].sprouts.find(sprout_name);
     if (it_sprout == _root.branches["|"].sprouts.end()) {
     //cout << "Unknown sprout '" << sprout_name << "'?" << endl;
     return 0.0;
     } else {
     cout << "Known sprout '" << sprout_name << "'?" << endl;
     return (*it_sprout).second.probability;
     }
     }// */

    long double max_prob = 0.0;
    node* max_node = NULL;
    node* cur_node = &root_;
    map<string, sprout>::iterator it_sprout;
    auto itb = branch.begin();

    do {
        it_sprout = cur_node->sprouts.find(sprout_name);
        if (it_sprout != cur_node->sprouts.end()) {
            max_prob = (*it_sprout).second.probability;
            max_node = cur_node;
        }

        if (itb != branch.end()) {
            map<string, node>::iterator it_down = cur_node->branches.find((*itb));
            if (it_down == cur_node->branches.end()) {
                break;
            } else {
                cur_node = &((*it_down).second);
            }
            itb++;
        } else {
            break;
        }
    } while (1);

    /*
     unsigned int branch_size = branch.size();
     for (unsigned int i = 0; i < branch_size; i++) {
     if (cur_node->branches.empty()) {
     break;
     }
     map<string, node>::iterator it_down = cur_node->branches.find(branch[i]);
     if (it_down == cur_node->branches.end()) {
     break;
     } else {
     cur_node = &((*it_down).second);

     it_sprout = cur_node->sprouts.find(sprout_name);
     if (it_sprout != cur_node->sprouts.end()) {
     // Getting highest probability.
     //if ((*it_sprout).second.probability > max_prob) {
     max_prob = (*it_sprout).second.probability;
     max_node = cur_node;
     //}
     }
     }
     }*/

//if (max_node != NULL && max_node->name != "|")
    used_branch = getBranchOfNode(max_node);

    /*
     if (branch != used_branch) {
     cerr << branch.size() << " !! " << used_branch.size() << endl;
     cerr << ToString(branch) << " != " << ToString(used_branch) << endl;
     }
     //*/
    /*
     if (max_prob == 0.0) {
     cerr << "P(Context) = 0.0: P(" << sprout_name << " | " << ToString(branch) << ") = " << max_prob << "; " << ToString(used_branch) << endl;
     if (sprout_name == "RP") {
     cerr << "curnode: " << ToString(getSproutsNameOfNode(cur_node)) << endl;
     cerr << "root   : " << ToString(getSproutsOfRoot()) << endl;
     }
     }
     */
    return max_prob;
}

long double ContextTree::getSproutLastProbability(string sprout_name, const deque<string>& branch) {
    deque<string> not_interested;
    return getSproutLastProbability(sprout_name, branch, not_interested);
}

long double ContextTree::getSproutLastProbability(string sprout_name, const deque<string>& branch,
        deque<string>& used_branch) {
    node* n = getLastNodeOfBranchWithSprout(branch, sprout_name);
    used_branch = getBranchOfNode(n);
    return n != NULL ? n->sprouts[sprout_name].probability : 0.0;
}

long double ContextTree::getSproutLastProbability(string sprout_name, const deque<string>& branch,
        int& used_branch_size) {
    node* n = getLastNodeOfBranchWithSprout(branch, sprout_name);
//    used_branch = getBranchOfNode(n);
    used_branch_size = n != NULL ? n->depth : 0;
    return n != NULL ? n->sprouts[sprout_name].probability : 0.0;
}

long double ContextTree::getSproutLastWeight(string sprout_name, const deque<string>& branch) {
    deque<string> not_interested;
    return getSproutLastWeight(sprout_name, branch, not_interested);
}

long double ContextTree::getSproutLastWeight(string sprout_name, const deque<string>& branch,
        deque<string>& used_branch) {
    node* n = getLastNodeOfBranchWithSprout(branch, sprout_name);
    used_branch = getBranchOfNode(n);
    return n != NULL ? n->sprouts[sprout_name].count : 0.0;
}

long double ContextTree::getSproutLogProbability(string sprout_name, const deque<string>& branch,
        deque<string>& used_branch) {
    node* n = getLastNodeOfBranchWithSprout(branch, sprout_name);
    used_branch = getBranchOfNode(n);
    return n != NULL ? logl(n->sprouts[sprout_name].count) - logl(n->count) : -numeric_limits<long double>::infinity();
}

long double ContextTree::getSproutAccumulatedProbability(string sprout_name, const deque<string>& branch,
        deque<string>& used_branch) {
    int used_branch_size = 0;
    long double prob = getSproutAccumulatedProbability(sprout_name, branch, used_branch_size);
    used_branch = deque<string>(branch.begin(), branch.begin() + used_branch_size);
    return prob;
}

long double ContextTree::getSproutAccumulatedProbability(string sprout_name, const deque<string>& branch,
        int& used_branch_size) {
    node* n = getLastNodeOfBranchWithSprout(branch, sprout_name);
    used_branch_size = n != NULL ? n->depth : 0;
    long double prob = 0.0;
    while (n != NULL && n->depth > 0) {
        prob += n->sprouts[sprout_name].probability;
        n = n->parent;
    }
    return prob;
}

long double ContextTree::getSproutStarProbability(string sprout_name, const deque<string>& branch,
        const deque<bool>& ambiguity) {
    deque<string> not_interested;
    return getSproutStarProbability(sprout_name, branch, ambiguity, not_interested);
}

long double ContextTree::getSproutStarProbability(string sprout_name, const deque<string>& branch,
        const deque<bool>& ambiguity, deque<string>& used_branch) {
    /*
     unsigned int branch_size = branch.size();
     deque<string> star_branch;
     for (unsigned int i = 0; i < branch_size; i++) {
     if (ambiguity[i]) {
     star_branch.push_back(_star_node);
     } else {
     star_branch.push_back(branch[i]);
     }
     }
     return getSproutProbability(sprout_name, star_branch, used_branch);
     // */
//*
    if (branch.empty()) {
        map<string, sprout>::iterator it_sprout = root_.sprouts.find(sprout_name);
        if (it_sprout == root_.sprouts.end()) {
            return 0.0;
        } else {
            return (*it_sprout).second.probability;
        }
    }
    long double max_prob = 0.0;
    node* cur_node = &root_;
    node* max_node = NULL;
    map<string, sprout>::iterator it_sprout;
    unsigned int branch_size = branch.size();
    for (unsigned int i = 0; i < branch_size; i++) {
        if (cur_node->branches.empty()) {
            break;
        }
        //string child_node = ambiguity[i] ? _star_node : branch[i];
        map<string, node>::iterator it_down = cur_node->branches.find(branch[i]);
        if (it_down == cur_node->branches.end()) {
            if (ambiguity[i]) {
                it_down = cur_node->branches.find(star_node_);
                if (it_down == cur_node->branches.end()) {
                    break;
                } else {
                    cur_node = &((*it_down).second);
                    it_sprout = cur_node->sprouts.find(sprout_name);
                    if (it_sprout != cur_node->sprouts.end()) {
//                        if ((*it_sprout).second.probability > max_prob) {
                        max_prob = (*it_sprout).second.probability;
                        max_node = cur_node;
//                        }
                    }
                }
            } else {
                break;
            }
        } else {
            it_sprout = (*it_down).second.sprouts.find(sprout_name);
            if (it_sprout != (*it_down).second.sprouts.end()) {
                // Getting highest probability.
//                if ((*it_sprout).second.probability > max_prob) {
                max_prob = (*it_sprout).second.probability;
                max_node = &(*it_down).second;
//                }
            }
            auto prev_node = cur_node;
            cur_node = &((*it_down).second);

            if (ambiguity[i]) {
                auto it_star = prev_node->branches.find(star_node_);
                if (it_star != prev_node->branches.end()) {
                    auto it_star_sprout = (*it_star).second.sprouts.find(sprout_name);
                    if (it_star_sprout != (*it_star).second.sprouts.end()) {
                        if ((*it_star_sprout).second.probability > max_prob) {
                            max_prob = (*it_star_sprout).second.probability;
                            max_node = &(*it_star).second;
                        }
                        if ((*it_star_sprout).second.probability > (*it_sprout).second.probability) {
                            cur_node = &((*it_star).second);
                        }
                    }
                }
            }
        }
    }
    used_branch = getBranchOfNode(max_node);
    return max_prob;
// */
}

long double ContextTree::getSproutProbabilityWithStars(string sprout_name, const deque<string>& branch,
        const deque<unsigned int>& ambiguity) {
    string _star_node = "*";
    map<string, sprout>::iterator it_sprout;

    if (branch.empty()) {
        it_sprout = root_.sprouts.find(sprout_name);
        if (it_sprout == root_.sprouts.end()) {
            //cout << "Unknown sprout '" << sprout_name << "'?" << endl;
            return 0.0;
        } else {
            return (*it_sprout).second.probability;
        }
    }
    long double max_prob = 0.0;

    auto it_root = root_.branches.find(branch[0]);
    auto it_star = root_.branches.find(_star_node);
    deque<decltype(it_star)> bfs_nodes;

    if (it_root == root_.branches.end()) {
        it_root = it_star;
        if (it_root == root_.branches.end()) {
            map<string, sprout>::iterator it_sprout = root_.sprouts.find(sprout_name);
            if (it_sprout == root_.sprouts.end()) {
                return 0.0;
            } else {
                return (*it_sprout).second.probability;
            }
        }
    } else {
        if (it_star != root_.branches.end()) {
            bfs_nodes.push_back(it_star);
        }
    }

    bfs_nodes.push_back(it_root);
    int n = (int) branch.size();
    while (!bfs_nodes.empty()) {
        auto it_node = bfs_nodes.front();
        bfs_nodes.pop_front();

        it_sprout = (*it_node).second.sprouts.find(sprout_name);
        if (it_sprout != (*it_node).second.sprouts.end()) {
            if ((*it_sprout).second.probability > max_prob) {
                max_prob = (*it_sprout).second.probability;
            }
        }

        if ((*it_node).second.depth < n) {
            string next_tag = branch[(*it_node).second.depth];
            auto it_down = (*it_node).second.branches.find(next_tag);
            if (it_down != (*it_node).second.branches.end()) {
                bfs_nodes.push_back(it_down);
            }
            it_down = (*it_node).second.branches.find(_star_node);
            if (it_down != (*it_node).second.branches.end()) {
                bfs_nodes.push_back(it_down);
            }
        }
    }

    /*
     while (!branch_nodes.empty()) {
     it_node = branch_nodes.back();
     it_sprout = (*it_node).second.sprouts.find(sprout_name);
     if (it_sprout == (*it_node).second.sprouts.end()) {
     branch_nodes.pop_back();
     } else {
     // TODO: return last available node in context or the one with highest probability?
     //return (*it_sprout).second.probability;
     // Getting highest probability.
     if ((*it_sprout).second.probability > max_prob) {
     max_prob = (*it_sprout).second.probability;
     }
     branch_nodes.pop_back();
     }
     } // */

    return max_prob;
}

long double ContextTree::getSproutWeight(string sprout_name, const deque<string>& branch) {
    deque<string> not_interested;
    return getSproutWeight(sprout_name, branch, not_interested);
}

long double ContextTree::getSproutWeight(string sprout_name, const deque<string>& branch, deque<string>& used_branch) {
    long double not_interested;
    return getSproutWeight(sprout_name, branch, used_branch, not_interested);
}

long double ContextTree::getSproutWeight(string sprout_name, const deque<string>& branch, deque<string>& used_branch,
        long double& branch_weight) {
    /*
     if (branch.empty()) {
     map<string, sprout>::iterator it_sprout = _root.sprouts.find(sprout_name);
     if (it_sprout == _root.sprouts.end()) {
     branch_weight = 0.0;
     used_branch = deque<string>();
     return 0.0;
     } else {
     branch_weight = _root.count;
     used_branch = deque<string>();
     return (*it_sprout).second.count;
     }
     }// */

    long double max_weight = 0.0;
    node* max_node = NULL;
    node* cur_node = &root_;
    map<string, sprout>::iterator it_sprout;
    unsigned int branch_size = branch.size();
    for (unsigned int i = 0; i < branch_size; i++) {
        if (cur_node->branches.empty()) {
            break;
        }
        map<string, node>::iterator it_down = cur_node->branches.find(branch[i]);
        if (it_down == cur_node->branches.end()) {
            break;
        } else {
            cur_node = &((*it_down).second);
            it_sprout = cur_node->sprouts.find(sprout_name);
            if (it_sprout != cur_node->sprouts.end()) {
                // Getting highest probability.
                //if ((*it_sprout).second.probability > max_prob) {
                max_weight = (*it_sprout).second.count;
                max_node = cur_node;
                //}
            }
        }
    }
    used_branch = getBranchOfNode(max_node);
    branch_weight = max_node != NULL ? max_node->count : 0.0;
    return max_weight;
}

deque<string> ContextTree::getSproutsOfRoot() {
    deque<string> sprouts_names;
    map<string, sprout>::iterator it_sprout;
    for (it_sprout = root_.sprouts.begin(); it_sprout != root_.sprouts.end(); it_sprout++) {
        sprouts_names.push_back((*it_sprout).first);
    }
    return sprouts_names;
}

deque<string> ContextTree::getSproutsOfNode(string node_name) {
    deque<string> sprouts_names;

    map<string, node>::iterator it_node = root_.branches.find(node_name);
    if (it_node == root_.branches.end())
        return sprouts_names;
    map<string, sprout>::iterator it_sprout;
    for (it_sprout = (*it_node).second.sprouts.begin(); it_sprout != (*it_node).second.sprouts.end(); it_sprout++) {
        sprouts_names.push_back((*it_sprout).first);
    }

    return sprouts_names;
}

deque<string> ContextTree::getSproutsNameOfNode(node* node) {
    deque<string> sprouts_names;
    map<string, sprout>::iterator it_sprout;
    for (it_sprout = node->sprouts.begin(); it_sprout != node->sprouts.end(); it_sprout++) {
        sprouts_names.push_back((*it_sprout).first);
    }
    return sprouts_names;
}

deque<string> ContextTree::getSproutsOfBranch(deque<string> branch) {
    deque<string> sprouts_names;

    if (branch.empty())
        return sprouts_names;

    map<string, node>::iterator it_node = root_.branches.find(branch[0]);
    if (it_node == root_.branches.end())
        return sprouts_names;
    branch.pop_front();

    unsigned int branch_size = branch.size();
    for (unsigned int i = 0; i < branch_size; i++) {
        map<string, node>::iterator it_down = (*it_node).second.branches.find(branch[i]);
        if (it_down == (*it_node).second.branches.end()) {
            break;
        } else {
            it_node = it_down;
        }
    }

    map<string, sprout>::iterator it_sprout;
    for (it_sprout = (*it_node).second.sprouts.begin(); it_sprout != (*it_node).second.sprouts.end(); it_sprout++) {
        sprouts_names.push_back((*it_sprout).first);
    }

    return sprouts_names;
}

deque<string> ContextTree::getRelevantContextOfHistory(string present, vector<string> history) {
    deque<string> context(1, present);

    if (history.empty())
        return context;
    map<string, node>::iterator it_node = root_.branches.find(present);
    if (it_node == root_.branches.end())
        return context;

    unsigned int history_size = history.size();
    for (unsigned int i = 0; i < history_size; i++) {
        map<string, node>::iterator it_down = (*it_node).second.branches.find(history[i]);
        if (it_down == (*it_node).second.branches.end()) {
            break;
        } else {
            context.push_back((*it_down).first);
            it_node = it_down;
        }
    }

    return context;
}

deque<string> ContextTree::getBranchOfNode(node* n) {
    deque<string> branch;

    while (n != NULL && n->depth > 0) {
        branch.push_front(n->name);
        n = n->parent;
    }

    return branch;
}

ContextTree::node* ContextTree::getLastNodeOfBranch(const deque<string>& branch) {
    node* n = &root_;

    unsigned int branch_size = branch.size();
    for (unsigned int i = 0; i < branch_size; i++) {
        auto it = n->branches.find(branch[i]);
        if (it == n->branches.end()) {
            break;
        } else {
            n = &((*it).second);
        }
    }

    return n;
}

ContextTree::node* ContextTree::getLastNodeOfBranchWithSprout(const deque<string>& branch, const string& sprout) {
    node* n = &root_;
    node* last_n = NULL;

    for (unsigned int i = 0; i < branch.size(); i++) {
        auto it = n->branches.find(branch[i]);
        if (it == n->branches.end()) {
            break;
        } else {
            n = &((*it).second);
            if (n->sprouts.find(sprout) != n->sprouts.end()) {
                last_n = n;
            }
        }
    }

    return last_n;
}

ContextTree::node* ContextTree::getFirstNode(ContextTree::node* n) {
//	cerr << "n->name: " << n->name << endl;
    if (n->parent == NULL) {
//		cerr << "n->father == NULL" << endl;
        return n;
    }
    while (n->parent->parent != NULL) {
        n = n->parent;
//		cerr << "  n->name: " << n->name << endl;
    }
//	cerr << "ret n->name: " << n->name << endl;
    return n;
}

deque<ContextTree::node*> ContextTree::getLeafNodes() {
    deque<node*> leaves;
    stack<node*> nodes_stack;

    nodes_stack.push(&root_);
    node* cur_node;
    while (!nodes_stack.empty()) {
        cur_node = nodes_stack.top();
        nodes_stack.pop();
        if ((*cur_node).branches.empty()) { // No children.
            leaves.push_back(cur_node);
        } else {
            for (auto it_node = (*cur_node).branches.rbegin(); it_node != (*cur_node).branches.rend(); it_node++) {
                nodes_stack.push(&(*it_node).second);
            }
        }
    }

    return leaves;
}

unsigned int ContextTree::getDeepestLevelNumber() {
    if (deepest_level_number_ == 0 && !root_.branches.empty()) {
        deque<node*> leaves = getLeafNodes();
        unsigned int max_depth = 0;
        for (unsigned int i = 0; i < leaves.size(); i++) {
            max_depth = max((int) max_depth, leaves[i]->depth);
        }
        deepest_level_number_ = max_depth;
        return deepest_level_number_;
    } else {
        return deepest_level_number_;
    }
}

///////////////////////////////////////////////////////////////////////
void ContextTree::refreshNodesParents() {
    map<string, node>::iterator it_node;
    stack<node*> nodes_stack;

    root_.parent = NULL;
    nodes_stack.push(&root_);
    node* cur_node;
    while (!nodes_stack.empty()) {
        cur_node = nodes_stack.top();
        nodes_stack.pop();
        for (it_node = (*cur_node).branches.begin(); it_node != (*cur_node).branches.end(); it_node++) {
            (*it_node).second.parent = cur_node;
            nodes_stack.push(&(*it_node).second);
        }
    }
    return;
}

///////////////////////////////////////////////////////////////////////

void ContextTree::printSpecialBranch(string special_sub_root) {
    map<string, node>::iterator it, it_sub_root;
    it_sub_root = root_.branches.find(special_sub_root);
    if (it_sub_root == root_.branches.end()) {
        cout << "Attention: in ContextTree: could not find special sub root named: " << special_sub_root << endl;
        return;
    }
    cout << "=============================" << endl;
    cout << "Sub tree of special sub root: '" << special_sub_root << "'";
    cout << " |Branches| = " << (*it_sub_root).second.branches.size() << " P = " << (*it_sub_root).second.probability;
    cout << " Sprouts: ";
    map<string, sprout>::iterator it_sprout;
    for (it_sprout = (*it_sub_root).second.sprouts.begin(); it_sprout != (*it_sub_root).second.sprouts.end();
            it_sprout++) {
        cout << (*it_sprout).first << "|" << (*it_sprout).second.probability << " ";
    }
    cout << endl;
    for (it = (*it_sub_root).second.branches.begin(); it != (*it_sub_root).second.branches.end(); it++) {
        cout << "-----------------------------" << endl;
        cout << (*it).second.depth << ":" << (*it).first << " |Branches| = " << (*it).second.branches.size() << " P = "
                << (*it).second.probability;
        cout << " father's depth: " << (*it).second.parent->depth;
        cout << " Sprouts: ";
        map<string, sprout>::iterator it_sprout;
        for (it_sprout = (*it).second.sprouts.begin(); it_sprout != (*it).second.sprouts.end(); it_sprout++) {
            cout << (*it_sprout).first << "|" << (*it_sprout).second.probability << " ";
        }
        cout << endl;

        deque<map<string, node>::iterator> queue;
        queue.push_back(it);
        map<string, node>::iterator it_father;
        while (!queue.empty()) {
            it_father = queue.front();
            queue.pop_front();
            //cout << "-- depth: " << (*it_father).second.depth << endl;
            map<string, node>::iterator it_son;
            for (it_son = (*it_father).second.branches.begin(); it_son != (*it_father).second.branches.end();
                    it_son++) {
                cout << (*it_son).second.depth << ":" << (*it_son).first << "[" << (*it_son).second.branches.size()
                        << "](" << (*it_son).second.probability << ")";
                cout << " - father's depth: " << (*it_son).second.parent->depth;
                cout << " |Branches| = " << (*it_son).second.branches.size();
                cout << " Sprouts: ";
                map<string, sprout>::iterator it_sprout;
                for (it_sprout = (*it_son).second.sprouts.begin(); it_sprout != (*it_son).second.sprouts.end();
                        it_sprout++) {
                    //copy((*it_son).second.sprouts.begin(), (*it_son).second.sprouts.end(), ostream_iterator< pair<string, long double> >(cout, " "));
                    cout << (*it_sprout).first << "|" << (*it_sprout).second.probability << " ";
                }
                cout << endl;
                queue.push_back(it_son);
            }
        }
    }
    cout << "-----------------------------" << endl;
}

void ContextTree::print() {
    map<string, node>::iterator it;
    for (it = root_.branches.begin(); it != root_.branches.end(); it++) {
        cout << "-----------------------------" << endl;
        cout << (*it).second.depth << ":" << (*it).first << " |Branches| = " << (*it).second.branches.size() << " P = "
                << (*it).second.probability;
        cout << " father's depth: " << (*it).second.parent->depth << endl;

        deque<map<string, node>::iterator> queue;
        queue.push_back(it);
        map<string, node>::iterator it_father;
        while (!queue.empty()) {
            it_father = queue.front();
            queue.pop_front();
            //cout << "-- depth: " << (*it_father).second.depth << endl;
            map<string, node>::iterator it_son;
            for (it_son = (*it_father).second.branches.begin(); it_son != (*it_father).second.branches.end();
                    it_son++) {
                cout << (*it_son).second.depth << ":" << (*it_son).first << "[" << (*it_son).second.branches.size()
                        << "](" << (*it_son).second.probability << ")";
                cout << " - father's depth: " << (*it_son).second.parent->depth << " : father->name: "
                        << (*it_son).second.parent->name << endl;
                queue.push_back(it_son);
            }
        }
    }
    cout << "-----------------------------" << endl;
}

void ContextTree::printStatistics() {
    int max_size = 0;
    int sum_size = 0;
    int num_branches = 0;
    deque<string> max_branch;
    vector<deque<string> > max_branches;
    map<int, int> branches_sizes; // key: branch size; data: number of branches with this size.

    deque<map<string, node>::iterator> queue;
    map<string, node>::iterator it_from;
    for (it_from = root_.branches.begin(); it_from != root_.branches.end(); it_from++) {
        queue.push_back(it_from);
    }
    map<string, node>::iterator it_father;
    deque<string> branch;
    int last_depth = 0;
    while (!queue.empty()) {
        it_father = queue.front();
        queue.pop_front();
        while ((*it_father).second.depth < last_depth) {
            branch.pop_back();
            last_depth--;
        }
        branch.push_back((*it_father).first);
        if ((*it_father).second.branches.empty()) { // is a leaf
            sum_size += (*it_father).second.depth;
            branches_sizes[(*it_father).second.depth]++;
            num_branches++;
            if ((*it_father).second.depth > max_size) {
                max_size = (*it_father).second.depth;
                max_branches.clear();
                max_branch = branch;
            }
            if ((*it_father).second.depth == max_size) {
                max_branches.push_back(branch);
            }
            branch.pop_back();
            last_depth = (*it_father).second.depth;
        } else {
            map<string, node>::iterator it_son;
            for (it_son = (*it_father).second.branches.begin(); it_son != (*it_father).second.branches.end();
                    it_son++) {
                queue.push_front(it_son);
            }
        }
    }

    long double average_size = (long double) sum_size / (long double) num_branches;
    string name = !name_.empty() ? name_ + ' ' : "";
    vout << "[" << name << "Tree Statistics: ]" << endl;
    long double k = getCutValue();
    vout << "\tCut value (K): " << k << "; density:  " << minimum_times_to_be_in_tree_ << endl;
    vout << "\tNumber of branches: " << num_branches << endl;
    vout << "\tAverage branch size: " << average_size << endl;
    vout << "\tGreatest branch size: " << max_size << ", of branch: ";
    if (vout.output_priority <= vout.allowed_priority) {
        copy(max_branch.rbegin(), max_branch.rend(), ostream_iterator<string>(cout, " "));
        cout << endl;

        map<int, int>::iterator it_br;
        for (it_br = branches_sizes.begin(); it_br != branches_sizes.end(); it_br++) {
            cout << "\tNumber of branches with size (" << (*it_br).first << "): " << (*it_br).second << endl;
        }
    }
    voutLow << "\tBranches with this size:" << endl;
    if (voutLow.output_priority <= voutLow.allowed_priority) {
        unsigned int max = max_branches.size() > 15 ? 15 : max_branches.size();
        for (unsigned int i = 0; i < max; i++) {
            cout << "\t\t";
            copy(max_branches[i].rbegin(), max_branches[i].rend(), ostream_iterator<string>(cout, " "));
            cout << endl;
        }
        if (max_branches.size() > max)
            cout << "\t\t(and " << max_branches.size() - max << " more...)" << endl;
    }

    vout << "[End Tree Statistics]" << endl;
}

///////////////////////////////////////////////////////////////////////
// File operations.

// Write tree to file.
bool ContextTree::writeToFile(ofstream & outfile) //map<string, node>::iterator it_from)
        {
//ofstream outfile;
//outfile.open(file_name.c_str(), ofstream::app);
    if (!outfile.good()) {
        //cerr << "Error: could not open file '" << file_name << "' for writing the context tree" << endl;
        return false;
    }

    deque<map<string, node>::iterator> queue;
    int cur_depth = 0;
    int level_branch_counter = 1;

// FORMAT: "depth:probability:branches.size():branch1name,b1number;branch2name,b2number;:sprout1name,probability;sprout2name,probability;:\n".
// A branch number is its depth dot its order of appearaence at this depth (format: #.#).
    outfile << "TREE=" << endl;
    outfile << root_.depth << ":" << root_.probability << ":" << root_.branches.size() << ":";

    map<string, node>::iterator it;
    int i = 1;
    for (it = root_.branches.begin(); it != root_.branches.end(); it++, i++) {
        outfile << (*it).first << "," << (*it).second.depth << "." << i << ";";
        queue.push_back(it);
    }
    outfile << ":";
    map<string, sprout>::iterator it_sprout;
    for (it_sprout = root_.sprouts.begin(); it_sprout != root_.sprouts.end(); it_sprout++) {
        outfile << (*it_sprout).first << "," << (*it_sprout).second.probability << ";";
    }
    outfile << ":";

    outfile << root_.is_final_leaf;
    outfile << ":" << endl;

    map<string, node>::iterator it_node;
    while (!queue.empty()) {
        it_node = queue.front();
        queue.pop_front();

        if ((*it_node).second.depth > cur_depth) { // First node of next level in the tree.
            cur_depth = (*it_node).second.depth;
            level_branch_counter = 1;
        }

        outfile << (*it_node).second.depth << "." << level_branch_counter << ":" << (*it_node).second.probability << ":"
                << (*it_node).second.branches.size() << ":";

        level_branch_counter++;

        //map<string, node>::iterator it_son;
        int i = 1;
        for (it = (*it_node).second.branches.begin(); it != (*it_node).second.branches.end(); i++, it++) {
            outfile << (*it).first << "," << (*it).second.depth << "." << i << ";";
            queue.push_back(it);
        }
        outfile << ":";
        for (it_sprout = (*it_node).second.sprouts.begin(); it_sprout != (*it_node).second.sprouts.end(); it_sprout++) {
            outfile << (*it_sprout).first << "," << (*it_sprout).second.probability << ";";
        }
        outfile << ":";

        outfile << (*it_node).second.is_final_leaf;
        outfile << ":" << endl;
    }

    return true;
}
// */

// Read tree from file.
bool ContextTree::readFromFile(ifstream & infile) {
    if (!infile.good()) {
        //cerr << "Error: could not open file '" << file_name << "' for reading the context tree" << endl;
        return false;
    }

    string line;
    string delim;
    vector<string> line_tokens;
    int line_number = 0;
    vector<node*> p_upper_nodes;
    vector<node*> p_cur_nodes;
    int cur_father = -1;
    int previous_depth = -1;
    int cur_level_node = 0;
    map<string, node>::iterator it_cur_father_branches;

    while (true) {
        line_number++;
        infile >> line;
        if (infile.eof())
            break;

        delim = ":";
        line_tokens = StringTokenize(line, delim);
        //cout << "|line_tokens|: " << line_tokens.size() << endl;

        //copy(line_tokens.begin(), line_tokens.end(), ostream_iterator<string>(cout, " "));
        //cout << endl;

        if (line_tokens.size() != 6) {
            cerr << "ERROR[1]: context tree file on line " << line_number << ": invalid line format" << endl;
            return false;
        }

        // FORMAT: "depth:probability:branches.size():branch1name,b1number;branch2name,b2number;:sprout1name,probability;sprout2name,probability;:\n".
        // A branch number is its depth dot its order of appearaence at this depth (format: #.#).

        node a_node;
        sprout a_sprout;
        int depth;        //, level_order;
        long double probability;
        int num_branches;
        string branch_name;
        string sprout_name;

        // First field [depth.order]
        vector<string> temp_tokens = StringTokenize(line_tokens[0], "."); // get depth.order
        if (temp_tokens.size() == 0) {
            cerr << "ERROR[2]: context tree file on line " << line_number << ": invalid line format" << endl;
            return false;
        }
        depth = atoi(temp_tokens[0].c_str());
        if (depth == 0) {
            previous_depth = depth;
        } else {
            if (temp_tokens.size() != 2) {
                cerr << "ERROR[3]: context tree file on line " << line_number << ": invalid line format" << endl;
                return false;
            }
//            level_order = atoi(temp_tokens[1].c_str());
        }

        a_node.depth = depth;

        // Second field [probability|count]
        probability = atof(line_tokens[1].c_str());

        a_node.probability = probability;

        // Third field [number of branches]
        num_branches = atoi(line_tokens[2].c_str());

        // Forth field [branches]
        vector<string> branches = StringTokenize(line_tokens[3], ";");

        if (num_branches != (int) branches.size()) {
            cerr << "ERROR[4]: context tree file on line " << line_number << ": invalid line format" << endl;
            return false;
        }

        vector<string> name_number;
        for (unsigned int i = 0; i < branches.size(); i++) {
            name_number = StringTokenize(branches[i], ",");
            if (name_number.size() != 2) {
                cerr << "ERROR[5]: context tree file on line " << line_number << ": invalid line format" << endl;
                return false;
            }
            a_node.branches[name_number[0]];
        }

        // Fifth field [sprouts]
        vector<string> sprouts = StringTokenize(line_tokens[4], ";");
        vector<string> name_prob;
        for (unsigned int i = 0; i < sprouts.size(); i++) {
            name_prob = StringTokenize(sprouts[i], ",");
            if (name_prob.size() != 2) {
                cerr << "ERROR[6]: context tree file on line " << line_number << ": invalid line format" << endl;
                return false;
            }
            a_sprout.probability = atof(name_prob[1].c_str());
            a_node.sprouts[name_prob[0]] = a_sprout;
        }

        // Sixth field [is_final_leaf]
        int final_leaf = atoi(line_tokens[5].c_str());
        if (final_leaf == 1) {
            a_node.is_final_leaf = true;
        } else {
            a_node.is_final_leaf = false;
        }

        // Storing info //
        if (depth == 0) { // is the ROOT
            root_ = a_node;
            p_upper_nodes.clear();
            p_upper_nodes.push_back(&root_);
            cur_father = 0;
            it_cur_father_branches = p_upper_nodes[cur_father]->branches.begin();

            previous_depth = depth + 1;
            cur_level_node = 1;
            continue;
        }
        if (depth != previous_depth) {
            p_upper_nodes = p_cur_nodes;
            p_cur_nodes.clear();
            cur_father = 0;
            it_cur_father_branches = (p_upper_nodes[cur_father])->branches.begin();

            cur_level_node = 1;
            previous_depth = depth;
        }

        while (it_cur_father_branches == (p_upper_nodes[cur_father])->branches.end()) {
            cur_father++;
            if (cur_father >= (int) p_upper_nodes.size()) {
                cerr << "ERROR[7]: context tree file on line " << line_number << ": invalid line format" << endl;
                return false;
            }
            it_cur_father_branches = (p_upper_nodes[cur_father])->branches.begin();
        }
        (*it_cur_father_branches).second = a_node;
        (*it_cur_father_branches).second.parent = &(*p_upper_nodes[cur_father]);
        p_cur_nodes.push_back(&((*it_cur_father_branches).second));
        it_cur_father_branches++;

        cur_level_node++;
    }

    return true;
}
