/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2014 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include "functions.h"
#include <iomanip>

template<>
void ChangeConflictingTags(set<string>& tags) {
    string old_tag, new_tag;
    old_tag = ",";
    new_tag = "COMMA";
    tags.erase(old_tag);
    tags.insert(new_tag);
    //replace(tags.begin(), tags.end(), old_tag, new_tag);
    old_tag = ".";
    new_tag = "FPUNC";
    tags.erase(old_tag);
    tags.insert(new_tag);
    //replace(tags.begin(), tags.end(), old_tag, new_tag);
    old_tag = "(";
    new_tag = "PARENTHESES";
    tags.erase(old_tag);
    tags.insert(new_tag);
    //replace(tags.begin(), tags.end(), old_tag, new_tag);
}

/**
 * Tokenize @source string on every @delimiter string, at most @max_splits times (0 means all),
 * possibly @backwards (does not reverse the string nor the order of the returned tokens).
 */
vector<string> StringTokenize(string source, string delimiter, unsigned int max_splits, bool backwards) {
    vector<string> tokens;
    if (backwards) {
        int i = source.length() - 1;
        unsigned int j = 0, num_splits = 0;
        while (i >= 0) {
            j = source.rfind(delimiter, i);
            if (j == string::npos)
                break;
            tokens.insert(tokens.begin(), source.substr(j + delimiter.length(), i + 1 - (j + delimiter.length())));
            i = j - 1;
            if (max_splits > 0 && ++num_splits >= max_splits)
                break;
        }
        tokens.insert(tokens.begin(), source.substr(0, i + 1));
        //reverse(tokens.begin(), tokens.end());
    } else {
        unsigned int i = 0, j = 0, num_splits = 0;
        while (i < source.length()) {
            j = source.find(delimiter, i);
            if (j == string::npos)
                break;
            tokens.push_back(source.substr(i, j - i));
            i = j + delimiter.length();
            if (max_splits > 0 && ++num_splits >= max_splits)
                break;
        }
        tokens.push_back(source.substr(i, source.length() - i));
    }

    return tokens;
}

vector<vector<string> > VectorStringTokenize(vector<string> source, string delimiter) {
    vector<vector<string> > vector_tokens;
    vector<string> subvector;

    for (unsigned int i = 0; i < source.size(); i++) {
        if (source[i] == delimiter) {
            subvector.push_back(source[i]);
            vector_tokens.push_back(subvector);
            subvector.clear();
        } else {
            subvector.push_back(source[i]);
        }
    }

    return vector_tokens;
}

vector<vector<string> > VectorStringTokenize(vector<string> source, set<string> delimiters) {
    vector<vector<string> > vector_tokens;
    vector<string> subvector;

    for (unsigned int i = 0; i < source.size(); i++) {
        if (delimiters.find(source[i]) != delimiters.end()) {
            subvector.push_back(source[i]);
            vector_tokens.push_back(subvector);
            subvector.clear();
        } else {
            subvector.push_back(source[i]);
        }
    }

    return vector_tokens;
}

vector<vector<string> > ImitateVectorStringTokenize(vector<string> source, vector<vector<string> > model) {
    vector<vector<string> > vector_tokens;
    vector<string>::iterator it_source;

    it_source = source.begin();
    for (unsigned int i = 0; i < model.size(); i++) {
        if (it_source + model[i].size() > source.end()) {
            cerr << "Warning: in function ImitateVectorStringTokenize:" << endl
                    << "\t Operation incomplete: the source is shorter than the model" << endl;
            return vector_tokens;
        }
        vector<string> subvector(it_source, it_source + model[i].size());
        //vector_tokens.insert(vector_tokens.end(), it_source, it_source+(int)model[i].size());
        vector_tokens.push_back(subvector);
        it_source += (int) model[i].size();
    }

    return vector_tokens;
}

string TrimString(const string & str, const string & chrs_to_trim = " \t\n\r") {
    using std::string;
    string::size_type pos = str.find_first_not_of(chrs_to_trim);
    if (pos == string::npos)
        return "";
    return str.substr(pos, str.find_last_not_of(chrs_to_trim) - pos + 1);
}

// ChrsToTrim = " \t\n\r"
// TrimDir = 0
void Trim(std::string& str, const std::string & ChrsToTrim, int TrimDir) {
    size_t startIndex = str.find_first_not_of(ChrsToTrim);
    if (startIndex == std::string::npos) {
        str.erase();
        return;
    }
    if (TrimDir < 2)
        str = str.substr(startIndex, str.size() - startIndex);
    if (TrimDir != 1)
        str = str.substr(0, str.find_last_not_of(ChrsToTrim) + 1);
}

inline void TrimRight(std::string& str, const std::string & ChrsToTrim = " \t\n\r") {
    Trim(str, ChrsToTrim, 2);
}

inline void TrimLeft(std::string& str, const std::string & ChrsToTrim = " \t\n\r") {
    Trim(str, ChrsToTrim, 1);
}

map<string, map<string, int> > GetComposedTagsWithWords(vector<vector<string> > & stags,
        vector<vector<string> > & swords) {
    if (stags.size() != swords.size()) {
        cerr << "Error in GetComposedTagsWithWords: divergent number of tags and words." << endl;
    }

    map<string, map<string, int> > composed_tags;
    char sep = '+';

    for (unsigned int i = 0; i < stags.size(); i++) {
        for (unsigned int j = 0; j < stags[i].size(); j++) {
            if (stags[i][j].find(sep, 0) != string::npos)
                composed_tags[stags[i][j]][swords[i][j]]++;
        }
    }

    return composed_tags;
}

map<string, map<deque<string>, int> > GetMapOfComposedWords(map<string, map<string, int> > & composed_tags,
        map<deque<string>, map<deque<string>, int> > & split_words) {
    map<string, map<deque<string>, int> > composed_words;

    for (auto sw_it = split_words.begin(); sw_it != split_words.end(); sw_it++) {
        for (auto sw_it2 = (*sw_it).second.begin(); sw_it2 != (*sw_it).second.end(); sw_it2++) {
            const deque<string> split_tags = ((*sw_it2).first);
            string joined_tags = split_tags.size() > 0 ? split_tags[0] : "";
            for (unsigned int i = 1; i < split_tags.size(); i++) {
                joined_tags += '+' + split_tags[i];
            }
            //cout << joined_tags << endl;
            if (composed_tags.count(joined_tags)) {
                for (auto ct_it = composed_tags[joined_tags].begin(); ct_it != composed_tags[joined_tags].end(); ct_it++) {
                    composed_words[(*ct_it).first][(*sw_it).first] += (*ct_it).second + (*sw_it2).second;
                    if ((*ct_it).first == "nela") {
                        cout << (*ct_it).first << ": " << ToString((*sw_it).first) << ": " << ToString((*sw_it2).first)
                                << ": " << joined_tags << endl;
                        //for (unsigned int i = 0; i < )
                    }
                }
            }
        }
    }

    return composed_words;
}

/**
 * Count frequency of tags.
 * @returns Map of tag:frequency.
 */
map<string, long double> CountTags(const vector<vector<string> >& tags) {
    map<string, long double> tags_freq;
    for (unsigned int i = 0; i < tags.size(); i++) {
        for (unsigned int j = 0; j < tags[i].size(); j++) {
            tags_freq[tags[i][j]]++;
        }
    }
    return tags_freq;
}

/**
 * Count frequency of tags.
 * @returns A map with possible repeating keys where the tag's frequency is the key
 * 		and the value is the tag itself.
 */
multimap<long double, string> ReverseCountTags(const vector<vector<string> >& tags) {
    map<string, long double> tags_freq = CountTags(tags);
    multimap<long double, string> freqs_tag;
    for (auto it = tags_freq.begin(); it != tags_freq.end(); it++) {
        freqs_tag.insert(make_pair((*it).second, (*it).first));
    }
    return freqs_tag;
}

// model_tags are the correct tags, while test_tags are the tags obtained by the Viterbi algorithm on tagging words.
long double CalculateAccuracy(vector<vector<string> > model_tags, vector<vector<string> > test_tags,
        vector<vector<string> > words, set<string> known_words, set<string> ambiguous_words, string errors_file_name) {
    int num_correct = 0;
    int num_wrong = 0;
    int num_unknown_words_correct = 0;
    int num_unknown_words_wrong = 0;
    int num_unknown_words = 0;
    int num_ambiguous_words_correct = 0;
    int num_ambiguous_words_wrong = 0;
    vector<int> unknown_words_index;
    map<string, long double> most_wrong_ambiguous_words;
    map<string, long double> words_count;
    map<string, long double> tags_number_gold, tags_number_test, tags_number_match;
    unsigned int num_words = 0;

//    cout << "Model tags: "<<model_tags.size()<<" Test tags: "<<test_tags.size()<<" words: "<<words.size()<<" known words: " << known_words.size()<<endl;
    for (unsigned int s = 0; s < words.size(); s++) {
        for (unsigned int i = 0; i < words[s].size(); i++) {
            num_words++;
            if (known_words.find(words[s][i]) == known_words.end()) { //so words[i] is unknown
                num_unknown_words++;
            }
        }
    }

    string file_name = errors_file_name;
    ofstream outfile;
    outfile.open(file_name.c_str());
    if (!outfile.good()) {
        cerr << "Warning: could not open file '" << file_name << "' for writing tagging errors." << endl;
    }
    outfile << "CONTEXT WORD/WRONG_TAG%CORRECT_TAG CONTEXT" << endl;
    unsigned min_sents_size = min(words.size(), min(model_tags.size(), test_tags.size()));
    for (unsigned int s = 0; s < min_sents_size; s++) {
        unsigned int min_size = min(words[s].size(), min(model_tags[s].size(), test_tags[s].size()));
        for (unsigned int i = 0; i < min_size; i++) {
            words_count[words[s][i]]++;
            if (model_tags[s][i] == test_tags[s][i]) {
                num_correct++;
                if (known_words.find(words[s][i]) == known_words.end()) { //so word is unknown
                    num_unknown_words_correct++;
                }
                if (ambiguous_words.find(words[s][i]) != ambiguous_words.end()) { // So current word is ambiguous.
                    num_ambiguous_words_correct++;
                }

                tags_number_gold[model_tags[s][i]]++;
                tags_number_test[test_tags[s][i]]++;
                tags_number_match[test_tags[s][i]]++;
            } else {
                //cout << model_tags[s][i] << " != " << test_tags[s][i] << endl;
                string know_word = "word(!ambiguous)";
                num_wrong++;
                if (ambiguous_words.find(words[s][i]) != ambiguous_words.end()) { // So current word is ambiguous.
                    num_ambiguous_words_wrong++;
                    know_word = "word";
                }
                most_wrong_ambiguous_words[words[s][i]]++;
                if (known_words.find(words[s][i]) == known_words.end()) { //so word is unknown
                    num_unknown_words_wrong++;
                    know_word = "word(U)";
                }

                //wrong_tagging.push_back(words[i]+"/"+test_tags[i]+"$"+model_tags[i]);
                outfile << words[s][i] + "/" + test_tags[s][i] + "%" + model_tags[s][i] << ": ";
                outfile << "sentence " << s << ", " << know_word << " " << i << ": Left side: ";
                unsigned int lcs = 10; //left_context_size_to_print
                for (unsigned int j = (i < lcs ? 0 : i - lcs); j < i; j++) {
                    outfile << (words[s][j] + "/" + test_tags[s][j]) << " ";
                }
                //outfile << ((i > 1) ? (words[i-2]+"/"+test_tags[i-2]) : "") << " ";
                //outfile << ((i > 0) ? (words[i-1]+"/"+test_tags[i-1]) : "") << " ";
                outfile << "| Right side: ";

                int rcs = 4; //right_context_size_to_print
                for (unsigned int j = i + 1; j < i + rcs && j < min_size; j++) {
                    outfile << (words[s][j] + "/" + test_tags[s][j]) << " ";
                }
                //outfile << ((i+1 < min_size) ? (words[i+1]+"/"+test_tags[i+1]) : "") << endl;
                outfile << endl;

                tags_number_gold[model_tags[s][i]]++;
                tags_number_test[test_tags[s][i]]++;
            }
        }
    }
    outfile.close();

    printf("[Stats of tagging]\n");
    //TODO: configurable params
    //unsigned int top_n_pr = 15, bottom_n_pr = 15;
    unsigned int top_n_pr = 0, bottom_n_pr = 0;
    if (top_n_pr > 0)
        cout << "Best " << top_n_pr << " tags precision | recall:" << endl;

    int longest_tag_string = 0;
    multimap<long double, string> precision, recall;
    map<string, long double> tags_precision, tags_recall;
    map<string, long double>::iterator it_pr;
    for (it_pr = tags_number_gold.begin(); it_pr != tags_number_gold.end(); it_pr++) {
        long double pre = tags_number_match[(*it_pr).first] / (*it_pr).second;
        tags_precision[(*it_pr).first] = pre;
        precision.insert(pair<long double, string>(pre, (*it_pr).first));
        longest_tag_string = max(longest_tag_string, (int) (*it_pr).first.size());
    }
    for (it_pr = tags_number_test.begin(); it_pr != tags_number_test.end(); it_pr++) {
        long double rec = tags_number_match[(*it_pr).first] / (*it_pr).second;
        tags_recall[(*it_pr).first] = rec;
        recall.insert(pair<long double, string>(rec, (*it_pr).first));
    }
    deque<string> output_pr;
    unsigned int n = 0;
    for (auto it_top_p = precision.rbegin(), it_top_r = recall.rbegin();
            it_top_p != precision.rend() && it_top_r != recall.rend() && n < top_n_pr; it_top_p++, it_top_r++, n++) {
        stringstream out_pr;
        out_pr << fixed << setprecision(2);
        out_pr << "" << std::setw(longest_tag_string) << (*it_top_p).second << ": " << setw(4) << (*it_top_p).first
                << "(" << tags_recall[(*it_top_p).second] << ")" << " | " << setw(longest_tag_string)
                << (*it_top_r).second << ": " << setw(4) << (*it_top_r).first << "("
                << tags_precision[(*it_top_r).second] << ")";
        output_pr.push_back(out_pr.str());
    }
    deque<string> output_pr2;
    n = 0;
    for (auto it_bottom_p = precision.begin(), it_bottom_r = recall.begin();
            it_bottom_p != precision.end() && it_bottom_r != recall.end() && n < bottom_n_pr;
            it_bottom_p++, it_bottom_r++, n++) {
        stringstream out_pr;
        out_pr << fixed << setprecision(2);
        out_pr << "" << std::setw(longest_tag_string) << (*it_bottom_p).second << ": " << setw(4)
                << (*it_bottom_p).first << "(" << tags_recall[(*it_bottom_p).second] << ")" << " | "
                << setw(longest_tag_string) << (*it_bottom_r).second << ": " << setw(4) << (*it_bottom_r).first << "("
                << tags_precision[(*it_bottom_r).second] << ")";
        output_pr2.push_back(out_pr.str());
    }

    for (unsigned int i = 0; i < output_pr.size(); i++) {
        cout << output_pr[i] << endl;
    }
    if (bottom_n_pr > 0) {
        cout << "..." << endl;
        cout << "Worst " << bottom_n_pr << " tags precision | recall:" << endl;
        for (unsigned int i = 0; i < output_pr2.size(); i++) {
            cout << output_pr2[i] << endl;
        }
        cout << endl;
    }
    cout << "Most wrong tagged words: word: #errors / #total = %errors" << endl;
    unsigned int top_n = 12;
    map<string, long double>::iterator it_most;
    multimap<long double, string> more;
    more.insert(pair<long double, string>(0.0, ""));
    for (it_most = most_wrong_ambiguous_words.begin(); it_most != most_wrong_ambiguous_words.end(); it_most++) {
        if ((*it_most).second > (*more.begin()).first) {
            if (more.size() < top_n) {
                more.insert(pair<long double, string>((*it_most).second, (*it_most).first));
            } else {
                more.erase(more.begin());
                more.insert(pair<long double, string>((*it_most).second, (*it_most).first));
            }
        }
    }
    multimap<long double, string>::reverse_iterator it_more;
    for (it_more = more.rbegin(); it_more != more.rend(); it_more++) {
        //printf("\t\t%s: %Lg\n", (*it_more).second.c_str(), (*it_more).first);
        cout << setw(12) << (*it_more).second << ": " << setw(4) << (*it_more).first << " / " << setw(5)
                << words_count[(*it_more).second] << " = " << setprecision(4)
                << 100.0 * ((*it_more).first / ((long double) words_count[(*it_more).second])) << "%" << endl;
    }

    int num_known_words = num_words - num_unknown_words;
    int num_known_words_correct = num_correct - num_unknown_words_correct;
    int num_known_words_wrong = num_wrong - num_unknown_words_wrong;
    long double percent_known_words = (long double) num_known_words / (long double) num_words;
    long double known_words_accuracy = (long double) ((long double) num_known_words_correct
            / (long double) num_known_words);
    cout << endl;
    cout << setw(9) << "Words" << setw(9) << "Total" << setw(8) << "(%)" << setw(11) << "Correct" << setw(8) << "Wrong"
            << setw(11) << "Accuracy%" << endl;
    cout << setw(11) << "Known: " << setw(7) << num_known_words << " (" << fixed << setprecision(4) << setw(7)
            << 100.0 * percent_known_words << "%)" << setw(8) << num_known_words_correct << setw(8)
            << num_known_words_wrong << setw(10) << known_words_accuracy * 100.0 << "%" << endl;
    if (num_unknown_words != 0) {
        long double percent_unknown_words = (long double) num_unknown_words / (long double) num_words;
        long double unknown_words_accuracy = (long double) ((long double) num_unknown_words_correct
                / (long double) num_unknown_words);
        cout << setw(11) << "Unknown: " << setw(7) << num_unknown_words << " (" << fixed << setprecision(4) << setw(7)
                << 100.0 * percent_unknown_words << "%)" << setw(8) << num_unknown_words_correct << setw(8)
                << num_unknown_words_wrong << setw(10) << setprecision(4) << unknown_words_accuracy * 100.0 << "%"
                << endl;
    }
    long int num_ambiguous_words = (long int) (num_ambiguous_words_correct + num_ambiguous_words_wrong);
    long double percent_ambiguous_words = num_ambiguous_words / (long double) num_words;
    long double ambiguous_words_accuracy = (long double) ((long double) num_ambiguous_words_correct
            / num_ambiguous_words);
    cout << setw(11) << "Ambiguous: " << setw(7) << num_ambiguous_words << " (" << fixed << setprecision(4) << setw(7)
            << 100.0 * percent_ambiguous_words << "%)" << setw(8) << num_ambiguous_words_correct << setw(8)
            << num_ambiguous_words_wrong << setw(10) << ambiguous_words_accuracy * 100.0 << "%" << endl;

    long double accuracy = (long double) num_correct / (long double) num_words; //model_tags.size();

    cout << setw(11) << "Total: " << setw(7) << num_correct + num_wrong << " (" << setw(7) << 100 << "%)" << setw(8)
            << num_correct << setw(8) << num_wrong << setw(10) << accuracy * 100.0 << "%" << endl;

//    cout << "Total correct tags: " << num_correct
//    	 << " Total wrong tags: " << num_wrong
//    	 << endl;

    cout << resetiosflags(ios::fixed) << setprecision(6); // default

    return accuracy;
}
