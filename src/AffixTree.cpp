/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2014 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#include "AffixTree.h"

extern ui::UIHandler::VerboseOutput vout, voutHigh, voutLow;

AffixTree::AffixTree() {
    // Setting parameters.
    density_ = 1;
    // Initializing
    init();
}
AffixTree::AffixTree(int min_times) {
    // Setting parameters.
    density_ = min_times;
    // Initialization
    init();
}
void AffixTree::init() {
    // Initialization
    cut_value_ = 0.0;
//	root_.probability = 0.0;
    root_.depth = 0;

    root_.count = 0.0;
    root_.is_final_leaf = false;
    root_.parent = NULL;
}

AffixTree::~AffixTree() {
}

// Private //

AffixTree::sprout AffixTree::newSprout() {
    sprout new_sprout;
    new_sprout.count = 0.0;
//	new_sprout.probability = 0.0;
    return new_sprout;
}

AffixTree::node AffixTree::newNode() {
    node new_node;
    new_node.count = 0.0;
//	new_node.probability = 0.0;
    new_node.depth = -1;
    new_node.is_final_leaf = false;
    new_node.parent = NULL;

    return new_node;
}

// Public //

// 'nodes' contains a reversed sequence of tags occuring in the training corpus.
// The first node in 'nodes' is the destination, and the remainings, the past
// (from most recent to oldest).
void AffixTree::addBranch(string branch, string sprout_name) {
    //(*(root_.sprouts.insert(pair<string, sprout>(sprout_name, newSprout()))).first).second.count++;
    if (root_.sprouts.count(sprout_name) == 0) {
        root_.sprouts[sprout_name] = newSprout();
    }
    root_.sprouts[sprout_name].count++;
    root_.count++;
    if (branch.empty()) {
        return;
    }

    node* parent = &root_;
    node* new_node = NULL;
    for (unsigned int i = 0; i < branch.size(); i++) {
        if (parent->branches.count(branch[i]) == 0) {
            parent->branches[branch[i]] = newNode();
            new_node = &parent->branches[branch[i]];
            new_node->name = branch[i];
            new_node->parent = parent;
            new_node->depth = parent->depth + 1;
        } else {
            new_node = &parent->branches[branch[i]];
        }
        new_node->count++;

        // Add sprout and increment probability (counter).
        //(*((*it_node).second.sprouts.insert(pair<string, sprout>(sprout_name, newSprout()))).first).second.count++;
        if (new_node->sprouts.count(sprout_name) == 0) {
            new_node->sprouts[sprout_name] = newSprout();
        }
        new_node->sprouts[sprout_name].count++;

        parent = new_node;
    }
}

/**
 * Minimum times a branch must occur for staying in the tree.
 *
 * The shake() method must be called in order to apply this rule.
 */
void AffixTree::setDensity(int min_times) {
    density_ = min_times;
}

int AffixTree::getDensity()
{
    return density_;
}

/*
 void AffixTree::normalizeProbabilities() {
 for (map<string, sprout>::iterator it_sprout = root_.sprouts.begin();
 it_sprout != root_.sprouts.end(); it_sprout++) {
 (*it_sprout).second.probability = (*it_sprout).second.count
 / root_.count;
 }

 for (map<char, node>::iterator it_roots = root_.branches.begin();
 it_roots != root_.branches.end(); it_roots++) {
 deque<map<char, node>::iterator> node_queue;
 node_queue.push_back(map<char, node>::iterator(it_roots));

 // Going Breadth First down the tree.
 while (!node_queue.empty()) {
 typeof(node_queue.front()) cur_element = node_queue.front();
 node_queue.pop_front();

 (*cur_element).second.probability = (*cur_element).second.count
 / (*cur_element).second.father->count;

 // Normalize: P(X|Y) = N(Y,X) / N(Y).
 map<string, sprout>::iterator it_sprout;
 for (it_sprout = (*cur_element).second.sprouts.begin();
 it_sprout != (*cur_element).second.sprouts.end();
 it_sprout++) {
 //?if ((*it_sprout).second.probability >= _minimum_times_to_be_in_tree) {
 (*it_sprout).second.probability = (*it_sprout).second.count
 / (*cur_element).second.count;
 //?}
 }

 // Update queue.
 map<char, node>::iterator it_node;
 for (it_node = (*cur_element).second.branches.begin();
 it_node != (*cur_element).second.branches.end();) {
 if ((*it_node).second.count < minimum_times_to_be_in_tree_) {
 map<char, node>::iterator it_next = it_node;
 it_next++;
 (*cur_element).second.branches.erase(it_node);
 it_node = it_next;
 } else {
 node_queue.push_back(map<char, node>::iterator(it_node));
 it_node++;
 }
 }
 }
 }
 }
 */

/*void AffixTree::normalizeProbabilities() {
 node* cur_node;
 deque<node*> node_queue;
 node_queue.push_back(&root_);
 // Going Breadth First down the tree.
 // Normalize.
 while (!node_queue.empty()) {
 cur_node = node_queue.front();
 node_queue.pop_front();
 // Normalize: P(X|Y) = N(Y,X) / N(Y).
 for (typeof(cur_node->sprouts.begin()) its = cur_node->sprouts.begin(); its != cur_node->sprouts.end(); its++) {
 (*its).second.probability = (*its).second.count / cur_node->count;
 }
 if (cur_node->father != NULL) {
 cur_node->probability = cur_node->count / cur_node->father->count;
 } else {
 cur_node->probability = 1.0;
 }
 // Update queue.
 for (typeof(cur_node->branches.begin()) itb = cur_node->branches.begin(); itb != cur_node->branches.end();
 itb++) {
 node_queue.push_back(&(*itb).second);
 }
 }
 }*/

/**
 * @brief Remove nodes occurring less times than the specified density (#_minimum_times_to_be_in_tree).
 */
void AffixTree::shake(bool apply_to_sprouts) {
    node* cur_node;
    deque<node*> node_queue;
    long double smallest_frequency = 1000000.0, biggest_frequency = 0.0;
    // Going Breadth First down the tree.
    // Cut off rare nodes.
    node_queue.push_back(&root_);
//    for (map<char, node>::iterator it_roots = root_.branches.begin(); it_roots != root_.branches.end(); it_roots++) {
//        node_queue.push_back(&(*it_roots).second);
//    }
    while (!node_queue.empty()) {
        cur_node = node_queue.front();
        node_queue.pop_front();
        smallest_frequency = min(smallest_frequency, cur_node->count);
        biggest_frequency = max(biggest_frequency, cur_node->count);
        if (cur_node->count < getDensity()) {
            if (cur_node->parent == NULL) { // so cur_node is the root
                //delete cur_node;
                cur_node->branches.clear();
                return;
            }
            cur_node->parent->branches.erase(cur_node->name);
        } else {
            if (apply_to_sprouts) {
                vector<decltype(cur_node->sprouts.begin())> erasables;
                for (auto its = cur_node->sprouts.begin();
                        its != cur_node->sprouts.end(); its++) {
                    if ((*its).second.count < getDensity()) {
                        cur_node->count -= (*its).second.count;
                        erasables.push_back(its);
                    }
                }
                for (unsigned int i = 0; i < erasables.size(); i++) {
                    cur_node->sprouts.erase(erasables[i]);
                }
            }
            if (cur_node->sprouts.empty() && cur_node->parent != NULL) {
                cur_node->parent->branches.erase(cur_node->name);
            } else {
                for (auto itb =
                        cur_node->branches.begin();
                        itb != cur_node->branches.end(); itb++) {
                    node_queue.push_back(&(*itb).second);
                }
            }
        }
    }
    voutLow << name_ << "'s smallest frequency: " << smallest_frequency << endl;
    voutLow << name_ << "'s biggest frequency: " << biggest_frequency << endl;
}

/**
 * Returns true if tree contains the entire branch.
 * False otherwise.
 */
bool AffixTree::addToSproutsCountsAlongBranch(const string& dest_sprout,
        const string& branch, long double count) {
    node* n = getLastNodeOfBranchWithSprout(branch, dest_sprout);
    node* n1 = n;
    while (n != NULL) {
        auto it = n->sprouts.find(dest_sprout);
        if (it != n->sprouts.end()) {
            (*it).second.count += count;
        } else {
            //return false;
        }
        n = n->parent;
    }
    if (n1->depth == (int) branch.size()) {
        return true;
    } else {
        return false;
    }
}

/**
 * Returns true if tree contains the entire branch.
 * False otherwise.
 */
/*
 bool AffixTree::addToSproutsProbabilityAlongBranch(const string& dest_sprout,
 const string& branch, long double probability) {
 node* n = getLastNodeOfBranchWithSprout(branch, dest_sprout);
 node* n1 = n;
 while (n != NULL) {
 typeof(n->sprouts.begin()) it = n->sprouts.find(dest_sprout);
 if (it != n->sprouts.end()) {
 (*it).second.probability += probability;
 } else {
 //return false;
 }
 n = n->father;
 }
 if (n1->depth == (int) branch.size()) {
 return true;
 } else {
 return false;
 }
 }
 */

/**
 * nodes->sprouts.probability = logl(nodes->sprouts.probability)
 */
/*
 long double AffixTree::applyLogToSproutsProbability() {
 map<char, node>::iterator it_node;
 stack<node*> nodes_stack;

 nodes_stack.push(&root_);
 node* cur_node;
 while (!nodes_stack.empty()) {
 cur_node = nodes_stack.top();
 nodes_stack.pop();

 //(*cur_node).count = initial_probability * (*cur_node).sprouts.size();
 //(*cur_node).probability = initial_probability;

 map<string, sprout>::iterator it_sprout;
 for (it_sprout = (*cur_node).sprouts.begin();
 it_sprout != (*cur_node).sprouts.end(); it_sprout++) {
 (*it_sprout).second.probability = logl(
 (*it_sprout).second.probability);
 }
 for (it_node = (*cur_node).branches.begin();
 it_node != (*cur_node).branches.end(); it_node++) {
 nodes_stack.push(&(*it_node).second);
 }
 }

 return 0.0;
 }
 */

///////////////////////////////////////////////////////////////////////
//long double AffixTree::getSproutProbability(const string& sprout_name,
//		const string& branch) {
//	long double not_interested;
//	return getSproutProbability(sprout_name, branch, not_interested);
//}
//long double AffixTree::getSproutProbability(const string& sprout_name,
//		const string& branch, long double& branch_prob) {
//	if (branch.empty()) {
//		map<string, sprout>::iterator it_sprout = root_.sprouts.find(
//				sprout_name);
//		if (it_sprout == root_.sprouts.end()) {
//			//cout << "Unknown sprout '" << sprout_name << "'?" << endl;
//			return 0.0;
//		} else {
//			return (*it_sprout).second.probability;
//		}
//	}
//
//	//*
//	long double last_prob = 0.0;
//	node* last_node = NULL;
//	node* cur_node = &root_;
//	map<string, sprout>::iterator it_sprout;
//	unsigned int branch_size = branch.size();
//	for (unsigned int i = 0; i < branch_size; i++) {
//		if (cur_node->branches.empty()) {
//			break;
//		}
//		map<char, node>::iterator it_down = cur_node->branches.find(branch[i]);
//		if (it_down == cur_node->branches.end()) {
//			break;
//		} else {
//			cur_node = &((*it_down).second);
//
//			it_sprout = cur_node->sprouts.find(sprout_name);
//			if (it_sprout != cur_node->sprouts.end()) {
//				// Getting highest probability.
//				//if ((*it_sprout).second.probability > max_prob) {
//				last_prob = (*it_sprout).second.probability;
//				last_node = cur_node;
//				//}
//			}
//		}
//	}
//	branch_prob = last_node != NULL ? last_node->probability : 0.0;
//	return last_prob;
//	// */
//
//	/*
//	 map<char, node>::iterator it_node;
//	 map<char, node>::iterator it_root = _root.branches.find(branch[0]);
//	 if (it_root == _root.branches.end()) return 0.0;
//	 it_node = it_root;
//
//	 deque< map<char, node>::iterator > branch_nodes(1, it_node);
//	 map<string, sprout>::iterator it_sprout;
//	 unsigned int branch_size = branch.size();
//	 for (unsigned int i = 1; i < branch_size; i++) {
//	 if ((*it_node).second.branches.empty()) {
//	 break;
//	 }
//	 map<char, node>::iterator it_down = (*it_node).second.branches.find(branch[i]);
//	 if (it_down == (*it_node).second.branches.end()) {
//	 break;
//	 } else {
//	 branch_nodes.push_back(it_down);
//	 it_node = it_down;
//	 }
//	 }
//
//	 while (!branch_nodes.empty()) {
//	 it_node = branch_nodes.back();
//	 it_sprout = (*it_node).second.sprouts.find(sprout_name);
//	 if (it_sprout == (*it_node).second.sprouts.end()) {
//	 branch_nodes.pop_back();
//	 } else {
//	 return (*it_sprout).second.probability;
//	 }
//	 }
//	 //*/
//
//	// Arriving here means the 'branch' does not have the 'sprout(_name)'.
//	// So we return the probability of the sprout as an initial sprout.
//	// If it does not exists, we return 0.
//	/*it_sprout = _root.sprouts.find(sprout_name);
//	 if (it_sprout == _root.sprouts.end()) {
//	 //cout << "Unknown sprout '" << sprout_name << "'?" << endl;
//	 return (*it_root).second.probability / (*it_root).second.father->probability;
//	 //return 0.0;
//	 } else {
//	 return (*it_sprout).second.probability;
//	 }// */
//
//	return 0.0;
//}
long double AffixTree::getSproutCount(const string& sprout_name,
        const string& branch) {
    if (branch.empty()) {
        map<string, sprout>::iterator it_sprout = root_.sprouts.find(
                sprout_name);
        if (it_sprout == root_.sprouts.end()) {
            return 0.0;
        } else {
            return (*it_sprout).second.count;
        }
    }

    long double last_prob = 0.0;
    //node* last_node = NULL;
    node* cur_node = &root_;
    map<string, sprout>::iterator it_sprout;
    for (unsigned int i = 0; i < branch.size(); i++) {
        if (cur_node->branches.empty()) {
            break;
        }
        map<char, node>::iterator it_down = cur_node->branches.find(branch[i]);
        if (it_down == cur_node->branches.end()) {
            break;
        } else {
            cur_node = &((*it_down).second);

            it_sprout = cur_node->sprouts.find(sprout_name);
            if (it_sprout != cur_node->sprouts.end()) {
                // Getting highest probability.
                //if ((*it_sprout).second.probability > max_prob) {
                last_prob = (*it_sprout).second.count;
                //last_node = cur_node;
                //}
            }
        }
    }
    //branch_prob = last_node != NULL ? last_node->count : 0.0;
    return last_prob;
}

long double AffixTree::getSproutAccumulatedCount(const string& sprout_name,
        const string& branch) {
    if (branch.empty()) {
        map<string, sprout>::iterator it_sprout = root_.sprouts.find(
                sprout_name);
        if (it_sprout == root_.sprouts.end()) {
            return 0.0;
        } else {
            return (*it_sprout).second.count;
        }
    }

    long double last_prob = 0.0;
    node* cur_node = &root_;
    map<string, sprout>::iterator it_sprout;
    unsigned int branch_size = branch.size();
    for (unsigned int i = 0; i < branch_size; i++) {
        if (cur_node->branches.empty()) {
            break;
        }
        map<char, node>::iterator it_down = cur_node->branches.find(branch[i]);
        if (it_down == cur_node->branches.end()) {
            break;
        } else {
            cur_node = &((*it_down).second);

            it_sprout = cur_node->sprouts.find(sprout_name);
            if (it_sprout != cur_node->sprouts.end()) {
                last_prob += (*it_sprout).second.count;
            }
        }
    }
    return last_prob;
}

/*
 long double AffixTree::getSproutAccumulatedProbability(
 const string& sprout_name, const string& branch) {
 if (branch.empty()) {
 map<string, sprout>::iterator it_sprout = root_.sprouts.find(
 sprout_name);
 if (it_sprout == root_.sprouts.end()) {
 return 0.0;
 } else {
 return (*it_sprout).second.count;
 }
 }

 long double last_prob = 0.0;
 node* cur_node = &root_;
 map<string, sprout>::iterator it_sprout;
 unsigned int branch_size = branch.size();
 for (unsigned int i = 0; i < branch_size; i++) {
 if (cur_node->branches.empty()) {
 break;
 }
 map<char, node>::iterator it_down = cur_node->branches.find(branch[i]);
 if (it_down == cur_node->branches.end()) {
 break;
 } else {
 cur_node = &((*it_down).second);

 it_sprout = cur_node->sprouts.find(sprout_name);
 if (it_sprout != cur_node->sprouts.end()) {
 last_prob += (*it_sprout).second.probability;
 }
 }
 }
 return last_prob;
 }
 */

deque<string> AffixTree::getSproutsOfRoot() {
    deque<string> sprouts_names;
    map<string, sprout>::iterator it_sprout;
    for (it_sprout = root_.sprouts.begin(); it_sprout != root_.sprouts.end();
            it_sprout++) {
        sprouts_names.push_back((*it_sprout).first);
    }
    return sprouts_names;
}

/*deque<string> AffixTree::getSproutsOfNode(char node_name)
 {
 deque<string> sprouts_names;

 map<string, node>::iterator it_node = _root.branches.find(node_name);
 if (it_node == _root.branches.end())
 return sprouts_names;
 map<string, sprout>::iterator it_sprout;
 for (it_sprout = (*it_node).second.sprouts.begin(); it_sprout != (*it_node).second.sprouts.end(); it_sprout++) {
 sprouts_names.push_back((*it_sprout).first);
 }

 return sprouts_names;
 }*/

set<string> AffixTree::getSproutsOfBranch(string branch) {
    set<string> sprouts_names;

    if (branch.empty())
        return sprouts_names;

    map<char, node>::iterator it_node = root_.branches.find(branch[0]);
    if (it_node == root_.branches.end())
        return sprouts_names;
    branch.erase(0, 1);                //pop_front();

    unsigned int branch_size = branch.size();
    for (unsigned int i = 0; i < branch_size; i++) {
        map<char, node>::iterator it_down = (*it_node).second.branches.find(
                branch[i]);
        if (it_down == (*it_node).second.branches.end()) {
            break;
        } else {
            it_node = it_down;
        }
    }

    map<string, sprout>::iterator it_sprout;
    for (it_sprout = (*it_node).second.sprouts.begin();
            it_sprout != (*it_node).second.sprouts.end(); it_sprout++) {
        sprouts_names.insert((*it_sprout).first);
    }

    return sprouts_names;
}

AffixTree::node* AffixTree::getLastNodeOfBranchWithSprout(const string& branch,
        const string& sprout) {
    node* n = &root_;
    node* last_n = n;

    unsigned int branch_size = branch.size();
    for (unsigned int i = 0; i < branch_size; i++) {
        auto it = n->branches.find(branch[i]);
        if (it == n->branches.end()) {
            break;
        } else {
            n = &((*it).second);
            if (n->sprouts.find(sprout) != n->sprouts.end()) {
                last_n = n;
            }
        }
    }

    return last_n;
}

///////////////////////////////////////////////////////////////////////////////

/*
 long double AffixTree::resetProbabilities(long double initial_probability) {
 map<char, node>::iterator it_node;
 stack<node*> nodes_stack;

 nodes_stack.push(&root_);
 node* cur_node;
 while (!nodes_stack.empty()) {
 cur_node = nodes_stack.top();
 nodes_stack.pop();

 //(*cur_node).count = initial_probability * (*cur_node).sprouts.size();
 (*cur_node).probability = initial_probability;

 map<string, sprout>::iterator it_sprout;
 for (it_sprout = (*cur_node).sprouts.begin();
 it_sprout != (*cur_node).sprouts.end(); it_sprout++) {
 //(*it_sprout).second.count = initial_count;
 (*it_sprout).second.probability = initial_probability;
 }
 for (it_node = (*cur_node).branches.begin();
 it_node != (*cur_node).branches.end(); it_node++) {
 nodes_stack.push(&(*it_node).second);
 }
 }

 return initial_probability;
 }
 */

///////////////////////////////////////////////////////////////////////
void AffixTree::setCutValue(long double K) {
    cut_value_ = K;
}

/**
 * @brief Remove nodes if the KL Divergence with respect to their parents is less than the cut value (#getCutValue()).
 *
 * This implementation traverses the tree in a bottom-up manner.
 * Gives a slightly different result than the top-down approach.
 */
/*bool AffixTree::prune() {
 vout << "[Prunning " << getName() << " Tree]" << endl;
 typeof(root_.branches.begin()) it_node;
 stack<typeof(it_node)> nodes_stack, starting_stack;

 long double cut_value = getCutValue();
 bool is_being_pruned = true;
 while (is_being_pruned) {
 for (it_node = root_.branches.begin(); it_node != root_.branches.end(); it_node++) {
 if (!(*it_node).second.branches.empty()) {
 nodes_stack.push(it_node);
 }
 }
 is_being_pruned = false;
 int times_pruned = 0, times_not_pruned = 0;
 typeof(it_node) cur_node, max_node;
 long double max_count = 0;
 while (!nodes_stack.empty()) {
 cur_node = nodes_stack.top();
 nodes_stack.pop();

 if ((*cur_node).second.branches.empty() && (*cur_node).second.is_final_leaf == false) {
 // Node is a leaf. Calculate Delta.
 long double delta = 0.0;
 map<string, sprout>::iterator it_sprout;
 for (it_sprout = (*cur_node).second.sprouts.begin(); it_sprout != (*cur_node).second.sprouts.end();
 it_sprout++) {
 //                    long double cut_branch_prob = (*cur_node).second.father->sprouts[(*it_sprout).first].probability;
 //                    if (cut_branch_prob == 0.0) {
 //                        vout << "Sprout: " << (*it_sprout).first << "; Cut branch prob: " << cut_branch_prob
 //                                << "; count: " << (*cur_node).second.father->sprouts[(*it_sprout).first].count
 //                                << "; Depth: " << (*cur_node).second.father->depth << endl;
 //                    }
 //                    long double log_value = log((*it_sprout).second.probability / cut_branch_prob);
 //                    long double product = (*it_sprout).second.probability * log_value * (*cur_node).second.count;

 long double cut_branch_prob = logl((*cur_node).second.father->sprouts[(*it_sprout).first].count) - logl((*cur_node).second.father->count);
 long double sprout_prob = logl((*it_sprout).second.count) - logl((*cur_node).second.count);
 long double product = getKLDivergence(sprout_prob, cut_branch_prob);
 product *= (*cur_node).second.count; // Weighted divergence

 // THIS IS A TEST! NOT FORMALY SPECIFIED.
 //product *= (1.0 + ((*cur_node).second.depth / algoParam.contextTree_max_order));

 if ((*cur_node).second.count > max_count) {
 max_count = (*cur_node).second.count;
 max_node = cur_node;
 }
 delta += product;
 }
 // Decide pruning or not.
 //vout << "          Delta: " << delta << "; cut value: " << cut_value << endl;
 if (delta < cut_value) { // prune
 (*cur_node).second.father->branches.erase(cur_node);
 // Warning: now cur_node is invalid for the rest of this iteration.
 times_pruned++;
 is_being_pruned = true;
 } else {
 // Leave this branch on the tree.
 // is_final_leaf prevents the loop from analyzing this branch again.
 (*cur_node).second.is_final_leaf = true;
 times_not_pruned++;
 }
 } else {
 // Node it not a leaf. Put sub-nodes on stack.
 for (it_node = (*cur_node).second.branches.begin(); it_node != (*cur_node).second.branches.end();
 it_node++) {
 if ((*it_node).second.is_final_leaf == false) {
 nodes_stack.push(it_node);
 }
 }
 }
 }
 voutLow << "\tTimes pruned: " << times_pruned << " Times not pruned: " << times_not_pruned << endl;
 if (is_being_pruned)
 voutLow << "\tMax branch count: " << max_count << " with size: " << (*max_node).second.depth << endl;
 }

 return true;
 }*/

/**
 * @brief Remove nodes if the KL Divergence with respect to their parents is less than the cut value (#getCutValue()).
 *
 * This implementation traverses the tree in a top-down manner.
 * Gives a slightly different result than the bottom-up approach.
 */
bool AffixTree::prune() {
    vout << "[Prunning " << getName() << " Tree]" << endl;

    long double cut_value = getCutValue();
    stack<node*> nodes_stack;
    nodes_stack.push(&root_);
    int times_pruned = 0, times_not_pruned = 0;
    node* cur_node;
    while (!nodes_stack.empty()) {
        cur_node = nodes_stack.top();
        nodes_stack.pop();
        // Copy subnodes references in order to be able to erase them from cur_node.branches (i.e., we can't iterate over cur_node.branches and erase a branch).
        deque<node*> subnodes;
        for (auto it_subnodes =
                (*cur_node).branches.begin();
                it_subnodes != (*cur_node).branches.end(); it_subnodes++) {
            subnodes.push_back(&(*it_subnodes).second);
        }
        for (unsigned int i = 0; i < subnodes.size(); i++) {
            node* subnode = subnodes[i];
            long double delta = 0.0;
            for (map<string, sprout>::iterator it_sprout =
                    (*subnode).sprouts.begin();
                    it_sprout != (*subnode).sprouts.end(); it_sprout++) {
                long double father_sprout_prob = logl(
                        (*cur_node).sprouts[(*it_sprout).first].count)
                        - logl((*cur_node).count);
                long double child_sprout_prob = logl((*it_sprout).second.count)
                        - logl((*subnode).count);
                long double product = getKLDivergence(child_sprout_prob,
                        father_sprout_prob);
                product *= (*subnode).count; // Weighted divergence
                delta += product;
            }
            // Decide pruning or not.
            if (delta < cut_value) { // prune
                (*cur_node).branches.erase((*subnode).name);
                times_pruned++;
            } else {
                // Leave this node on the tree.
                nodes_stack.push(subnode);
                times_not_pruned++;
            }
        }
        if (!(*cur_node).branches.empty()) {
            (*cur_node).is_final_leaf = true;
        }
    }
    voutLow << "\tTimes pruned: " << times_pruned << " Times not pruned: "
            << times_not_pruned << endl;
    return true;
}

long double AffixTree::getCutValue() {
    // C > 2|X|+4 (? appearing to be not correct.)
    // K = Kn ~ C * log(n)
    // So considering some other constant.

    long double Kn = 0.0;
//	long double C = average_sentence_size;
    if (cut_value_ >= 0.0) {
        Kn = cut_value_;
    } else {
//        Kn = ((long double)log((long double)input_size) / (long double)log((long double)getNumberOfTags()) * C);
        Kn = 50.0;
    }
    //Kn = Kn * 20.0;
    /*voutLow << "SuffixTree: Kn = " << Kn
     << "   (C: " << C << " |words|: " << input_size << " |tags|: " << getNumberOfTags()
     << "   log(|words|)/log(|tags|): " << (long double)(log((long double)input_size) / log((long double)getNumberOfTags()))
     << endl;*/

    return Kn;
}
// */

/**
 * @brief Get Kullback-Leibler divergence of two log probabilities.
 */
long double AffixTree::getKLDivergence(long double logprob_uv,
        long double logprob_v) {
    return expl(logprob_uv) * (logprob_uv - logprob_v);
}

///////////////////////////////////////////////////////////////////////
void AffixTree::refreshNodesParents() {
    map<char, node>::iterator it_node;
    stack<node*> nodes_stack;

    root_.parent = NULL;
    nodes_stack.push(&root_);
    node* cur_node;
    while (!nodes_stack.empty()) {
        cur_node = nodes_stack.top();
        nodes_stack.pop();
        for (it_node = (*cur_node).branches.begin();
                it_node != (*cur_node).branches.end(); it_node++) {
            (*it_node).second.parent = cur_node;
            nodes_stack.push(&(*it_node).second);
        }
    }
    return;
}

///////////////////////////////////////////////////////////////////////

void AffixTree::print() {
    map<char, node>::iterator it;
    for (it = root_.branches.begin(); it != root_.branches.end(); it++) {
        cout << "-----------------------------" << endl;
        cout << (*it).second.depth << ":" << (*it).first << " |Branches| = "
                << (*it).second.branches.size() << " P = "
                //				<< (*it).second.probability
                ;
        cout << " father's depth: " << (*it).second.parent->depth << endl;

        deque<map<char, node>::iterator> queue;
        queue.push_back(it);
        map<char, node>::iterator it_father;
        while (!queue.empty()) {
            it_father = queue.front();
            queue.pop_front();
            //cout << "-- depth: " << (*it_father).second.depth << endl;
            map<char, node>::iterator it_son;
            for (it_son = (*it_father).second.branches.begin();
                    it_son != (*it_father).second.branches.end(); it_son++) {
                cout << (*it_son).second.depth << ":" << (*it_son).first << "["
                        << (*it_son).second.branches.size() << "]("
                        //						<< (*it_son).second.probability
                        << ")";
                cout << " - father's depth: " << (*it_son).second.parent->depth
                        << endl;
                queue.push_back(it_son);
            }
        }
    }
    cout << "-----------------------------" << endl;
}

void AffixTree::printStatistics() {
    int max_size = 0;
    int sum_size = 0;
    int num_branches = 0;
    deque<char> max_branch;
    vector<deque<char> > max_branches;
    map<int, int> branches_sizes; // key: branch size; data: number of branches with this size.

    deque<decltype(root_.branches.begin())> queue;
    for (auto it_from = root_.branches.begin(); it_from != root_.branches.end();
            it_from++) {
        queue.push_back(it_from);
    }
    decltype(root_.branches.begin()) it_father;
    deque<char> branch;
    int last_depth = 0;
    while (!queue.empty()) {
        it_father = queue.front();
        queue.pop_front();
        while ((*it_father).second.depth < last_depth) {
            branch.pop_back();
            last_depth--;
        }
        branch.push_back((*it_father).first);
        if ((*it_father).second.branches.empty()) { // is a leaf
            sum_size += (*it_father).second.depth;
            branches_sizes[(*it_father).second.depth]++;
            num_branches++;
            if ((*it_father).second.depth > max_size) {
                max_size = (*it_father).second.depth;
                max_branches.clear();
                max_branch = branch;
            }
            if ((*it_father).second.depth == max_size) {
                max_branches.push_back(branch);
            }
            branch.pop_back();
            last_depth = (*it_father).second.depth;
        } else {
            for (auto it_son = (*it_father).second.branches.begin();
                    it_son != (*it_father).second.branches.end(); it_son++) {
                queue.push_front(it_son);
            }
        }
    }

    long double average_size = (long double) sum_size
            / (long double) num_branches;
    vout << "[" << getName() << "Tree Statistics: ]" << endl;
    long double k = getCutValue(); //(_use_global_cut_value && algoParam.K_is_set) ? algoParam.K : _cut_value;
    vout << "\tCut value (K): " << k << "; density:  "
            << getDensity() << endl;
    vout << "\tNumber of branches: " << num_branches << endl;
    vout << "\tAverage branch size: " << average_size << endl;
    vout << "\tGreatest branch size: " << max_size << ", of branch: ";
    if (vout.output_priority <= vout.allowed_priority) {
        vout << ToString(max_branch) << endl;

        map<int, int>::iterator it_br;
        for (it_br = branches_sizes.begin(); it_br != branches_sizes.end();
                it_br++) {
            vout << "\tNumber of branches with size (" << (*it_br).first
                    << "): " << (*it_br).second << endl;
        }
    }
    voutLow << "\tBranches with this size:" << endl;
    if (vout.output_priority <= vout.allowed_priority) {
        for (unsigned int i = 0; i < max_branches.size(); i++) {
            voutLow << "\t\t" << ToString(max_branches[i]) << endl;
        }
    }
    vout << "[End Tree Statistics]" << endl;
}

void AffixTree::printSimpleStatistics() {
    int max_size = 0;
    int sum_size = 0;
    int num_branches = 0;
    string max_branch;

    deque<map<char, node>::iterator> queue;
    map<char, node>::iterator it_from;
    for (it_from = root_.branches.begin(); it_from != root_.branches.end();
            it_from++) {
        queue.push_back(it_from);
    }
    map<char, node>::iterator it_father;
    string branch;
    int last_depth = 0;
    while (!queue.empty()) {
        it_father = queue.front();
        queue.pop_front();
        while ((*it_father).second.depth < last_depth) {
            branch.erase(branch.size() - 1, branch.size()); //pop_back();
            last_depth--;
        }
        branch.push_back((*it_father).first);
        if ((*it_father).second.branches.empty()) { // is a leaf
            sum_size += (*it_father).second.depth;
            num_branches++;
            if ((*it_father).second.depth > max_size) {
                max_size = (*it_father).second.depth;
                max_branch = branch;
            }
            branch.erase(branch.size() - 1, branch.size()); //pop_back();
            last_depth = (*it_father).second.depth;
        } else {
            map<char, node>::iterator it_son;
            for (it_son = (*it_father).second.branches.begin();
                    it_son != (*it_father).second.branches.end(); it_son++) {
                queue.push_front(it_son);
            }
        }
    }

    long double average_size = (long double) sum_size
            / (long double) num_branches;
    vout << "[Affix Tree Statistics: ]" << endl;
    vout << "\tAverage branch size: " << average_size << endl;
    vout << "\tGreatest branch size: " << max_size << ", of branch: "
            << max_branch;
    vout << endl;
}

///////////////////////////////////////////////////////////////////////
// File operations.

/*/ Write tree to file.
 bool AffixTree::writeToFile(ofstream & outfile) //map<string, node>::iterator it_from)
 {
 //ofstream outfile;
 //outfile.open(file_name.c_str(), ofstream::app);
 if (!outfile.good()) {
 //cerr << "Error: could not open file '" << file_name << "' for writing the context tree" << endl;
 return false;
 }

 deque<map<char, node>::iterator> queue;
 int cur_depth = 0;
 int level_branch_counter = 1;

 // FORMAT: "depth:probability:branches.size():branch1name,b1number;branch2name,b2number;:sprout1name,probability;sprout2name,probability;:\n".
 // A branch number is its depth dot its order of appearaence at this depth (format: #.#).
 outfile << "AFFIX_TREE=" << endl;
 outfile << root_.depth << "@@" << root_.probability << "@@"
 << root_.branches.size() << "@@";

 map<char, node>::iterator it;
 int i = 1;
 for (it = root_.branches.begin(); it != root_.branches.end(); it++, i++) {
 outfile << (*it).first << "~" << (*it).second.depth << "." << i << "^";
 queue.push_back(it);
 }
 outfile << "@@";
 map<string, sprout>::iterator it_sprout;
 for (it_sprout = root_.sprouts.begin(); it_sprout != root_.sprouts.end();
 it_sprout++) {
 outfile << (*it_sprout).first << "," << (*it_sprout).second.probability
 << ";";
 }
 outfile << "@@" << endl;

 map<char, node>::iterator it_node;
 while (!queue.empty()) {
 it_node = queue.front();
 queue.pop_front();

 if ((*it_node).second.depth > cur_depth) { // First node of next level in the tree.
 cur_depth = (*it_node).second.depth;
 level_branch_counter = 1;
 }

 outfile << (*it_node).second.depth << "." << level_branch_counter
 << "@@" << (*it_node).second.probability << "@@"
 << (*it_node).second.branches.size() << "@@";

 level_branch_counter++;

 //map<char, node>::iterator it_son;
 int i = 1;
 for (it = (*it_node).second.branches.begin();
 it != (*it_node).second.branches.end(); i++, it++) {
 outfile << (*it).first << "~" << (*it).second.depth << "." << i
 << "^";
 queue.push_back(it);
 }
 outfile << "@@";
 for (it_sprout = (*it_node).second.sprouts.begin();
 it_sprout != (*it_node).second.sprouts.end(); it_sprout++) {
 outfile << (*it_sprout).first << ","
 << (*it_sprout).second.probability << ";";
 }
 outfile << "@@" << endl;
 }

 return true;
 }
 // */

// Read tree from file.
//bool AffixTree::readFromFile(ifstream & infile) //map<char, node>::iterator it_from)
//		{
//	//ifstream infile;
//	//infile.open(file_name.c_str());
//	if (!infile.good()) {
//		//cerr << "Error: could not open file '" << file_name << "' for reading the context tree" << endl;
//		return false;
//	}
//
//	//deque< map<string, node>::iterator > queue;
//	//int cur_depth = 0;
//	//int level_branch_counter = 1;
//
/////////////
//
//	string line;
//	string delim;
//	vector<string> line_tokens;
//	int line_number = 0;
//
//	/* No more necessary. We're using infile (ifstream), no file_name (string).
//	 bool file_header_ok = false;
//	 while (true) {
//	 line_number++;
//	 infile >> line;
//	 if (infile.eof()) break;
//	 if (line == "TREE=") {
//	 file_header_ok = true;
//	 break;
//	 }
//	 }
//
//	 if (!file_header_ok) {
//	 cerr << "ERROR: context tree file [" << file_name << "]: invalid file format" << endl;
//	 return false;
//	 }
//	 */
//
//	//deque< map<string, node>::iterator > queue;
//	//queue.push_back(it);
//	//map<string, node>::iterator it_father;
//	//node * p_father;
//	vector<node*> p_upper_nodes;
//	vector<node*> p_cur_nodes;
//	int cur_father = -1;
//	int previous_depth = -1;
//	int cur_level_node = 0;
//	map<char, node>::iterator it_cur_father_branches;
//
//	int position_before;
//	while (true) {
//		line_number++;
//		position_before = infile.tellg();
//		infile >> line;
//		//cout << "Read line: " << line << endl;
//		if (infile.eof())
//			break;
//		if (line == "TREE=") {
//			infile.seekg(position_before);
//			break;
//		}
//
//		delim = "@@";
//		line_tokens = StringTokenize(line, delim);
//		//cout << "|line_tokens|: " << line_tokens.size() << endl;
//
//		//copy(line_tokens.begin(), line_tokens.end(), ostream_iterator<string>(cout, "\n"));
//		//cout << endl;
//
//		if (line_tokens.size() != 5) {
//			cerr << "ERROR[1]: affix tree file on line " << line_number
//					<< ": invalid line format" << endl;
//			return false;
//		}
//
//		// FORMAT: "depth:probability:branches.size():branch1name,b1number;branch2name,b2number;:sprout1name,probability;sprout2name,probability;:\n".
//		// A branch number is its depth dot its order of appearance at this depth (format: #.#).
//
//		node a_node;
//		sprout a_sprout;
//		a_sprout.count = 0.0;
//		int depth;
//		long double probability;
//		int num_branches;
//		//string branch_name;
//		//char branch_name;
//		//long double branch_probability;
//		string sprout_name;
//		//long double sprout_probability;
//
//		// First field [depth.order]
//		vector<string> temp_tokens = StringTokenize(line_tokens[0], "."); // get depth.order
//		if (temp_tokens.size() == 0) {
//			cerr << "ERROR[2]: affix tree file on line " << line_number
//					<< ": invalid line format" << endl;
//			return false;
//		}
//		depth = atoi(temp_tokens[0].c_str());
//		if (depth == 0) {
//			previous_depth = depth;
//		} else {
//			if (temp_tokens.size() != 2) {
//				cerr << "ERROR[3]: affix tree file on line " << line_number
//						<< ": invalid line format" << endl;
//				return false;
//			}
//			//level_order = atoi(temp_tokens[1].c_str());
//		}
//
//		a_node.depth = depth;
//
//		// Second field [probability|count]
//		probability = atof(line_tokens[1].c_str());
//
//		a_node.probability = probability;
//
//		// Third field [number of branches]
//		num_branches = atoi(line_tokens[2].c_str());
//
//		// Forth field [branches]
//		vector<string> branches = StringTokenize(line_tokens[3], "^");
//
//		if (num_branches != (int) branches.size()) {
//			cerr << "ERROR[4]: affix tree file on line " << line_number
//					<< ": invalid line format" << endl;
//			return false;
//		}
//
//		//cout << "Num branches from file: " << branches.size() << endl;
//		vector<string> name_number;
//		for (unsigned int i = 0; i < branches.size(); i++) {
//			name_number = StringTokenize(branches[i], "~");
//			//copy(name_number.begin(), name_number.end(), ostream_iterator<string>(cout, " "));
//			//cout << endl;
//			if (name_number.size() != 2) {
//				cerr << "ERROR[5]: affix tree file on line " << line_number
//						<< ": invalid line format" << endl;
//				return false;
//			}
//			a_node.branches[name_number[0][0]];
//		}
//
//		// Fifth field [sprouts]
//		vector<string> sprouts = StringTokenize(line_tokens[4], ";");
//		vector<string> name_prob;
//		for (unsigned int i = 0; i < sprouts.size(); i++) {
//			name_prob = StringTokenize(sprouts[i], ",");
//			if (name_prob.size() != 2) {
//				cerr << "ERROR[6]: affix tree file on line " << line_number
//						<< ": invalid line format" << endl;
//				return false;
//			}
//			a_sprout.probability = atof(name_prob[1].c_str());
//			a_node.sprouts[name_prob[0]] = a_sprout;
//		}
//		//cout << "Num sprouts: " << a_node.sprouts.size() << endl;
//
//		//cout << "line: " << line << endl;
//		//cout << "depth: " << depth << endl;
//
//		//////////////////
//		// Storing info //
//		if (depth == 0) { // is the ROOT
//			root_ = a_node;
//			//cout << "root sprouts: " << _root.sprouts.size() << endl;
//			p_upper_nodes.clear();
//			p_upper_nodes.push_back(&root_);
//			//p_father = &_root;
//			cur_father = 0;
//			it_cur_father_branches =
//					p_upper_nodes[cur_father]->branches.begin();
//
//			previous_depth = depth + 1;
//			cur_level_node = 1;
//			continue;
//		}
//		if (depth != previous_depth) {
//			p_upper_nodes = p_cur_nodes;
//			//cout << "p_upper size: " << p_upper_nodes.size() << endl;
//			p_cur_nodes.clear();
//			cur_father = 0;
//			it_cur_father_branches =
//					(p_upper_nodes[cur_father])->branches.begin();
//
//			cur_level_node = 1;
//			previous_depth = depth;
//		}
//		while (it_cur_father_branches
//				== (p_upper_nodes[cur_father])->branches.end()) {
//			//cout << "Cur father: " << cur_father << endl;
//			cur_father++;
//			if (cur_father >= (int) p_upper_nodes.size()) {
//				cerr << "ERROR[7]: affix tree file on line " << line_number
//						<< ": invalid line format" << endl;
//				return false;
//			}
//			it_cur_father_branches =
//					(p_upper_nodes[cur_father])->branches.begin();
//		}
//		//cout << "Sprouts: " << a_node.sprouts.size() << endl;
//		//cout << "cur branch name: " << (*it_cur_father_branches).first << endl;
//		(*it_cur_father_branches).second = a_node;
//		(*it_cur_father_branches).second.father = &(*p_upper_nodes[cur_father]);
//		//cout << "asldkjf " << (*it_cur_father_branches).second.sprouts.size() << endl;
//		//cout << "kljdsfg " << (*it_cur_father_branches).second.father->branches.size() << endl;
//		p_cur_nodes.push_back(&((*it_cur_father_branches).second));
//		it_cur_father_branches++;
//
//		cur_level_node++;
//	}
//
//	//print();
//
//	return true;
//}
