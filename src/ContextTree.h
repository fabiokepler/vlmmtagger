/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2013 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
******************************************************************************/

#ifndef CONTEXTTREE_H
#define CONTEXTTREE_H

#include "main.h"
#include "functions.h"
#include <stack>


class ContextTree{
public:
    class sprout {
    public:
    	long double probability;
    	long double count;

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
        	ar & BOOST_SERIALIZATION_NVP(probability);
        	ar & BOOST_SERIALIZATION_NVP(count);
        }
    };

    class node {
    public:
        int depth;
        string name;
        long double probability;
        long double count;
        bool is_final_leaf; // Prevents the pruning loop from analysing this node again.
        map<string, node> branches;
        map<string, sprout> sprouts;
        node* parent;

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
        	ar & BOOST_SERIALIZATION_NVP(depth);
        	ar & BOOST_SERIALIZATION_NVP(name);
        	ar & BOOST_SERIALIZATION_NVP(probability);
        	ar & BOOST_SERIALIZATION_NVP(count);
        	ar & BOOST_SERIALIZATION_NVP(is_final_leaf);
        	ar & BOOST_SERIALIZATION_NVP(branches);
        	ar & BOOST_SERIALIZATION_NVP(sprouts);
//        	ar & BOOST_SERIALIZATION_NVP(father); // Doesn't work
        };

        string toString() {
            return "(" + name + "; depth: " + ToString(depth) + "; count: " + ToString(count) + "; parent: "
                    + (parent != NULL ? parent->name : "null") + "; number of sprouts: " + ToString(sprouts.size()) + "; number of branches: "
                    + ToString(branches.size()) + ")";
        }

    };

    ContextTree();
    ContextTree(int min_times);
    virtual ~ContextTree();

    inline void setName(const string& name) { name_ = name; };
    inline const string getName() { return name_; };

    // Mutators
    void setMinimumTimesForBranchesToBeInTree(int min_times);
    void setCutValue(long double K);
    inline void setMinimumBranchSize(unsigned int min_size) { minimum_branch_size_ = min_size; };
    void setAverageSentenceSize(long double averageSentenceSize) {
        average_sentence_size_ = averageSentenceSize;
    }
    void setInputSize(int inputSize) {
        input_size_ = inputSize;
    }

    void addBranch(deque<string> nodes);
    void addBranch(const string& dest_sprout, const deque<string>& branch);
    void addBranch(const string& dest_sprout, const deque<string>& branch, const deque<bool>& ambiguity);
    //void addBranch(deque<string> nodes, string word);
    //void descendBranch(map<string, node>::iterator it_from);
    void createBranch(const string& dest_sprout, const deque<string>& branch, long double initial_sprout_prob = 0.0);

    bool setSproutProbability(const string& dest_sprout, const deque<string>& branch, long double probability);
    bool addToSproutProbability(const string& dest_sprout, const deque<string>& branch, long double probability);
    bool setSproutWeight(const string& dest_sprout, const deque<string>& branch, long double count);
    bool addToSproutWeight(const string& dest_sprout, const deque<string>& branch, long double count);

    bool addToSproutsProbabilityAlongBranch(const string& dest_sprout, const deque<string>& branch, long double probability);

    long double setSproutsProbabilityToWeightTimesFactor(long double factor);
    long double applyLogToSproutsProbability();

    void normalizeNode(node* ni);

    void normalizeProbabilities();
    void shake(bool apply_to_sprouts = false);
    bool prune();
    long double getCutValue();
    bool pruneSprouts(long double cut_value = -1.0);
    void cutShortBranches(unsigned int min_size = 0);

    long double getKLDivergence(long double logprob_uv, long double logprob_v);

    long double runTraining(unsigned int iterations = 2);

    long double reset(long double initial_count = 1.0);
    long double resetProbabilities(long double initial_probability = 1.0);
    unsigned int resetLeavesStatus(bool is_final_leaf = false);

    // Accessors
    node* getRoot() { return &root_; };
    unsigned int getDeepestLevelNumber();
    inline unsigned int getMinimumBranchSize() { return minimum_branch_size_; };
    long double getSproutProbability(string sprout_name, const deque<string>& branch);
    long double getSproutProbability(string sprout_name, const deque<string>& branch, deque<string>& used_branch);
    long double getSproutLastProbability(string sprout_name, const deque<string>& branch);
    long double getSproutLastProbability(string sprout_name, const deque<string>& branch, deque<string>& used_branch);
    long double getSproutLastProbability(string sprout_name, const deque<string>& branch, int& used_branch_size);
    long double getSproutLastWeight(string sprout_name, const deque<string>& branch);
    long double getSproutLastWeight(string sprout_name, const deque<string>& branch, deque<string>& used_branch);
    long double getSproutLogProbability(string sprout_name, const deque<string>& branch, deque<string>& used_branch);
    long double getSproutAccumulatedProbability(string sprout_name, const deque<string>& branch, deque<string>& used_branch);
    long double getSproutAccumulatedProbability(string sprout_name, const deque<string>& branch, int& used_branch_size);
    long double getSproutStarProbability(string sprout_name, const deque<string>& branch, const deque<bool>& ambiguity);
    long double getSproutStarProbability(string sprout_name, const deque<string>& branch, const deque<bool>& ambiguity, deque<string>& used_branch);
    long double getSproutProbabilityWithStars(string sprout_name, const deque<string>& branch, const deque<unsigned int>& ambiguity);
    long double getSproutWeight(string sprout_name, const deque<string>& branch);
    long double getSproutWeight(string sprout_name, const deque<string>& branch, deque<string>& used_branch);
    long double getSproutWeight(string sprout_name, const deque<string>& branch, deque<string>& used_branch, long double& branch_weight);

    deque<string> getSproutsOfRoot();
    deque<string> getSproutsOfNode(string node_name);
    deque<string> getSproutsNameOfNode(node* node);
    deque<string> getSproutsOfBranch(deque<string> branch);
    deque<string> getRelevantContextOfHistory(string present, vector<string> history);
    int getNumberOfTags() {return (int)root_.branches.size();};
    deque<string> getBranchOfNode(node*);
    node* getLastNodeOfBranch(const deque<string>& branch);
    node* getLastNodeOfBranchWithSprout(const deque<string>& branch, const string& sprout);
    node* getFirstNode(node*);
    deque<node*> getLeafNodes();
    long double getAverageSentenceSize() const {
        return average_sentence_size_;
    }
    int getInputSize() const {
        return input_size_;
    }
    inline const string starNodeRepresentation() {return star_node_;};

    void refreshNodesParents();

    // Utilities
    void printSpecialBranch(string special_sub_root);
    void print();
    void printStatistics();

    bool writeToFile(ofstream & outfile);
    bool readFromFile(ifstream & infile);


private:
    void init();
    ContextTree::sprout newSprout();
    ContextTree::sprout newSprout(long double initial_prob);
    ContextTree::node newNode();

    node root_;
    string star_node_;

    string name_;
    unsigned int deepest_level_number_;

    int minimum_times_to_be_in_tree_;
    long double cut_value_;
    unsigned int minimum_branch_size_;
    int input_size_; // number of words on the training corpus
    long double average_sentence_size_;


    friend class boost::serialization::access;
    // When the class Archive corresponds to an output archive, the
    // & operator is defined similar to <<.  Likewise, when the class Archive
    // is a type of input archive the & operator is defined similar to >>.
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & BOOST_SERIALIZATION_NVP(input_size_);
        ar & BOOST_SERIALIZATION_NVP(average_sentence_size_);
        ar & star_node_;
        ar & BOOST_SERIALIZATION_NVP(name_);
        ar & BOOST_SERIALIZATION_NVP(deepest_level_number_);
        ar & BOOST_SERIALIZATION_NVP(minimum_times_to_be_in_tree_);
        ar & BOOST_SERIALIZATION_NVP(cut_value_);
        ar & BOOST_SERIALIZATION_NVP(minimum_branch_size_);

        ar & BOOST_SERIALIZATION_NVP(root_);

        refreshNodesParents();
    }
};

BOOST_CLASS_VERSION(ContextTree, 2)
BOOST_CLASS_VERSION(ContextTree::node, 2)
BOOST_CLASS_VERSION(ContextTree::sprout, 2)
//BOOST_CLASS_TRACKING(ContextTree::node, boost::serialization::track_always);

#endif // CONTEXTTREE_H
