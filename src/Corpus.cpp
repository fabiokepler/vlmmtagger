/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2013 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

/* 
 * File:   Corpus.cpp
 * Author: kepler
 * 
 * Created on November 23, 2011, 11:55 AM
 */

#include "Corpus.h"
#include "FileHandler.h"

void Corpus::init() {
    number_of_words = number_of_tags = number_of_sentences = 0;
    is_tagged = false;
}

Corpus::Corpus() {
    init();
}

Corpus::Corpus(const vector<vector<string> >& sentences_words, const vector<vector<string> >& sentences_tags) {
    init();
    this->sentences_words_ = sentences_words;
    this->sentences_tags_ = sentences_tags;
    is_tagged = true;
    count();
}

Corpus::~Corpus() {
}

bool Corpus::setSentences(vector<vector<string> >& sentences_words, vector<vector<string> >& sentences_tags) {
    if (this->is_tagged == false) {
        cerr << "Tried to set the sentences of words and tags on an untagged corpus. Flagging corpus as tagged."
                << endl;
        this->is_tagged = true;
    }
    this->sentences_words_ = sentences_words;
    this->sentences_tags_ = sentences_tags;
    count();
    return true;
}

bool Corpus::setSentences(vector<vector<string> >& sentences_words) {
    if (this->is_tagged == true) {
        cerr << "Tried to set just the sentences of words on a tagged corpus. Flagging corpus as untagged." << endl;
        this->is_tagged = false;
        this->sentences_tags_.clear();
    }
    this->sentences_words_ = sentences_words;
    count();
    return true;
}

bool Corpus::addSentences(vector<vector<string> >& sentences_words, vector<vector<string> >& sentences_tags) {
    if (this->is_tagged == false) {
        cerr << "Tried to append sentences of words and tags to an untagged corpus. Operation cancelled." << endl;
        return false;
    }
    this->sentences_words_.insert(this->sentences_words_.end(), sentences_words.begin(), sentences_words.end());
    this->sentences_tags_.insert(this->sentences_tags_.end(), sentences_tags.begin(), sentences_tags.end());
    count();
    return true;
}

bool Corpus::addSentences(vector<vector<string> >& sentences_words) {
    if (this->is_tagged == true) {
        cerr << "Tried to append sentences of just words to a tagged corpus. Operation cancelled." << endl;
        return false;
    }
    this->sentences_words_.insert(this->sentences_words_.end(), sentences_words.begin(), sentences_words.end());
    count();
    return true;
}

bool Corpus::splitRandomly(Corpus& second_corpus, double percentage_to_keep) {
    if (percentage_to_keep <= 0.0 or percentage_to_keep > 1.0) {
        cerr << "Splitting corpus: invalid percentage: " << percentage_to_keep << "; must be between 0 and 1." << endl;
        return false;
    }
    srand(time(NULL));
    vector<vector<string> > sentences_words_to_keep, sentences_tags_to_keep;
    vector<vector<string> > sentences_words_new, sentences_tags_new;

    for (unsigned int i = 0; i < number_of_sentences; i++) {
        if (((long double) rand() / RAND_MAX) < percentage_to_keep) {
            sentences_words_to_keep.push_back(sentences_words_[i]);
            sentences_tags_to_keep.push_back(sentences_tags_[i]);
        } else {
            sentences_words_new.push_back(sentences_words_[i]); //.insert(sentences_words_new.end(), sentences_words[i].begin(), sentences_words[i].end());
            sentences_tags_new.push_back(sentences_tags_[i]);
        }
    }
    setSentences(sentences_words_to_keep, sentences_tags_to_keep);
    second_corpus.addSentences(sentences_words_new, sentences_tags_new);

    return true;
}

bool Corpus::loadTaggedText(string filename, map<unsigned int, string> separators,
        set<string> final_punctuation_symbols, unsigned int max_sentence_size = 0) {
    if (hasData() and !isTagged()) {
        cerr << "Error adding tagged file '" << filename << "' to corpus: corpus already has untagged data." << endl;
        return false;
    }
    setSeparators(separators);
    setFinalPunctuationSymbols(final_punctuation_symbols);


//    vector<string> words, tags;
//    if (!FileHandler::ReadTaggedText(filename, separators, words, tags)) {
    vector<vector<string>> sentences_words_new, sentences_tags_new;
    set<string> eos_tags;
    eos_tags.insert(".");
    // TODO: replace final_punctuation with this (even in configuration)
    if (!FileHandler::ReadTaggedText(filename, sentences_words_new, sentences_tags_new, separators, eos_tags)) {
        return false;
    }
    this->is_tagged = true;

    // Build hash (map) of sentences by method A or B:
    // A. based on a final punctuation tag
/*
    set<string> eos_tags;
    eos_tags.insert(".");
    auto sentences_tags_new = VectorStringTokenize(tags, eos_tags);
    auto sentences_words_new = ImitateVectorStringTokenize(words, sentences_tags_new);
    if (sentences_tags_new.size() <= 1) {
        // B. based on final punctuation tokens
        // (this is not the task known as "sentence segmentation", since we already
        // have punctuation marks tokenized).
        auto sentences_words_new = VectorStringTokenize(words, final_punctuation_symbols);
        auto sentences_tags_new = ImitateVectorStringTokenize(tags, sentences_words_new);
    }
    words.clear();
    tags.clear();
*/

    // Append new sentences to current ones.
    if (max_sentence_size > 0) {
        // Appending only sentences of "max-length".
        unsigned int n = 0;
        for (unsigned int s = 0; s < sentences_words_new.size(); s++) {
            if (sentences_words_new[s].size() <= max_sentence_size) {
                this->sentences_words_.push_back(sentences_words_new[s]);
                this->sentences_tags_.push_back(sentences_tags_new[s]);
                n++;
            }
        }
    } else {
        addSentences(sentences_words_new, sentences_tags_new);
    }
    count();
    return true;
}

bool Corpus::loadUntaggedText(string filename, set<string> final_punctuation_symbols,
        unsigned int max_sentence_size = 0) {
    if (hasData() and isTagged()) {
        cerr << "Error adding untagged file '" << filename << "' to corpus: corpus already has tagged data." << endl;
        return false;
    }

    setFinalPunctuationSymbols(final_punctuation_symbols);
    vector<string> words;

    if (!FileHandler::ReadUntaggedText(filename, words)) {
        return false;
    }
    this->is_tagged = false;

    // Build hash (map) of sentences based on final punctuation tokens
    // (this is not the task known as "sentence segmentation", since we already
    // have punctuation marks tokenized).
    // TODO: FIXME: for untagged text, we have to use a sentence segmenter/detector,
    //              or a tokenizer and then the method below (which only works if
    //              the punctuation symbols are not adjacent to words).
    auto sentences_words_new = VectorStringTokenize(words, final_punctuation_symbols);
    words.clear();

    // Append new sentences to current ones.
    if (max_sentence_size > 0) {
        // Appending only sentences of "max-length".
        unsigned int n = 0;
        for (unsigned int s = 0; s < sentences_words_new.size(); s++) {
            if (sentences_words_new[s].size() <= max_sentence_size) {
                this->sentences_words_.push_back(sentences_words_new[s]);
                n++;
            }
        }
    } else {
        addSentences(sentences_words_new);
    }
    count();
    return true;
}

bool Corpus::saveTaggedText(string filename, map<unsigned int, string> separators) {
    if (!hasData()) {
        cerr << "Error: corpus is empty. Not saving to file." << endl;
        return false;
    }
    if (!isTagged()) {
        cerr << "Error: trying to save a tagged file, but the corpus is not tagged." << endl;
        return false;
    }
    return FileHandler::WriteTaggedText(filename, separators, this->sentences_words_, this->sentences_tags_);
}

bool Corpus::saveUntaggedText(string filename) {
    if (!hasData()) {
        cerr << "Error: corpus is empty. Not saving to file." << endl;
        return false;
    }
    if (isTagged()) {
        cerr << "Warning: trying to save an untagged file, but the corpus is tagged. Ignoring tags and continuing."
                << endl;
    }
    return FileHandler::WriteText(filename, this->sentences_words_); //, "\n", " ");
}
