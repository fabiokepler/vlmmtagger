/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2013 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
******************************************************************************/

/* 
 * File:   ProgramOptions.cpp
 * Author: kepler
 * 
 */

#include <iostream>
#include <fstream>
#include <exception>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/program_options.hpp>
#include <boost/algorithm/string/replace.hpp>

#include "ProgramOptions.h"
#include "functions.h"

using namespace std;

ProgramOptions::ProgramOptions(string program_name, string version, string contact) {
    program_name_ = program_name;
    version_ = version;
    contact_ = contact;
    init();
}

ProgramOptions::~ProgramOptions() {
}

void ProgramOptions::init() {
    separators[0] = "/";
    separators[1] = " ";

    // Configurations (TODO: read from a config file)
    final_punctuation_symbols.insert(".");
    final_punctuation_symbols.insert("?");
    final_punctuation_symbols.insert("!");
    final_punctuation_symbols.insert(";");
    final_punctuation_symbols.insert(":");
    final_punctuation_symbols.insert("...");


//    train_file = false;
//    test_file = false;
//    test_file_not_tagged = false;
//    select_part_of_train_corpus = false;
//    corpus_file = false;
//    divide_corpus_in_train_and_test = 0.75; // Default. But only applied if 'corpus_file == true'.
//
//    save_trained_chain_file = false;
//    load_trained_chain_file = false;
//    save_counted_chain_file = false;
//    load_counted_chain_file = false;
//
//    load_trained_chain_file_name = "trained.data";
//
//    /// Get list of composed words and tags.
//    extract_split_words = false;
//    extract_composed_tags = false;
//
//    count_tags = false;

}

int ProgramOptions::parseArguments(int argc, char** argv)
{
    try {
        po::options_description generic("Generic options");
        generic.add_options()
            ("version", "Show version number.")
            ("help,h", "Show help message.")
            ("verbose,v", "Verbose mode: do show running state messages.")
            ("quiet,q", "Quiet mode: do not show unnecessary messages.")
            ;

        po::options_description config("General options");
        config.add_options()
            ("save-id", po::value<string>(), "Set project id. Everything going to be saved to files will use the specified id as a prefix, "
                                             "unless overridden by an explicit option. E.g., if '-save-config test.conf' is passed in the command line "
                                             "the file name used will be '#ID.test.conf'; if '-save-config testing.conf' is used and save-id not, the file name "
                                             "will be 'testing.conf'.")
//            ("load-id", po::value<string>(), "Set project id. Everything going to be loaded from files will use the specified id as a prefix. This is the opposite of 'save-id'.")
            ("save-config", po::value<string>(), "Save all specified options to the given file name (but check the 'save-id' option).")
            ("load-config", po::value<string>(), "Load options from the given file name.")
            ;

        po::options_description ioput("Input/output");
        ioput.add_options()
            ("train", po::value< vector<string> >()->multitoken()->composing(), "files to use for training the model (files must be tagged).")
            ("test", po::value< vector<string> >()->multitoken()->composing(), "files to use for testing a trained model (files must be tagged; a new tagged file is saved under the name FILE.pos).")
            ("tag", po::value< vector<string> >()->multitoken()->composing(), "files to tag using the trained model (files must NOT be tagged; a new tagged file is saved under the name FILE.pos).")
            ("corpus", po::value<string>(), "corpus file to use for both training and testing a model (file must be tagged; see 'part-corpus' option).")
            ("part-corpus", po::value<double>()->default_value(0.75), "percentage of sentences from the corpus to use for training the model; the rest is added to the testing corpus (randomly selected; value must be between 0 and 1).")

            ("load-model", po::value<string>(), "Load trained model from a file; any other training option is ignored.")
            ("save-model", po::value<string>(), "Save model to a file after the training phase (check the 'save-id' option).")
//            ("save-cumulative", po::value<string>(), "Save unnormalized model to the specified file.")
//            ("load-cumulative", po::value<string>(), "Load unnormalized model from the specified file; other training options are NOT ignored, so this is useful for incrementing a previous model.")
//            ("save-iter", po::value<string>(), "Save model to 'arg-#i.vlmm' after each iteration of the training phase.")

//            ("eval,e", po::value<string>(), "Evaluate parsed file against gold trees given by --test option.")
            ("save-train,w", po::value<string>(), "Save the training corpus to a file.")
            ("save-test", po::value<string>(), "Save words/tags from testing corpus to a file.")
            ("save,o", po::value<string>(), "Save tagged corpus to file.")

            ("max-length,x", po::value<unsigned int>()->default_value(0), "Max allowable length of sentences to be tagged (0 for unlimited).")
            ("separators", po::value< vector<string> >()->multitoken(), "A separated list of up to 4 elements, where 1st element: word-tag separator in input files; 2nd: word-tag pairs separator in input files; 3rd and 4th: respectively for output files.")
            ;

        po::options_description model("Model options");
        model.add_options()
            ("cut-value,K", po::value<double>()->default_value(50.0), "Cut value for context tree pruning.")
            ("density", po::value<int>()->default_value(1), "Discard sequences occurring less than this number of times.")
            ("order", po::value<int>()->default_value(10), "Maximum length of sequences to consider.")
            ("suffix-size", po::value<double>(), "Percentage of a word's size to consider as its suffix (value between 0 and 1).")

            ("unknown-word-max-freq", po::value<unsigned int>()->default_value(5), "Minimum frequency of a word to be considered a known word.")
            ("open-class-threshold", po::value<unsigned int>()->default_value(10), "Minimum distinct word assigns for a tag to be opened (thus a candidate for unknown words).")

            ("training", "do training")
            ("iterations", po::value<int>()->default_value(0), "Max number of training iterations.")

            ("skip-build-lct", "skip building the left context tree")
            ("skip-build-wst", "skip building the word-suffixes tree")
            ("skip-build-twp", "skip building the tag-word probabilities table")
            ("skip-build-all", "skip building the 3 items above")
            ;

        // Hidden options, will be allowed both on command line and
        // in config file, but will not be shown to the user.
        po::options_description hidden("Experimental options");
        hidden.add_options()
            ("ignore-punctuation", "Ignore punctuation occurring in contexts.") //TODO: implement logic using Method.filterContext()
            ("simplify-tags", "Strip tags down to the first '+' or '-'.") //TODO: implement logic using Model.simplifyTags()
//            ("input-file", po::value< vector<string> >(), "input file")
            ;

        po::options_description cmdline_options;
        cmdline_options.add(generic).add(ioput).add(config).add(model).add(hidden);

        po::options_description config_file_options;
        config_file_options.add(ioput).add(config).add(model).add(hidden);

        po::options_description visible("Allowed options");
        visible.add(generic).add(config).add(ioput).add(model);//.add(hidden);

        //po::positional_options_description p;
        //p.add("input-file", -1);
        //store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
        po::parsed_options popts = po::command_line_parser(argc, argv)
                .options(cmdline_options)
                .style(po::command_line_style::unix_style|po::command_line_style::allow_long_disguise) // allow_long_disguise: long options can be specified with just one dash '-'.
                .run();
        po::store(popts, variables_map_);

        if (variables_map_.count("help")) {
            std::stringstream stream;
            stream << visible;
            string helpMsg = stream.str();
            boost::algorithm::replace_all(helpMsg, "--", "-"); // allow_long_disguise does not change the help message (https://svn.boost.org/trac/boost/ticket/4644)

            cout << program_name_ << ": Variable-Length Markov Model Tagger (version " << version_ << ").\n"
                << helpMsg << "\n"
                << "\nReport bugs to <fabio@kepler.pro.br>." << endl;
            return 0;
        }
        if (variables_map_.count("version")) {
            cout << program_name_ << " version " << version_ << endl
                << "Report bugs to <fabio@kepler.pro.br>." << endl;
            return 0;
        }

        try {
            // Read options from config file.
            if (isSet("load-config")) {
                string filename = get<string>("load-config");
                loadConfigFile(filename, config_file_options);
            }
        } catch (exception& e) {
            cerr << "Error loading config file: " << e.what() << ".\n";
            return -1;
        }
    } catch (exception& e) {
        cerr << "Error parsing program options: " << e.what() << ".\n";
        return -1;
    }

    try {
        // Save all options (from command line and config file)
        if (isSet("save-config")) {
            string filename = get<string>("save-config");
            if (isSet("save-id")) {
                filename = get<string>("save-id") + "." + filename;
            }
            saveConfigFile(filename, buildPropertyTree(variables_map_));
        }
    } catch (exception& e) {
        cerr << "Error saving config file: " << e.what() << ".\n";
        return -1;
    }
    return 1;
}

pt::ptree ProgramOptions::buildPropertyTree(po::variables_map& vm)
{
    pt::ptree pt;
    for (po::variables_map::iterator i = vm.begin(); i != vm.end(); i++) {
        string key = (*i).first;
        const po::variable_value& v = (*i).second;
        if (!v.empty()) {
            const ::std::type_info& type = v.value().type();
            if (type == typeid(string)) {
                const string& val = v.as<string>();
                pt.add(key, val);
            } else if (type == typeid(vector<string>)) {
                const vector<string>& val = v.as< vector<string> >();
                pt.add(key, ToString(val));
                //for (unsigned int j = 0; j < val.size(); j++)
                //    pt.add(key, val[j]);
            } else if (type == typeid(int)) {
                int val = v.as<int>();
                pt.add(key, val);
            } else if (type == typeid(double)) {
                double val = v.as<double>();
                pt.add(key, val);
            }
        } else {
            cout << "v.empty: " << i->first << endl;;
        }
    }
    return pt;
}

bool ProgramOptions::loadConfigFile(string filename, po::options_description config_file_options)
{
    ifstream config_stream(filename.c_str());
    po::store(po::parse_config_file(config_stream, config_file_options), variables_map_);
    po::notify(variables_map_);
    return true;
}

bool ProgramOptions::saveConfigFile(string filename, pt::ptree property_tree)
{
    pt::ini_parser::write_ini(filename, property_tree);
    return true;
}

void ProgramOptions::parseSeparators() {
    const unsigned int number_of_separators = 4;
    if (isSet("separators")) {
        vector<string> seps = get< vector<string> >("separators");
        for (unsigned int i = 0; i < seps.size() && i < number_of_separators; i++) {
            separators[i] = seps[i] == "\\n" ? "\n" : seps[i];
        }
    }
}

