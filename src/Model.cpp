/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2014 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

/*
 * Model.cpp
 *
 *      Author: Kepler
 */

#include "Model.h"

extern ui::UIHandler::VerboseOutput vout, voutHigh, voutLow;

Model::Model() {
    options_ = ModelOptions();
    initialize();

//    // Initializations
//    m_initial_state = "_INITIAL_STATE_";
//    m_unknown_word = "_UNKNOWN_WORD_";
//    m_input_words_size = m_input_tags_size = 0;
//    m_total_numerical_tags = m_total_uppercase_letter_tags = m_total_uppercase_word_tags = 0.0;
//    m_total_hyphen_word_tags = 0.0;
//    average_sentence_size_ = 0.0;

}

Model::~Model() {
}

bool Model::initialize() {
    is_trained = false;
    input_words_size_ = 0.0;
    input_tags_size_ = 0.0;
    input_sentences_size_ = 0.0;
    average_open_word_length_ = 0.0;

    // Unnecessary to initialize map when value is 0.
    /*
    initializeMap(tag_counter_, 0.0);
    initializeMap(word_counter_, 0.0);
    initializeMatrix(word_tag_counter_, 0.0);
    initializeMatrix(tag_word_counter_, 0.0);
    initializeMap(unknown_tag_counter_, 0.0);
    initializeMap(uppercase_letter_tags_counter_, 0.0);
    initializeMap(uppercase_word_tags_counter_, 0.0);
    initializeMap(numerical_tags_counter_, 0.0);
    initializeMap(hyphen_word_tags_counter_, 0.0);
    */

    total_numerical_tags_ = 0.0;
    total_uppercase_letter_tags_ = 0.0;
    total_uppercase_word_tags_ = 0.0;
    total_hyphen_word_tags_ = 0.0;

    return true;
}

void Model::setOptions(ProgramOptions& options) {
    options_.setOptions(options);
    leftContextTree_.setMinimumTimesForBranchesToBeInTree(options_.minimum_times_to_be_in_tree);
    leftContextTree_.setCutValue(options_.cut_value);
}

bool Model::hasData() {
    return input_tags_size_ > 0 and input_words_size_ > 0;
}

bool Model::train(Corpus& corpus) {
    vector<vector<string> > words = corpus.getSentencesWords();
    vector<vector<string> > tags = corpus.getSentencesTags();
    if (!firstPassing(words, tags))
        return false;
    if (!secondPassing(words, tags))
        return false;

    return true;
}

bool Model::firstPassing(vector<vector<string> >& words, vector<vector<string> >& tags) {
    if (words.size() != tags.size()) {
        cerr << "Error: number of words not equal the number of tags. Aborting." << endl;
        return false;
    }

    computeWordsAndTagsCounts(words, tags);
    computeOpenAndClosedTags();
    computeUnknownWordFeaturesCounts(words, tags);

    return true;
}

bool Model::computeWordsAndTagsCounts(vector<vector<string> >& words, vector<vector<string> >& tags) {
    unsigned int S = words.size();
    input_sentences_size_ = S;
    for (unsigned int s = 0; s < S; s++) {
        unsigned int n = words[s].size();
        for (unsigned int i = 0; i < n; i++) {
            word_counter_[words[s][i]]++;
            tag_counter_[tags[s][i]]++;
            word_tag_counter_[words[s][i]][tags[s][i]]++;
            if (!options_.skip_tag_word_probabilities_building)
                tag_word_counter_[tags[s][i]][words[s][i]]++;
            input_words_size_++;
            input_tags_size_++;
        }
    }
    for (auto it_wt = word_tag_counter_.begin(); it_wt != word_tag_counter_.end(); it_wt++) {
        // Store ambiguity degree.
        if ((*it_wt).second.size() > 1) { // Ambiguous word.
            ambiguous_words_.insert((*it_wt).first);
            words_ambiguity_[(*it_wt).second.size()].insert((*it_wt).first);
        }
    }
    return true;
}

bool Model::computeOpenAndClosedTags() {
    unsigned int sum_length_of_words = 0;
    unsigned int number_of_opened_words = 0;
    // Consider a tag as an opened tag if it occurs in the corpus with more than a predefined number of different words.
    int num_opened = 0, num_closed = 0;
    for (auto it_tag_counter = tag_word_counter_.begin(); it_tag_counter != tag_word_counter_.end(); it_tag_counter++) {
        if ((*it_tag_counter).second.size() >= options_.opened_tag_minimum_assigns) {
            opened_tags_.insert((*it_tag_counter).first);
            num_opened++;
            number_of_opened_words += (*it_tag_counter).second.size();
            for (auto it_word = (*it_tag_counter).second.begin(); it_word != (*it_tag_counter).second.end(); it_word++) {
                sum_length_of_words += (*it_word).first.length();
            }
        } else {
            closed_tags_.insert((*it_tag_counter).first);
            num_closed++;
        }
    }
    average_open_word_length_ = (1.0 * sum_length_of_words) / (1.0 * number_of_opened_words);

    //FIXME: take this to another place
    // if save info
//    ofstream outfile;
//    outfile.open("opened_tags.info");
//    if (!outfile.good()) {
//        cerr << "Error: could not open file 'opened_tags.info' for writing" << endl;
//    } else {
//        set<string>::iterator it;
//        for (it = opened_tags_.begin(); it != opened_tags_.end(); it++) {
//            outfile << (*it) << endl;
//        }
//    }
//    outfile.close();
//    outfile.open("closed_tags.info");
//    if (!outfile.good()) {
//        cerr << "Error: could not open file 'closed_tags.info' for writing" << endl;
//    } else {
//        set<string>::iterator it;
//        for (it = closed_tags_.begin(); it != closed_tags_.end(); it++) {
//            outfile << (*it) << endl;
//        }
//    }
//    outfile.close();

    //FIXME: if show stats
//    voutLow << "Average length of words with opened tags: " << m_average_word_length << endl;
//    voutLow << "Number of words: " << input_words_size_ << " Number of sentences: " << words.size() << endl;
//    voutLow << "Average number of words per sentence: " << (long double) input_words_size_ / (long double) input_sentences_size_ << endl;
//    voutLow << "\tNumber of opened tags: " << num_opened << "(" << opened_tags_.size() << ") "
//            << "\tNumber of closed tags: " << num_closed << "(" << closed_tags_.size() << ")" << endl;
//    voutLow << "\tNumber of tags: " << tag_counter_.size() << "(" << m_contextTree.getNumberOfTags() << ")["
//            << tag_word_counter_.size() << "]" << endl;

    return true;
}

bool Model::computeUnknownWordFeaturesCounts(vector<vector<string> >& words, vector<vector<string> >& tags) {
    decltype(numerical_tags_counter_) numerical_tags;
    decltype(uppercase_word_tags_counter_) uppercase_word_tags;
    decltype(uppercase_letter_tags_counter_) uppercase_letter_tags;
    decltype(hyphen_word_tags_counter_) hyphen_word_tags;

    unsigned int S = words.size();
    for (unsigned int s = 0; s < S; s++) {
        unsigned int n = words[s].size();
        for (unsigned int i = 0; i < n; i++) {
            if (isdigit(words[s][i][0])) {
                if (atoi(words[s][i].c_str()) != 0) {
                    numerical_tags[tags[s][i]]++;
                }
            }
            if (HasUpperChar(words[s][i])) {
                if (isupper(words[s][i][0]) && isupper(words[s][i][words[s][i].length() - 1])) {
                    uppercase_word_tags[tags[s][i]]++;
                } else {
                    uppercase_letter_tags[tags[s][i]]++;
                }
            }
            if (words[s][i].size() > 2 && (words[s][i].find('-') != string::npos)) { // || (words[s][i].find('.') != string::npos && !isdigit(words[s][i][0])))) {
                hyphen_word_tags[tags[s][i]]++;
            }
        }
    }

    unsigned int threshold = options_.opened_tag_minimum_assigns;
    for (auto it_num = numerical_tags.begin(); it_num != numerical_tags.end(); it_num++) {
        if ((*it_num).second >= threshold) {
            numerical_tags_counter_[(*it_num).first] = (*it_num).second;
            total_numerical_tags_ += (*it_num).second;
        }
    }
    for (auto it_uwt = uppercase_word_tags.begin(); it_uwt != uppercase_word_tags.end(); it_uwt++) {
        if ((*it_uwt).second >= threshold) {
            uppercase_word_tags_counter_[(*it_uwt).first] = (*it_uwt).second;
            total_uppercase_word_tags_ += (*it_uwt).second;
        }
    }
    for (auto it_ult = uppercase_letter_tags.begin(); it_ult != uppercase_letter_tags.end(); it_ult++) {
        if ((*it_ult).second >= threshold) {
            uppercase_letter_tags_counter_[(*it_ult).first] = (*it_ult).second;
            total_uppercase_letter_tags_ += (*it_ult).second;
        }
    }
    for (auto it_hwt = hyphen_word_tags.begin(); it_hwt != hyphen_word_tags.end(); it_hwt++) {
        if ((*it_hwt).second >= threshold) {
            hyphen_word_tags_counter_[(*it_hwt).first] = (*it_hwt).second;
            total_hyphen_word_tags_ += (*it_hwt).second;
        }
    }

    //FIXME: put in a separate method
//    voutLow << "Tags detected:" << endl;
//    voutLow << "\tNumerical tags: ";
//    typeof(numerical_tags_counter_.begin()) it_tags;
//    for (it_tags = numerical_tags_counter_.begin(); it_tags != numerical_tags_counter_.end(); it_tags++) {
//        voutLow << (*it_tags).first << "(" << (*it_tags).second << ") ";
//    }
//    voutLow << endl;
//    voutLow << "\tFirst letter uppercase words' tags: ";
//    for (it_tags = uppercase_letter_tags_counter_.begin(); it_tags != uppercase_letter_tags_counter_.end();
//            it_tags++) {
//        voutLow << (*it_tags).first << "(" << (*it_tags).second << ") ";
//    }
//    voutLow << endl;
//    voutLow << "\tWhole uppercase words' tags: ";
//    for (it_tags = uppercase_word_tags_counter_.begin(); it_tags != uppercase_word_tags_counter_.end(); it_tags++) {
//        voutLow << (*it_tags).first << "(" << (*it_tags).second << ") ";
//    }
//    voutLow << endl;
//    voutLow << "\tHyphenized words' tags: ";
//    for (it_tags = hyphen_word_tags_counter_.begin(); it_tags != hyphen_word_tags_counter_.end(); it_tags++) {
//        voutLow << (*it_tags).first << "(" << (*it_tags).second << ") ";
//    }
//    voutLow << endl;

    return true;
}

bool Model::buildAffixTrees(vector<vector<string> >& words, vector<vector<string> >& tags) {
    // Get suffixes from words for later handle of unknown words.

    for (unsigned int s = 0; s < words.size(); s++) {
        for (unsigned int t = 0; t < words[s].size(); t++) { // tags[T-1] == FPUNC
            if (word_counter_[words[s][t]] <= options_.unknown_word_threshold) {
                //if (isWordRare(words[s][t])) {
                if (!options_.skip_wordSuffixTree_building) {
                    wordSuffixTree_.addBranch(getWordSuffix(words[s][t]), tags[s][t]);
                }
//                wordPrefixTree_.addBranch(getWordPrefix(words[s][t]), tags[s][t]);
//                unknown_tag_counter_[tags[s][t]]++;
            } else if (opened_tags_.find(tags[s][t]) != opened_tags_.end()) {
                if (!options_.skip_wordSuffixTree_building) {
                    wordSuffixTree_.addBranch(getWordSuffix(words[s][t]), tags[s][t]);
                }
//                wordPrefixTree_.addBranch(getWordPrefix(words[s][t]), tags[s][t]);
            }
        }
    }

    // Remove rare tags of unknown words
    // FIXME: removing tags for unknown words uses the same threshold, but should not
/*
    unsigned int threshold = options_.unknown_word_threshold; // FIXME: should be a new option
    typeof(unknown_tag_counter_.begin()) it_ut;
    vector<typeof(it_ut)> erasables_ut;
    for (it_ut = unknown_tag_counter_.begin(); it_ut != unknown_tag_counter_.end(); it_ut++) {
        if ((*it_ut).second < threshold) {
            erasables_ut.push_back(it_ut);
        }
    }
    for (unsigned int i = 0; i < erasables_ut.size(); i++) {
        unknown_tag_counter_.erase(erasables_ut[i]);
    }
*/
    //voutLow << "    unknown tags erased: " << erasables_ut.size() << endl;

    // Add some special information to the affix trees.
    long double max_prob = 0.0;
    string probable_numerical_tag = "NUM";
    for (auto it_tags = numerical_tags_counter_.begin(); it_tags != numerical_tags_counter_.end(); it_tags++) {
        if ((*it_tags).second >= max_prob) {
            probable_numerical_tag = (*it_tags).first;
            max_prob = (*it_tags).second;
        }
    }
    voutLow << "Probable numerical tag: " << probable_numerical_tag << endl;
    for (int i = 0; i < 10; i++) {
        if (!options_.skip_wordSuffixTree_building)
            wordSuffixTree_.addBranch(ToString(i), probable_numerical_tag); // Automatic
//        wordPrefixTree_.addBranch(ToString(i), probable_numerical_tag); // Automatic
        //wordSuffixTree_.addBranch(ToString(i), "NUM"); // Tycho Brahe & Mac-Morpho
        //wordSuffixTree_.addBranch(ToString(i), "NC"); // NILC
        //wordSuffixTree_.addBranch(ToString(i), "CD"); // Penn Treebank
    }
    return true;
}

string Model::getWordSuffix(string word) {
    unsigned int suffix_size;
    string suffix = "";
    if (word.length() > 2) {
        if (options_.word_suffix_size_is_set) {
            suffix_size = (unsigned int) floor((word.length() * options_.word_suffix_size) + 0.5);
        } else {
            suffix_size = (word.length() > average_open_word_length_) ? word.length() * 11 / 20 : word.length() / 2;
        }

        if (suffix_size == 0)
            suffix_size = 1;
        if (word.length() < suffix_size) {
            voutLow << "Warning!: word '" << word << "' is shorter than its suffix! " << word.length() << " vs. "
                    << suffix_size << endl;
        } else {
            suffix = word.substr(word.length() - suffix_size, word.length());
            reverse(suffix.begin(), suffix.end());
        }
    } else {
        suffix = word;
    }
    return suffix;
}

string Model::getWordPrefix(string word) {
    unsigned int prefix_size = word.length() / 4; // better than / 2.
    string prefix = word.substr(0, prefix_size);
    return prefix;
}

/*
bool Model::normalizeWordsAndTags() {
    typeof(word_counter_.begin()) it_w;
    for (it_w = word_counter_.begin(); it_w != word_counter_.end(); it_w++) {
        word_probability_[(*it_w).first] = (*it_w).second / input_words_size_;
    }
    typeof(tag_counter_.begin()) it_t;
    for (it_t = tag_counter_.begin(); it_t != tag_counter_.end(); it_t++) {
        tag_probability_[(*it_t).first] = (*it_t).second / input_tags_size_;
    }

    typeof(word_tag_counter_.begin()) it_wt;
    for (it_wt = word_tag_counter_.begin(); it_wt != word_tag_counter_.end(); it_wt++) {
        typeof((*it_wt).second.begin()) it_tag;
        for (it_tag = (*it_wt).second.begin(); it_tag != (*it_wt).second.end(); it_tag++) {
            word_tag_probability_[(*it_wt).first][(*it_tag).first] = (*it_tag).second / word_counter_[(*it_wt).first];
        }
    }
    if (!options_.skip_tag_word_probabilities_building) {
        typeof(tag_word_counter_.begin()) it_tw;
        for (it_tw = tag_word_counter_.begin(); it_tw != tag_word_counter_.end(); it_tw++) {
            typeof((*it_tw).second.begin()) it_word;
            for (it_word = (*it_tw).second.begin(); it_word != (*it_tw).second.end(); it_word++) {
                tag_word_probability_[(*it_tw).first][(*it_word).first] = (*it_word).second
                        / tag_counter_[(*it_tw).first];
            }
        }
    }
    return true;
}
*/


/*
bool Model::normalizeUnknownWordFeatures() {
    typeof(unknown_tag_counter_.begin()) it_ut;
    long double sum_unknown_tag_counter = 0.0;
    for (it_ut = unknown_tag_counter_.begin(); it_ut != unknown_tag_counter_.end(); it_ut++) {
        sum_unknown_tag_counter += (*it_ut).second;
    }
    for (it_ut = unknown_tag_counter_.begin(); it_ut != unknown_tag_counter_.end(); it_ut++) {
        m_unknown_tag_probability[(*it_ut).first] = (*it_ut).second / sum_unknown_tag_counter;
    }

    typeof(numerical_tags_counter_.begin()) it_num;
    for (it_num = numerical_tags_counter_.begin(); it_num != numerical_tags_counter_.end(); it_num++) {
        m_numerical_tags_probability[(*it_num).first] = (*it_num).second / total_numerical_tags_;
    }
    typeof(uppercase_word_tags_counter_.begin()) it_uwt;
    for (it_uwt = uppercase_word_tags_counter_.begin(); it_uwt != uppercase_word_tags_counter_.end(); it_uwt++) {
        m_uppercase_word_tags_probability[(*it_uwt).first] = (*it_uwt).second / total_uppercase_word_tags_;
    }
    typeof(uppercase_letter_tags_counter_.begin()) it_ult;
    for (it_ult = uppercase_letter_tags_counter_.begin(); it_ult != uppercase_letter_tags_counter_.end(); it_ult++) {
        m_uppercase_letter_tags_probability[(*it_ult).first] = (*it_ult).second / total_uppercase_letter_tags_;
    }
    typeof(hyphen_word_tags_counter_.begin()) it_hwt;
    for (it_hwt = hyphen_word_tags_counter_.begin(); it_hwt != hyphen_word_tags_counter_.end(); it_hwt++) {
        m_hyphen_word_tags_probability[(*it_hwt).first] = (*it_hwt).second / total_hyphen_word_tags_;
    }

    return true;
}
*/

/*
bool Model::initializeWordsAndTagsProbability(long double initial_probability) {
    typeof(word_counter_.begin()) it_w;
    for (it_w = word_counter_.begin(); it_w != word_counter_.end(); it_w++) {
        word_probability_[(*it_w).first] = initial_probability;
    }
    typeof(tag_counter_.begin()) it_t;
    for (it_t = tag_counter_.begin(); it_t != tag_counter_.end(); it_t++) {
        tag_probability_[(*it_t).first] = initial_probability;
    }

    typeof(word_tag_counter_.begin()) it_wt;
    for (it_wt = word_tag_counter_.begin(); it_wt != word_tag_counter_.end(); it_wt++) {
        typeof((*it_wt).second.begin()) it_tag;
        for (it_tag = (*it_wt).second.begin(); it_tag != (*it_wt).second.end(); it_tag++) {
            word_tag_probability_[(*it_wt).first][(*it_tag).first] = initial_probability;
        }
    }
    if (!modelOptions.skip_tag_word_probabilities_building) {
        typeof(tag_word_counter_.begin()) it_tw;
        for (it_tw = tag_word_counter_.begin(); it_tw != tag_word_counter_.end(); it_tw++) {
            typeof((*it_tw).second.begin()) it_word;
            for (it_word = (*it_tw).second.begin(); it_word != (*it_tw).second.end(); it_word++) {
                tag_word_probability_[(*it_tw).first][(*it_word).first] = initial_probability;
            }
        }
    }
    return true;
}*/

/*bool Model::initializeUnknownWordFeaturesProbability(long double initial_probability) {
    typeof(m_unknown_tag_counter.begin()) it_ut;
    for (it_ut = unknown_tag_counter_.begin(); it_ut != unknown_tag_counter_.end(); it_ut++) {
        m_unknown_tag_probability[(*it_ut).first] = initial_probability;
    }

    typeof(numerical_tags_counter_.begin()) it_num;
    for (it_num = numerical_tags_counter_.begin(); it_num != numerical_tags_counter_.end(); it_num++) {
        m_numerical_tags_probability[(*it_num).first] = initial_probability;
    }
    typeof(uppercase_word_tags_counter_.begin()) it_uwt;
    for (it_uwt = uppercase_word_tags_counter_.begin(); it_uwt != uppercase_word_tags_counter_.end(); it_uwt++) {
        m_uppercase_word_tags_probability[(*it_uwt).first] = initial_probability;
    }
    typeof(uppercase_letter_tags_counter_.begin()) it_ult;
    for (it_ult = uppercase_letter_tags_counter_.begin(); it_ult != uppercase_letter_tags_counter_.end(); it_ult++) {
        m_uppercase_letter_tags_probability[(*it_ult).first] = initial_probability;
    }
    typeof(hyphen_word_tags_counter_.begin()) it_hwt;
    for (it_hwt = hyphen_word_tags_counter_.begin(); it_hwt != hyphen_word_tags_counter_.end(); it_hwt++) {
        m_hyphen_word_tags_probability[(*it_hwt).first] = initial_probability;
    }

    return true;
}*/

/*
bool Model::addToUnknownWordFeaturesProbability(const string& word, const string& tag, long double probability) {
    if (isdigit(word[0])) {
        if (atoi(word.c_str()) != 0) {
            m_numerical_tags_probability[tag] += probability;
        }
    }

    if (HasUpperChar(word)) {
        map<string, long double>::iterator it;
        if (isupper(word[word.length() - 1])) {
            m_uppercase_word_tags_probability[tag] += probability;
        }
        m_uppercase_letter_tags_probability[tag] += probability;
        ;
    }
    if (word.size() > 2 && (word.find('-') != string::npos)) { // || (word.find('.') != string::npos && !isdigit(word[0])))) {
        m_hyphen_word_tags_probability[tag] += probability;
    }

    wordSuffixTree_.addToSproutsProbabilityAlongBranch(tag, getWordSuffix(word), probability);

    return true;
}
*/

bool Model::secondPassing(vector<vector<string> > & words, vector<vector<string> > & tags) {

    buildAffixTrees(words, tags);
	buildContextTree(words, tags);

    if (!options_.skip_wordSuffixTree_building) {
        wordSuffixTree_.setName("Suffix");
        wordSuffixTree_.setDensity(5);
        wordSuffixTree_.setCutValue(0.0);
        wordSuffixTree_.shake(false);
        wordSuffixTree_.prune();
        wordSuffixTree_.printStatistics();
    }

    if (!options_.skip_leftContextTree_building) {
        leftContextTree_.setName("Left");
        leftContextTree_.setInputSize(input_words_size_);
        leftContextTree_.setAverageSentenceSize((long double) input_words_size_ / (long double) input_sentences_size_);
        leftContextTree_.shake(false);
        leftContextTree_.prune();
        leftContextTree_.printStatistics();
    }
//    if (modelOptions.load_left_context_tree != "" && modelOptions.skip_leftContextTree_building) {
//        if (!readLeftContextTreeFromFile(modelOptions.load_left_context_tree)) {
//            return false;
//        }
//    }
    return true;
}

bool Model::buildContextTree(vector<vector<string> >& words, vector<vector<string> >& tags) {
    int S = (int) tags.size();
    vout << "[Context Trees counting]" << endl;
    time_t start_count = time(NULL);
    int part = S < 19 ? 1 : (int) (S / 19.0);

    for (int s = 0; s < S; s++) {
        if ((s + 1) % part == 0) {
            vout << "\r" << int((s + 1) * 100 / S) << "%";
            cout.flush();
        }
        int T = (int) tags[s].size();
        deque<string> context;
        for (int t = 0; t < T; t++) { // tags[T-1] == FPUNC
            if (!options_.skip_leftContextTree_building) {
                leftContextTree_.addBranch(tags[s][t], context); // Improves result!!!
            }
            if (options_.ignore_punctuation && isPunctuation(words[s][t])) {
                ; //skip
            } else {
                context.push_front(tags[s][t]);
            }
            if ((int) context.size() > options_.maximum_markov_order) {
                context.pop_back();
            }
        }
    }
    time_t stop_count = time(NULL);
    vout << "\r[Finished computing contexts in " << difftime(stop_count, start_count) << "s]" << endl;

    return true;
}


///////////////////////////////////////////////////////////////////////////////

bool Model::isWordRare(string word)
{
    return (getNumberOfWordOccurences(word) <= options_.unknown_word_threshold);
}

bool Model::isWordKnown(string word) const
{
    return (word_counter_.find(word) != word_counter_.end());
            //&& getNumberOfWordOccurences(word) > algoParam.unknown_word_threshold);
}

bool Model::isPunctuation(string word)
{
    return isFinalPunctuation(word) || word[0] == '.' || ispunct(word[0]) || word[0] == '\'' || word[0] == '`' || word[0] == '$' || word[0] == '"' || word[0] == ',' || word[0] == ':' || word[0] == '(';
}

bool Model::isFinalPunctuation(string word)
{
    if (word == "." || word == "..." || word == ";" || word == ":" || word == "!" || word == "?" || word == "!?" || word == "?!")
        return true;
    else
        return false;
}

set<string> Model::getKnownWords()
{
    set<string> known_words;
    for (auto it_words = word_tag_counter_.begin(); it_words != word_tag_counter_.end(); it_words++) {
        known_words.insert((*it_words).first);
    }
    return known_words;
}

set<string> Model::getAmbiguousWords()
{
    /*
    set<string> amb_words;
    map<string, long double>::iterator it_amb;
    for (it_amb = m_ambiguous_words.begin(); it_amb != m_ambiguous_words.end(); it_amb++) {
        amb_words.insert((*it_amb).first);
    }
    return amb_words;
    */
    return ambiguous_words_;
}

///////////////////////////////////////

set<string> Model::getTagsForWord(string word)
{
    set<string> tags_out;

    if (!isWordKnown(word)) {
        set<string> suffix_tags = wordSuffixTree_.getSproutsOfBranch(getWordSuffix(word));
        //set<string> prefix_tags = m_wordPrefixTree.getSproutsOfBranch(getWordPrefix(word));
        if (suffix_tags.empty()) {
            voutLow << "No suffix for word: " << word << endl;
            tags_out = opened_tags_;
        } else {
            tags_out = suffix_tags;
        }
    } else {
        for(auto it_trans = word_tag_counter_[word].begin(); it_trans != word_tag_counter_[word].end(); it_trans++) {
            tags_out.insert((*it_trans).first);
        }
    }

    return tags_out;
}

vector<string> Model::getCandidateTagsForWord(string word)
{
    set<string> tags = getTagsForWord(word);
    return vector<string>(tags.begin(), tags.end());
}

/**
 * @brief Return the probability of @a candidate_tag given @a word and its @a left_context.
 * @return The context actually found in the #ContextTree.
 */
long double Model::getLogProbabilityAndContextUsed(string candidate_tag, string word, const deque<string>& left_context, deque<string>& used_branch)
{
    long double word_prob = 0.0;
    if (isWordKnown(word)) {
        word_prob = getKnownWordLogProbability(candidate_tag, word);
    } else {
        word_prob = getUnknownWordLogProbability(candidate_tag, word);
    }

    long double left_prob = 0.0;
    left_prob = leftContextTree_.getSproutLogProbability(candidate_tag, left_context, used_branch);
    // NOTE: Same result than 1 and worse result than 2:
    //1 left_prob = m_leftContextTree.getSproutProbability(candidate_tag, left_context, used_branch);
    //2 left_prob = m_leftContextTree.getSproutAccumulatedProbability(candidate_tag, left_context, used_branch);
//    if (left_prob == 0.0) {
//        typeof(tag_probability_.begin()) it_tag = tag_probability_.find(candidate_tag);
//        if (it_tag != tag_probability_.end()) {
//            left_prob = (*it_tag).second;
    if (left_prob == -numeric_limits<long double>::infinity()) {
        auto it_tag = tag_counter_.find(candidate_tag);
        if (it_tag != tag_counter_.end()) {
            left_prob = logl((*it_tag).second) - logl(input_tags_size_);
        } else {
            voutHigh << "Warning: Unknown Tag: " << candidate_tag << " | word: " << word << "; left context: " << ToString(left_context) << endl;
        }
    }
//    word_prob = word_prob != 0.0 ? word_prob : 0.0;
//    left_prob = left_prob != 0.0 ? left_prob : 0.0;

//    if (word == "supremo") {
//        cerr << word << "/" << candidate_tag << ": Pw: " << ToString(word_prob) << "; Pl: " << ToString(left_prob) << endl;
//    }
    return word_prob + left_prob;
}

long double Model::getKnownWordProbability(const string& tag, const string& word, long double value_if_not_found)
{
    long double ret = getKnownWordLogProbability(tag, word, value_if_not_found);
    if (ret != value_if_not_found)
        return expl(ret);
    else
        return value_if_not_found;
}

long double Model::getKnownWordLogProbability(const string& tag, const string& word, long double value_if_not_found)
{
    auto it_prob = tag_word_counter_[tag].find(word);
    if (it_prob != tag_word_counter_[tag].end()) {
        return logl((*it_prob).second) - logl(tag_counter_[tag]);
    } else {
        return value_if_not_found;
    }
}

long double Model::getUnknownWordLogProbability(const string& candidate_tag, const string& word)
{
    long double case_prob = 0.0, case_prob2 = 0.0, hyphen_prob = 0.0, suffix_prob = 0.0;
//    long double prefix_prob = 0.0;
    if (HasUpperChar(word)) {
        map<string, long double>::iterator it;
        if (isupper(word[word.length()-1])) {
            it = uppercase_word_tags_counter_.find(candidate_tag);
            if (it != uppercase_word_tags_counter_.end()) {
                case_prob2 = (*it).second;
            }
        }
        it = uppercase_letter_tags_counter_.find(candidate_tag);
        if (it != uppercase_letter_tags_counter_.end()) {
            case_prob += (*it).second;
        }
    }
    if (word.size() > 2 && (word.find('-') != string::npos)) {// || (word.find('.') != string::npos && !isdigit(word[0])))) {
        map<string, long double>::iterator it = hyphen_word_tags_counter_.find(candidate_tag);
        if (it != hyphen_word_tags_counter_.end()) {
            hyphen_prob += (*it).second;
        }
    }
    suffix_prob += wordSuffixTree_.getSproutCount(candidate_tag, getWordSuffix(word));
    //prefix_prob += 0.2 * m_wordPrefixTree.getSproutCount(candidate_tag, getWordPrefix(word));

    suffix_prob = suffix_prob != 0.0 ? logl(suffix_prob) : 0.0;
    case_prob = case_prob != 0.0 ? logl(case_prob) : 0.0;
    hyphen_prob = hyphen_prob != 0.0 ? logl(hyphen_prob) : 0.0;
    case_prob2 = case_prob2 != 0.0 ? logl(case_prob2) : 0.0;
//    prefix_prob = prefix_prob != 0.0 ? logl(prefix_prob) : 0.0;

//    if (word == "supremo") {
//		cerr << "Word: " << word
//				<< ", tag: " << candidate_tag
//				<< "; " << case_prob
//				<< "; " << hyphen_prob
//				<< "; " << suffix_prob
//				<< "; " << case_prob2
////				<< "; " << prefix_prob
//				<< "; suff: " << getWordSuffix(word)
//				<< endl;
//		cerr << "sprout count: " << ToString(wordSuffixTree_.getSproutCount(candidate_tag, getWordSuffix(word))) << endl;
//    }
    return case_prob + hyphen_prob + suffix_prob + case_prob2; // + prefix_prob;
}


deque< pair<string, long double> > Model::getWordsByFrequency(int max) const
{
    if (max < 0) max = numeric_limits<int>::infinity();
    multimap<long double, string> more;
    more.insert(pair<long double,string>(0.0,""));
    for (auto it_most = word_counter_.begin(); it_most != word_counter_.end(); it_most++) {
        if ((*it_most).second > (*more.begin()).first) {
            if ((int)more.size() < max) {
                more.insert(pair<long double,string>((*it_most).second, (*it_most).first));
            } else {
                more.erase(more.begin());
                more.insert(pair<long double,string>((*it_most).second, (*it_most).first));
            }
        }
    }

    deque< pair<string, long double> > wf;
    for (auto it_more = more.rbegin(); it_more != more.rend(); it_more++) {
        wf.push_back(make_pair((*it_more).second, (*it_more).first));
    }
    return wf;
}

/**
 * @brief Takes punctuation tokens out of the @a context.
 * @param context Sequence of tokens to filter.
 * @return True if at least one token was removed from @a context.
 *
 * This function removes any punctuation tokens from @a context.
 * Punctuation is checked with the %isPunctuation method.
 */
bool Model::filterContext(deque<string>& context)
{
    bool filtered = false;
    for (int i = 0; i < (int)context.size(); i++) {
        if (isPunctuation(context[i])) {
            context.erase(context.begin()+i);
            i--;
            filtered = true;
        }
    }
    return filtered;
}

/**
 * @brief Gets a copy of @a context without any punctuation tokens.
 * @param context Sequence of tokens to filter.
 * @return A copy of @a context without punctuation tokens..
 *
 * This function removes any punctuation tokens from @a context.
 * Punctuation is checked with the %isPunctuation method.
 */
deque<string> Model::getFilteredContext(const deque<string>& context)
{
    deque<string> clean_context;
    for (unsigned int i = 0; i < context.size(); i++) {
        if (!isPunctuation(context[i])) {
            clean_context.push_back(context[i]);
        }
    }
    return clean_context;
}

bool Model::simplifyOneTag(string& tag)
{
    tag = StringTokenize(tag, "-")[0];
    return true;
}

/// Simplifying tags stripping '-.\+'.
bool Model::simplifyTags(deque<string>& context)
{
    bool simplified = true;

    for (unsigned int i = 0; i < context.size(); i++) {
        //context[i] =
        simplifyOneTag(context[i]);
    }

    return simplified;
}
