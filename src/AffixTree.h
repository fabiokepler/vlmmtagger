/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2013 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

#ifndef AFFIXTREE_H
#define AFFIXTREE_H

#include <stack>
#include "main.h"
#include "functions.h"

class AffixTree {
public:
    struct sprout {
        long double count;
//        long double probability;

        template<class Archive>
        void serialize(Archive & ar, const unsigned int version) {
//            ar & probability;
            ar & count;
        }
    };

    struct node {
        char name;
        long double count;
//        long double probability;
        int depth;
        bool is_final_leaf;
        map<char, node> branches;
        node* parent;
        map<string, sprout> sprouts;

        template<class Archive>
        void serialize(Archive & ar, const unsigned int version) {
            ar & count;
//            ar & probability;
            ar & depth;
            ar & is_final_leaf;
            ar & branches;
            ar & sprouts;
            //ar & father;
        }
    };

    AffixTree();
    AffixTree(int min_times);
    virtual ~AffixTree();

    // Mutators
    void addBranch(string nodes, string sprout_name);

    void setDensity(int min_times);
    void normalizeProbabilities();
    void setCutValue(long double K);
    void shake(bool apply_to_sprouts = false);
    bool prune();
    long double getCutValue();
    long double getKLDivergence(long double logprob_uv, long double logprob_v);

    bool addToSproutsCountsAlongBranch(const string& dest_sprout, const string& branch, long double count);
    bool addToSproutsProbabilityAlongBranch(const string& dest_sprout, const string& branch, long double probability);

    long double applyLogToSproutsProbability();

    // Accessors
    long double getSproutProbability(const string& sprout_name, const string& branch);
    long double getSproutProbability(const string& sprout_name, const string& branch, long double& branch_prob);
    long double getSproutCount(const string& sprout_name, const string& branch);
    long double getSproutAccumulatedCount(const string& sprout_name, const string& branch);
    long double getSproutAccumulatedProbability(const string& sprout_name, const string& branch);

    deque<string> getSproutsOfRoot();
    //deque<string> getSproutsOfNode(char node_name);
    set<string> getSproutsOfBranch(string branch);
    //deque<string> getRelevantContextOfHistory(string present, vector<string> history);
    //int getNumberOfTags() {return (int)_root.branches.size();};

    node* getLastNodeOfBranchWithSprout(const string& branch, const string& sprout);

    long double resetProbabilities(long double initial_probability = 1.0);

    void refreshNodesParents();

    // Utilities
    void print();
    void printStatistics();
    void printSimpleStatistics();

    bool writeToFile(ofstream & outfile);
    bool readFromFile(ifstream & infile);

    const string& getName() const {
        return name_;
    }

    void setName(const string& name) {
        name_ = name;
    }

    int getDensity();

private:
    void init();

    node root_;
    string name_;

    //Parameteres
    int density_;
    long double cut_value_;

    AffixTree::sprout newSprout();
    AffixTree::node newNode();

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version) {
        ar & root_;
        ar & density_;
        ar & cut_value_;

        refreshNodesParents();
    }

};

BOOST_CLASS_VERSION(AffixTree, 3)

#endif // AFFIXTREE_H
