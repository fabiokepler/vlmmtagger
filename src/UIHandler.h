/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2013 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
******************************************************************************/

/* 
 * File:   UIHandler.h
 * Author: kepler
 *
 * Created on October 5, 2011, 8:11 PM
 */

#ifndef UIHANDLER_H
#define	UIHANDLER_H

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <ostream>
#include <sstream>
#include <set>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <ostream>
#include <vector>
#include <string>
#include <set>
#include <map>
#include <deque>
#include <algorithm>
#include <iterator>
#include <math.h>

using namespace std;

namespace ui
{

class UIHandler
{
public:
	UIHandler();
	virtual ~UIHandler();


    class VerboseOutput : public ostream
    {
    public:
        enum Level {QUIET, NORMAL, VERBOSE} level;
        enum Priority {HIGH, MEDIUM, LOW} output_priority, allowed_priority;

        VerboseOutput() {
            level = NORMAL;
            allowed_priority = MEDIUM;
            output_priority = MEDIUM;
        };
        VerboseOutput(Priority prior) : output_priority(prior) {};

        void setLevel(Level x_level) {
            level = x_level;
            if (level == QUIET)
                allowed_priority = HIGH;
            else if (level == NORMAL)
                allowed_priority = MEDIUM;
            else if (level == VERBOSE)
                allowed_priority = LOW;
        };

        void setOutputPriority(Priority x_prior) {
            output_priority = x_prior;
        };

        typedef ostream& (*manip_t)(ostream&);
        template <typename T>
        VerboseOutput& operator<<(T str_object)
        {
            if (output_priority <= allowed_priority)
                cout << str_object;
            return *this;
        };
        //template < >
        VerboseOutput& operator<<(manip_t endl_obj)
        {
            if (output_priority <= allowed_priority)
                cout << endl_obj;
            return *this;
        };
        // */
    }; // class Verbose Output

}; // class UIHandler

} // namespace ui


#endif	/* UIHANDLER_H */

