/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2013 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
******************************************************************************/

/* 
 * File:   ProgramOptions.h
 * Author: kepler
 *
 * Created on October 19, 2011, 7:52 PM
 */

#ifndef PROGRAMOPTIONS_H
#define	PROGRAMOPTIONS_H

#include <string>
#include <map>
#include <set>
#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>

namespace po = boost::program_options;
namespace pt = boost::property_tree;

using std::string;
using std::map;
using std::set;

class ProgramOptions {

public:
    ProgramOptions(string program_name, string version, string contact);
    virtual ~ProgramOptions();

    int parseArguments(int argc, char** argv);

    bool loadConfigFile(string filename, po::options_description config_file_options);
    bool saveConfigFile(string filename, pt::ptree property_tree);

    inline bool isSet(string option) {return variables_map_.count(option);};
    template<typename T>
    T get(string option) {return variables_map_[option].as<T>();}

    void parseSeparators();
    map<unsigned int, string> getSeparators() const {
        return separators;
    }
    set<string> getFinalPunctuationSymbols() const {
        return final_punctuation_symbols;
    }

    
private:
    void init();
    pt::ptree buildPropertyTree(po::variables_map& vm);

    string program_name_, version_, contact_;
    po::variables_map variables_map_;
    
//    bool extract_split_words;
//    bool extract_composed_tags;

    /**
     * 0: Separator in input file between word and tag.
     * 1: Separator in input file between a pair of <word,tag>.
     * 2: Separator in output file between word and tag.
     * 3: Separator in output file between a pair of <word,tag>.
     */
    map<unsigned int, string> separators;

    set<string> final_punctuation_symbols; // Set manually in 'init()'. TODO: put it on a config file.
};

#endif	/* PROGRAMOPTIONS_H */

