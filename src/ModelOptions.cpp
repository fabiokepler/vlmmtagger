/*******************************************************************************
 * VLMMTagger: A Part-of-Speech Tagger based on Variable-Length Markov Models.
 * Copyright 2005-2013 Fabio N. Kepler
 * Done mostly under the supervision of Marcelo Finger.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

/* 
 * File:   ModelParameters.cpp
 * Author: kepler
 * 
 * Created on October 19, 2011, 8:13 PM
 */

#include <iostream>
#include <string>
#include <set>
#include <vector>
#include <map>

#include "Model.h"

Model::ModelOptions::ModelOptions() {
    do_training = false;
    iterations = 0;

    skip_leftContextTree_building = false;
    skip_wordSuffixTree_building = false;
    skip_tag_word_probabilities_building = false;
    ignore_punctuation = false;

    opened_tag_minimum_assigns = 10; //5;
    unknown_word_threshold = 5;

    word_suffix_size_is_set = false;
    word_suffix_size = 11.0 / 20.0;

    cut_value = 50.0;
    maximum_markov_order = 10;
    minimum_times_to_be_in_tree = 1;
}

Model::ModelOptions::~ModelOptions() {
}

bool Model::ModelOptions::isIgnoringPunctuation() const {
    return this->ignore_punctuation;
}

void Model::ModelOptions::setOptions(ProgramOptions& options) {
    cut_value = options.get<double>("cut-value");
    if (options.isSet("density"))
        minimum_times_to_be_in_tree = options.get<int>("density");
    if (options.isSet("order"))
        maximum_markov_order = options.get<int>("order");
    if (options.isSet("suffix-size")) {
        word_suffix_size_is_set = true;
        word_suffix_size = options.get<double>("suffix-size");
    }
    if (options.isSet("unknown-word-max-freq"))
        unknown_word_threshold = options.get<unsigned int>("unknown-word-max-freq");
    if (options.isSet("open-class-threshold"))
        opened_tag_minimum_assigns = options.get<unsigned int>("open-class-threshold");
    if (options.isSet("training"))
        do_training = true;
    iterations = options.get<int>("iterations");
    if (options.isSet("skip-build-lct"))
        skip_leftContextTree_building = true;
    if (options.isSet("skip-build-wst"))
        skip_wordSuffixTree_building = true;
    if (options.isSet("skip-build-twp"))
        skip_tag_word_probabilities_building = true;
    if (options.isSet("skip-build-all")) {
        skip_leftContextTree_building = true;
        skip_wordSuffixTree_building = true;
        skip_tag_word_probabilities_building = true;
    }
    if (options.isSet("ignore-punctuation"))
        ignore_punctuation = true;
}

void Model::ModelOptions::print() const {
    cout << "[Model Options]" << endl;
    cout << "Context Tree options:" << endl;
    cout << "\tK = " << cut_value << endl;
    cout << "\tDensity = " << minimum_times_to_be_in_tree << endl;
    cout << "\tMaximum order = " << maximum_markov_order << endl;
    cout << "General options:" << endl;
    if (word_suffix_size_is_set)
        cout << "\tWord suffix size = " << word_suffix_size << " * |word length|" << endl;
    cout << "\tMinimum assigns for a tag to be known = " << unknown_word_threshold << endl;
    cout << "\tMinimum assigns for a tag to be opened = " << opened_tag_minimum_assigns << endl;
    cout << "[End Model Options]" << endl;
}
