################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/AffixTree.cpp \
../src/ContextTree.cpp \
../src/Corpus.cpp \
../src/DecoderByViterbi.cpp \
../src/FileHandler.cpp \
../src/Model.cpp \
../src/ModelOptions.cpp \
../src/ProgramOptions.cpp \
../src/UIHandler.cpp \
../src/functions.cpp \
../src/main.cpp 

OBJS += \
./src/AffixTree.o \
./src/ContextTree.o \
./src/Corpus.o \
./src/DecoderByViterbi.o \
./src/FileHandler.o \
./src/Model.o \
./src/ModelOptions.o \
./src/ProgramOptions.o \
./src/UIHandler.o \
./src/functions.o \
./src/main.o 

CPP_DEPS += \
./src/AffixTree.d \
./src/ContextTree.d \
./src/Corpus.d \
./src/DecoderByViterbi.d \
./src/FileHandler.d \
./src/Model.d \
./src/ModelOptions.d \
./src/ProgramOptions.d \
./src/UIHandler.d \
./src/functions.d \
./src/main.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -Wextra -c -fmessage-length=0 -std=c++11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


