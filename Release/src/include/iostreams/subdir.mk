################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/include/iostreams/gzip.cpp \
../src/include/iostreams/zlib.cpp 

OBJS += \
./src/include/iostreams/gzip.o \
./src/include/iostreams/zlib.o 

CPP_DEPS += \
./src/include/iostreams/gzip.d \
./src/include/iostreams/zlib.d 


# Each subdirectory must supply rules for building sources it contributes
src/include/iostreams/%.o: ../src/include/iostreams/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -Wextra -c -fmessage-length=0 -std=c++11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


